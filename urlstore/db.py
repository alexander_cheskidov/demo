import aiopg


async def create_aiopg(app):
    pool = await aiopg.create_pool('dbname=urlstore user=a')
    app['db'] = pool
    

async def dispose_aiopg(app):
    app['db'].close()
    await app['db'].wait_closed()
