import hashids
from aiohttp import web

hashid_encoder = hashids.Hashids(salt='abc')


async def index(request):
    return web.Response(text=INDEX_TEMPLATE, content_type='text/html')


async def redir(request):
    url_hid = request.match_info['url_hid']
    url_id = hashid_encoder.decode(url_hid)
    if not url_id:
        return web.Response(text='wrong hid', status=409)
    url_id = url_id[0]
    with await request.app['db'].cursor() as cur:
        await cur.execute('select created_at, url from urlstore where id=%s', [url_id])
        row = await cur.fetchone()
        if row is None:
            return web.Response(text='not found', status=404)
        else:
            created_at, url = row
            try:
                do_redirect = bool(int(request.query.get('redirect', 1)))
            except TypeError:
                do_redirect = False
            if do_redirect:
                return web.Response(headers={'Location': url}, status=302)
            else:
                return web.Response(text=url)


async def store(request):
    data = await request.post()
    with await request.app['db'].cursor() as cur:
        await cur.execute('insert into urlstore(created_at, url) values (current_timestamp, %s) RETURNING id', [data['url']])
        result = await cur.fetchone()
        last_id = result[0]
        hashid = hashid_encoder.encode(last_id)
        return web.json_response({'id': last_id, 'hashid': hashid, 'url': request.url.with_path(path=hashid).human_repr()})


INDEX_TEMPLATE = '''<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
</head>
<body><center>
<form method="post" action="">
<input type="text" name="url"> 
<input type="submit">
</form>
</center></body>
</html>
'''
