from aiohttp import web
from views import index, redir, store
from db import create_aiopg, dispose_aiopg


app = web.Application()
app.router.add_get('/', index)
app.router.add_resource('/{url_hid}').add_route('GET', redir)
app.router.add_post('/', store)

app.on_startup.append(create_aiopg)
app.on_cleanup.append(dispose_aiopg)

web.run_app(app, host='127.0.0.1', port=8080)
