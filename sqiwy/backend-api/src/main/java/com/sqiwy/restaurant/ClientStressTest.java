package com.sqiwy.restaurant;


import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sqiwy.restaurant.api.BackendClient;
import com.sqiwy.restaurant.api.BackendException;
import com.sqiwy.restaurant.api.ClientConfig;
import com.sqiwy.restaurant.api.ClientConfig.SupportedLanguage;
import com.sqiwy.restaurant.api.OperationService;
import com.sqiwy.restaurant.api.data.ChatMessage;
import com.sqiwy.restaurant.api.data.ChatUser;
import com.sqiwy.restaurant.api.data.Menu;
import com.sqiwy.restaurant.api.data.NotificationType;
import com.sqiwy.restaurant.api.data.Order;
import com.sqiwy.restaurant.api.data.OrderProduct;
import com.sqiwy.restaurant.api.data.OrderProductModifier;
import com.sqiwy.restaurant.api.data.SystemParameters;
import com.sqiwy.restaurant.api.data.TableSession;


/**
 * Небольшой пример использования клиентского api
 */
public class ClientStressTest implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(ClientStressTest.class);

    private final BackendClient client = new BackendClient();
    private OperationService operationService;
    
    private final ClientConfig config = new ClientConfig();
    
    private String clientId;

    public static void main(String[] args) {

        (new Thread(new ClientStressTest("1"))).start();
        for (int i = 2; i<200; ++i) {
            String clientId = String.format("%d", i);
            (new Thread(new ClientStressTest(clientId))).start();
        }
    }
    
    public ClientStressTest(String clientId) {
        this.clientId = clientId;
    }

    public void start() {
        try {
//            config.setHostName("localhost");
            config.setApiUrl("http://127.0.0.1:8001/api/v1");
            config.setWebUrl("http://127.0.0.1:80");
            config.setClientId(this.clientId);
            config.setPassword("1234");            

            client.setConfig(config);
            client.setEventService(new ClientTestEventService(clientId));
            client.start();
            
            client.setLanguage(SupportedLanguage.RU);
            
            operationService = client.getOperationService();
            int i = 100;
            while (i>0) {
                
                i -= 1;
                
                try{
                    
                    testPing();
                    testSession();
//                    testPlaceOrder();
                    testGetOrders();
                    testNotification();
                    testSystemParameters();
                    testReportCommercialStats();
                    
//                    testChat1();
//                    
//                    boolean exit = false;
//                    while (!exit) {
//                        String usage = "Usage:\np - ping\no - open session\nc - close session\nm - menu\n1, 2, 3, 4, 5 - test methods\nx - exit";
//                        System.out.println(usage);
//                        while (!exit) {
//                            switch (System.in.read()) {
//                                case 'x': exit = true; break;
//                            }
//                        }
//                    }
                    
                } catch (BackendException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            log.error("Unhandled exception", e);
        }
        finally {
            if (client != null) {
                client.close();
            }
            System.out.println("exit...");
        }
        
    }
    
    public void testPing() throws IOException, BackendException {
        operationService.ping();
    }
    
    @SuppressWarnings("unused")
    public void testMenu() throws BackendException, IOException {
        Menu m = operationService.getMenu();
        return ;
    }
    
    @SuppressWarnings("unused")
    public void testGetOrders() throws BackendException, IOException {
        List<Order> ol = operationService.getOrderList();
        return ;
    }
    
    @SuppressWarnings("deprecation")
    public void testReportCommercialStats() throws BackendException, IOException
    {
        Map<String, Integer> data = new HashMap<String, Integer>();
        data.put("ad_id", 10);
        data.put("play_time", 1024);
        data.put("click", 1);
        operationService.statData(data);
    }
    
    @SuppressWarnings("unused")
    public void testSession() throws BackendException, IOException {
        TableSession ts = operationService.openTableSession();
        Boolean result = operationService.closeTableSession();
        TableSession ts1 = operationService.openTableSession();
        TableSession ts2 = operationService.openTableSession();
    }
    
    public void testPlaceOrder() throws BackendException, IOException {
        Order order = new Order();
        List<OrderProduct> orderProductList = new ArrayList<OrderProduct>();
        OrderProduct orderProduct = new OrderProduct();
        orderProduct.setAmount(1);
        orderProduct.setPrice(new BigDecimal("100.00"));
        orderProduct.setProductId(1);
        orderProductList.add(orderProduct);
          
        List<OrderProductModifier> orderProductModifierList = new ArrayList<OrderProductModifier>();
        OrderProductModifier m1 = new OrderProductModifier();
        m1.setModifierId(1);
        m1.setAmount(2);
        m1.setPrice(new BigDecimal("2.00"));
        orderProductModifierList.add(m1);
  
        OrderProductModifier m2 = new OrderProductModifier();
        m2.setModifierId(2);
        m2.setAmount(3);
        m2.setPrice(new BigDecimal("5.00"));
        orderProductModifierList.add(m2);
  
        orderProduct.setModifiers(orderProductModifierList);
  
        //order.setProductList(orderProductList);
        order = operationService.placeOrder(order);
    }

    public void testNotification() throws BackendException, IOException {
        operationService.callStaff(NotificationType.CALL_WAITER);
        operationService.callStaff(NotificationType.CALL_HOOKAH);
    }
    
    @SuppressWarnings("unused")
    public void testSystemParameters() throws BackendException, IOException {
        SystemParameters sp = operationService.getSystemParameters();
        return;
    }
    
    @SuppressWarnings("unused")
    public void testChat1() throws BackendException, IOException {
        
        TableSession sess = operationService.openTableSession();
        
        ChatUser chatUser1 = new ChatUser();
        chatUser1.setNickname("tester");
        chatUser1.setColor("#FFF000");
        chatUser1.setSmile(ChatUser.Smile.ANGRY);
        chatUser1.setStatus("testing chat with tester...");
        operationService.createChatUser(chatUser1);
        
        ChatMessage m = chatUser1.createMessage();
        m.setTargetId(0);
        m.setText("hello");
        operationService.sendChatMessage(m);
        
        List<ChatUser> ul = operationService.getChatUsers();
        operationService.exitChat();
        
    }

    @Override
    public void run() {
        start();
    }
    

}
