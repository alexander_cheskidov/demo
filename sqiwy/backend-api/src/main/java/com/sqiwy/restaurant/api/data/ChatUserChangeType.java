package com.sqiwy.restaurant.api.data;

import com.sqiwy.restaurant.api.EventService;

/**
 * Defines change types for the {@link EventService#chatUserChanged(ChatUserChangeType, ChatUser)} event.
 */
public enum ChatUserChangeType {

    /** Indicates that a new user joined the chat. */
    CREATED,

    /** Indicates that an existing user updated his properties. */
    UPDATED,

    /** Indicates that an existing user left the chat. */
    DELETED;
}
