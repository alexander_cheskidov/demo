package com.sqiwy.restaurant.api.data;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;


public class SystemParameters {

    private String tablePassword;
    private EnvironmentType environment;

    public String getTablePassword() {
        return tablePassword;
    }

    public void setTablePassword(String tablePassword) {
        this.tablePassword = tablePassword;
    }

    public EnvironmentType getEnvironment() {
        return environment;
    }

    public void setEnvironment(EnvironmentType environment) {
        this.environment = environment;
    }

    public String toJson() throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> settings = new HashMap<String, Object>();
        settings.put("tablePassword", tablePassword);
        settings.put("environment", environment);
        String json = mapper.writeValueAsString(settings);
        return json;
    }

    public static SystemParameters fromJson(String json) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        SystemParameters settings = mapper.readValue(json, SystemParameters.class);
        return settings;
    }
}
