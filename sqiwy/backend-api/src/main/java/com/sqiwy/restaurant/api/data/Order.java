package com.sqiwy.restaurant.api.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents an order.
 */
public class Order {
    private Integer id;
    private String waiterName;
    private OrderStatus status;
    private Date createTime;
    private Date finishTime;
    private BigDecimal total;
    private List<OrderGuest> guests;

    /**
     * Gets the id of this order.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id of this order.
     * <p>
     * You can specify the order id to append or update items of existing order. Otherwise, a new order will be created.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the localized name of waiter assigned to this order.
     */
    public String getWaiterName() {
        return waiterName;
    }

    /**
     * Sets the localized name of waiter assigned to this order.
     * <p>
     * This property is intended to set by the server.
     */
    public void setWaiterName(String waiterName) {
        this.waiterName = waiterName;
    }

    /**
     * Gets the status of this order.
     * <p>
     * You can update order only if status is OPEN.
     */
    public OrderStatus getStatus() {
        return status;
    }

    /**
     * Sets the status of this order.
     * <p>
     * You can update order only if status is OPEN.
     * <p>
     * This property is intended to set by the server.
     */
    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    /**
     * Gets the time when this order was created.
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * Sets the time when this order was created.
     * <p>
     * This property is intended to set by the server.
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * Gets the time when this order was paid or cancelled.
     */
    public Date getFinishTime() {
        return finishTime;
    }

    /**
     * Sets the time when this order was paid or cancelled.
     * <p>
     * This property is intended to set by the server.
     */
    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    /**
     * Gets the summary price of all order items.
     */
    public BigDecimal getSubtotal() {
        BigDecimal subtotal = BigDecimal.ZERO;
        if (guests != null) {
            for (OrderGuest quest : guests) {
                subtotal = subtotal.add(quest.getSubtotal());
            }
        }
        return subtotal;
    }

    /**
     * Gets the summary price of all order items after discounts. This sum must be paid by guests.
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Sets the summary price of all order items after discounts. This sum must be paid by guests.
     * <p>
     * This property is intended to set by the server.
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * Gets the list of all guests included to this order.
     */
    public List<OrderGuest> getGuests() {
        return guests;
    }

    /**
     * Gets the list of all guests included to this order.
     */
    public void setGuests(List<OrderGuest> guests) {
        this.guests = guests;
    }
    
    /**
     * @deprecated use {@link #setGuests(List)} and {@link OrderGuest#setProducts(List)} instead
     */
    @Deprecated
    public void setProductList(List<OrderProduct> productList) {
        if (guests == null) {
            guests = new ArrayList<OrderGuest>(1);
        }
        if (guests.size() == 0) {
            guests.add(new OrderGuest());
        }
        OrderGuest guest = guests.get(0);
        guest.setProducts(productList);
    }
}
