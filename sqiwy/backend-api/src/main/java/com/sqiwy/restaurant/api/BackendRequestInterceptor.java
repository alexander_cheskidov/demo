package com.sqiwy.restaurant.api;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import retrofit.RequestInterceptor;


public class BackendRequestInterceptor implements RequestInterceptor {
    
    private BackendFacade backendFacade;
    private ClientConfig config;
    
    public BackendRequestInterceptor(BackendFacade facade, ClientConfig config) {
        this.backendFacade = facade;
        this.config = config;
    }
        
    @Override
    public void intercept(RequestInterceptor.RequestFacade request) {
        String userAgent = "BackendClient";
        String date = String.format("%d", new Date().getTime());
        String challenge = getChallengeHash(date, config.getPassword(), config.getClientId());
        
        request.addHeader("Date", date);
        request.addHeader("Accept-Language", backendFacade.getLanguage().getCode());
        request.addHeader("User-Agent", userAgent);
        
        // NOTE: do not set Content-Type header! It is used by underlying libraries (OkHttp)
        // to indicate right data encoding type
        request.addHeader("X-Content-Type", "x-gson/json");
        request.addHeader("X-Device-Code", config.getClientId());
        request.addHeader("X-Device-Pass", challenge);
    }
    
    private String getChallengeHash(String date, String passwd, String clientId) {
        // TODO: salt probably should be configurable
        String glue = "ipa$@";
        String pass = date + glue + passwd + glue + clientId;
        return generateHash(pass);
    }
    
    /**
     * Be careful with this method - it should only contain ascii compatible strings (to avoid conversions to utf8), 
     * otherwise result will be unpredictable 
     * @param s
     * @return
     */
    private String generateHash(String s) {
        
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        }
        catch(NoSuchAlgorithmException e) {
            e.printStackTrace();
        } 
        md.update(s.getBytes());
        String hx = new BigInteger(1, md.digest()).toString(16);
        hx = String.format("%40s", hx).replace(' ', '0');
        return hx;
    }
}
