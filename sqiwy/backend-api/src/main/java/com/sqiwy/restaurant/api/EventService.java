package com.sqiwy.restaurant.api;

import com.sqiwy.restaurant.api.data.ChatMessage;
import com.sqiwy.restaurant.api.data.ChatUser;
import com.sqiwy.restaurant.api.data.ChatUserChangeType;
import com.sqiwy.restaurant.api.data.Order;
import com.sqiwy.restaurant.api.data.RestartMode;
import com.sqiwy.restaurant.api.data.SystemParameters;

/**
 * Объявляет методы для обработки уведомлений, пересылаемых от сервера к клиенту.
 * Реализация этого интерфейса требуется {@link BackendClient}.
 */
public interface EventService {

    /**
     * уведомление что меню было изменено
     */
    public void menuChanged();

    /**
     * уведомление о том что официант получил вызов
     */
    public void staffCallConfirmed();

    /**
     * сессия стола была открыта
     */
    public void tableSessionOpened();

    /**
     * сессия стола была закрыта
     */
    public void tableSessionClosed();

    /**
     * Called if the new chat message received.
     * @param message the new chat message
     */
    public void chatMessageReceived(ChatMessage message);

    /**
     * Called whenever a chat user was changed.
     * <p>
     * If the change type is {@link ChatUserChangeType#DELETED} then only id property of the user has an actual value.
     * All other properties are not initialized.
     * @param type the type of this change
     * @param user the changed user
     */
    public void chatUserChanged(ChatUserChangeType type, ChatUser user);

    /**
     * Indicates that the reboot has been requested.
     */
    public void restartRequested(RestartMode mode);
    
    public SystemParameters getInfo();

    /**
     * Called whenever an order was changed.
     * @param order the changed order
     */
    public void orderChanged(Order order);
}
