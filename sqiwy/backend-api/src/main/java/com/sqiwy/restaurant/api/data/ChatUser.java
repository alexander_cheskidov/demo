package com.sqiwy.restaurant.api.data;

import java.util.Date;

/**
 * Represents a chat member.
 */
public class ChatUser {

    /**
     * Specifies constants defining which smile to display.
     */
    public static enum Smile {
        HAPPY, SMILE, TOUNGUE, SAD, WRINKLE, HOLLYWOOD, COOL, ANGRY, EVIL, SURPRISE, CONFUSE, NETUTRAL, WONDER;
    }

    private int id;
    private String clientId;
    private String nickname;
    private String status;
    private Date joinTime;
    private String color;
    private Smile smile;

    public ChatUser() {
    }
    
    public ChatMessage createMessage() {
        ChatMessage message = new ChatMessage();
        message.setSenderId(this.getId());
        return message;
    }

    /**
     * Gets the id of this chat user.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of this chat user.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the code of a table linked to this chat user.
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Sets the code of a table linked to this chat user.
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * Gets the nickname of this chat user.
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Sets the nickname of this chat user.
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * Gets the status of this chat user.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the status of this chat user.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets the time when this user joined the chat.
     * <p>
     * This property is assigned on the server side.
     */
    public Date getJoinTime() {
        return joinTime;
    }

    /**
     * Sets the time when this user joined the chat.
     * <p>
     * This property is assigned on the server side.
     */
    public void setJoinTime(Date joinTime) {
        this.joinTime = joinTime;
    }

    /**
     * Gets the color of this chat user as a string in format "#RRGGBB".
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets the color of this chat user as a string in format "#RRGGBB".
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Gets the smile constant of this chat user.
     */
    public Smile getSmile() {
        return smile;
    }

    /**
     * Sets the smile constant of this chat user.
     */
    public void setSmile(Smile smile) {
        this.smile = smile;
    }

    /**
     * @deprecated this property is no longer used
     */
    @Deprecated
    public Boolean isActive() {
        return Boolean.TRUE;
    }

    /**
     * @deprecated this property is no longer used
     */
    @Deprecated
    public void setActive(Boolean active) {
    }

    /**
     * @deprecated use {@link #getJoinTime()} instead
     */
    public Date getStartTime() {
        return joinTime;
    }

    /**
     * @deprecated use {@link #getJoinTime()} instead
     */
    @Deprecated
    public void setStartTime(Date startTime) {
        this.joinTime = startTime;
    }

    /**
     * @deprecated this property is no longer used
     */
    @Deprecated
    public Date getEndTime() {
        return new Date(0L);
    }

    /**
     * @deprecated this property is no longer used
     */
    @Deprecated
    public void setEndTime(Date endTime) {
    }
}
