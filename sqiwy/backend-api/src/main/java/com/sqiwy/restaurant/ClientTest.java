package com.sqiwy.restaurant;


import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sqiwy.restaurant.api.BackendClient;
import com.sqiwy.restaurant.api.BackendException;
import com.sqiwy.restaurant.api.ClientConfig;
import com.sqiwy.restaurant.api.ClientConfig.SupportedLanguage;
import com.sqiwy.restaurant.api.OperationService;
import com.sqiwy.restaurant.api.data.ChatMessage;
import com.sqiwy.restaurant.api.data.ChatUser;
import com.sqiwy.restaurant.api.data.Menu;
import com.sqiwy.restaurant.api.data.NotificationType;
import com.sqiwy.restaurant.api.data.Order;
import com.sqiwy.restaurant.api.data.OrderProduct;
import com.sqiwy.restaurant.api.data.OrderProductModifier;
import com.sqiwy.restaurant.api.data.SystemParameters;
import com.sqiwy.restaurant.api.data.TableSession;


/**
 * Небольшой пример использования клиентского api
 */
public class ClientTest {
    private static final Logger log = LoggerFactory.getLogger(ClientTest.class);

    private final BackendClient client = new BackendClient();
    private OperationService operationService;
    
    private final ClientConfig config = new ClientConfig();

    public static void main(String[] args) {

        ClientTest test = new ClientTest();
        test.start();
    }

    public void start() {
        try {
//            config.setHostName("localhost");
//            config.setApiUrl("http://rest.demo.dev.sqiwy.com:8100/api/v1");
//            config.setWebUrl("http://rest.demo.dev.sqiwy.com:8100");
            config.setApiUrl("http://localhost:8100/api/v1");
            config.setWebUrl("http://localhost:8100");
            
            String clientId = "1";
            
            config.setClientId(clientId);
            config.setPassword("1234");            

            client.setConfig(config);
            client.setEventService(new ClientTestEventService(clientId));
            client.start();
            
                        
            operationService = client.getOperationService();
            
            client.setLanguage(SupportedLanguage.RU);
            
            try {
                testPing();
                testMenu();
                testSession();
                testPlaceOrder();
//                testGetOrders();
                testNotification();
                testSystemParameters();
                testReportCommercialStats();
                testChat1();
                
                boolean exit = false;
                while (!exit) {
                    String usage = "Usage:\np - ping\no - open session\nc - close session\nm - menu\n1, 2, 3, 4, 5 - test methods\nx - exit";
                    System.out.println(usage);
                    while (!exit) {
                        switch (System.in.read()) {
                            case 'x': exit = true; break;
                        }
                    }
                }
                
            } catch (BackendException e) {
                e.printStackTrace();
            }

        }
        catch (Exception e) {
            log.error("Unhandled exception", e);
        }
        finally {
            if (client != null) {
                client.close();
            }
            System.out.println("exit...");
        }
        
    }
    
    public void testPing() throws IOException, BackendException {
        operationService.ping();
    }
    
    @SuppressWarnings("unused")
    public void testMenu() throws BackendException, IOException {
        Menu m = operationService.getMenu();
        return ;
    }
    
    @SuppressWarnings("unused")
    public void testGetOrders() throws BackendException, IOException {
        List<Order> ol = operationService.getOrderList();
        return ;
    }
    
    @SuppressWarnings("deprecation")
    public void testReportCommercialStats() throws BackendException, IOException
    {
        Map<String, Integer> data = new HashMap<String, Integer>();
        data.put("ad_id", 10);
        data.put("play_time", 1024);
        data.put("click", 1);
        operationService.statData(data);
    }
    
    @SuppressWarnings("unused")
    public void testSession() throws BackendException, IOException {
        TableSession ts = operationService.openTableSession();
        Boolean result = operationService.closeTableSession();
        TableSession ts1 = operationService.openTableSession();
        TableSession ts2 = operationService.openTableSession();
    }
    
    @SuppressWarnings("deprecation")
    public void testPlaceOrder() throws BackendException, IOException {
        Order order = new Order();
        List<OrderProduct> orderProductList = new ArrayList<OrderProduct>();
        OrderProduct orderProduct = new OrderProduct();
        orderProduct.setAmount(1);
        orderProduct.setPrice(new BigDecimal("100.00"));
        orderProduct.setProductId(11);
        orderProductList.add(orderProduct);
          
        List<OrderProductModifier> orderProductModifierList = new ArrayList<OrderProductModifier>();
        OrderProductModifier m1 = new OrderProductModifier();
        m1.setModifierId(15);
        m1.setAmount(1);
//        m1.setPrice(new BigDecimal("2.00"));
        orderProductModifierList.add(m1);
  
        OrderProductModifier m2 = new OrderProductModifier();
        m2.setModifierId(16);
        m2.setAmount(1);
        m2.setPrice(new BigDecimal("5.00"));
        orderProductModifierList.add(m2);
  
        orderProduct.setModifiers(orderProductModifierList);
  
        order.setProductList(orderProductList);
        order = operationService.placeOrder(order);
    }

    public void testNotification() throws BackendException, IOException {
        operationService.callStaff(NotificationType.CALL_WAITER);
//        operationService.callStaff(NotificationType.CALL_HOOKAH);
    }
    
    @SuppressWarnings("unused")
    public void testSystemParameters() throws BackendException, IOException {
        SystemParameters sp = operationService.getSystemParameters();
        return;
    }
    
    @SuppressWarnings("unused")
    public void testChat1() throws BackendException, IOException {
        
        TableSession sess = operationService.openTableSession();
        
//        ChatUser chatUser1 = new ChatUser();
//        chatUser1.setNickname("tester");
//        chatUser1.setColor("#FFF000");
//        chatUser1.setSmile(ChatUser.Smile.ANGRY);
//        chatUser1.setStatus("testing chat with tester...");
//        operationService.createChatUser(chatUser1);
//        
//        chatUser1.setColor("#000000");
//        operationService.updateChatUser(chatUser1);
//        
//        ChatMessage m = chatUser1.createMessage();
//        m.setTargetId(0);
//        m.setText("hello");
//        operationService.sendChatMessage(m);
//        
        List<ChatUser> ul = operationService.getChatUsers();
        operationService.closeTableSession();
//        operationService.exitChat();
        
    }
    

}
