package com.sqiwy.restaurant.api.data;

/**
 * Represents a waiter.
 */
public class Waiter {

    private int id;
    private String name;

    /**
     * Gets the id of this waiter.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of this waiter.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the name of this waiter.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this waiter.
     */
    public void setName(String name) {
        this.name = name;
    }
}
