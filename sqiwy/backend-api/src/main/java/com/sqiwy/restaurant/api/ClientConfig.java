package com.sqiwy.restaurant.api;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides he {@link BackendClient} settings.
 * 
 * TODO: (think) SupportedLanguage was a hack and not a good design-desicion;
 * 
 */
public class ClientConfig {
    
    public enum SupportedLanguage {
        RU("Russian"), 
        EN("English"), 
        ZH("Chinese");
        
        private String description;
        
        SupportedLanguage(String description) {
            this.description = description;
        }
        
        public String getCode() {
            return this.toString();
        }
        
        public String getDescription() {
            return this.description;
        }
        
    }
    
    private static final Logger log = LoggerFactory.getLogger(ClientConfig.class);

    private int pollingDelay;
    private SupportedLanguage defaultLanguage = SupportedLanguage.RU;

    private static final int MIN_PORT_VALUE = 1;
    private static final int MAX_PORT_VALUE = 65535;

    private int maxPacketSize = 1048576 * 4; // 4Mb
    private String clientId;
    private String password;
    private String apiUrl;
    private String webUrl;
    

    protected Logger getLogger() {
        return log;
    }

    public SupportedLanguage getDefaultLanguage() {
        return defaultLanguage;
    }
    
    public void setDefaultLanguage(SupportedLanguage lang) {
        this.defaultLanguage = lang;
    }
    
    /**
     * Gets the client identifier. Used for client authentication.
     * @return The client identifier.
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Sets the client identifier. Used for client authentication.
     * @param clientId The client identifier.
     */
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    /**
     * Gets the client password. Used for client authentication.
     * @return The client password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the client password. Used for client authentication.
     * @param password The client password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public URL getWebUrl() throws MalformedURLException {
        return new URL(webUrl);
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public URL getApiUrl() throws MalformedURLException {
        return new URL(apiUrl);
    }

    public void setApiUrl(String apiUrl) throws MalformedURLException {
        // Testing if url is properly formed
        URL u = new URL(apiUrl);
        if (u.getHost().equals("")) {
            throw new MalformedURLException("Can not extract host from apiUrl");
        }
        this.apiUrl = apiUrl;
    }

    public void validate() {
        validateNotNegative("pollingDelay", pollingDelay);
        validateNotEmpty("clientId", clientId);
        validateNotEmpty("password", password);
        validateNotNegative("maxPacketSize", maxPacketSize);
    }

    /**
     * Gets the maximum incoming packet size in bytes. Default value is 1M.
     * TODO: make use of it
     * 
     * @return Maximum incoming packet size in bytes.
     */ 
    public int getMaxPacketSize() {
        return maxPacketSize;
    }

    /**
     * Sets the maximum incoming packet size in bytes. Default value is 1M.
     * TODO: make use of it
     * 
     * @param maxPacketSize Maximum incoming packet size in bytes.
     */
    public void setMaxPacketSize(int maxPacketSize) {
        this.maxPacketSize = maxPacketSize;
    }

    /**
     * Timeout for backentClient to connect
     */
    public int getConnectTimeout() {
        return 15 * 1000;
    }

    /**
     * Allowed time for backentClient between successfull reads (recv() operations) on socket 
     */
    public int getReadTimeout() {
        return 120 * 1000;
    }

    protected void validateNotNegative(String propertyName, int value) {
        getLogger().info(propertyName + " = " + value);
    
        if (value < 0) {
            String msg = String.format("Configuration propery '%s' = '%d' cannot be negative.", propertyName, value);
            throw new IllegalStateException(msg);
        }
    }

    protected void validateNotEmpty(String propertyName, String value) {
        getLogger().info(propertyName + " = " + value);
    
        if (value == null || value.length() == 0) {
            String msg = String.format("Configuration propery '%s' cannot be null or empty.", propertyName);
            throw new IllegalStateException(msg);
        }
    }

    protected void validatePort(String propertyName, int port) {
        getLogger().info(propertyName + " = " + port);
    
        if (port < MIN_PORT_VALUE || port > MAX_PORT_VALUE) {
            String msg = String.format("Configuration propery '%s' = '%d' must be within range %d and %d.",
                    propertyName, port, MIN_PORT_VALUE, MAX_PORT_VALUE);
            throw new IllegalStateException(msg);
        }
    }
}
