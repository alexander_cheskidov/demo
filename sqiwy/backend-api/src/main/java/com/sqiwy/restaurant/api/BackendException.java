package com.sqiwy.restaurant.api;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * The exception that is thrown when a back-end error occurs.
 */
public class BackendException extends Exception {
    private static final long serialVersionUID = -8142718933861771465L;
    private final BackendError error;

    /**
     * Returns the error that is associated with this exception. Cannot be null.
     */
    public BackendError getError() {
        return error;
    }

    /**
     * Constructs a new {@code BackendException} with the specified error code and detail message.
     * @param errorCode the error code, cannot be {@code null}
     * @param message the detail message
     */
    public BackendException(String errorCode, String message) {
        this(errorCode, message, null);
    }

    /**
     * Constructs a new {@code BackendException} with the specified error code, detail message and cause.
     * @param errorCode the error code, cannot be {@code null}
     * @param message the detail message
     * @param cause the cause
     */
    public BackendException(String errorCode, String message, Throwable cause) {
        super(message, cause);

        if (errorCode == null) {
            throw new IllegalArgumentException("error code is null");
        }
        error = new BackendError(errorCode);
    }

    /**
     * Constructs a new {@code BackendException} with the specified error and detail message.
     * @param error the error, cannot be {@code null}
     * @param message the detail message
     */
    public BackendException(BackendError error, String message) {
        this(error, message, null);
    }

    /**
     * Constructs a new {@code BackendException} with the specified error, detail message and cause.
     * @param error the error, cannot be {@code null}
     * @param message the detail message
     * @param cause the cause
     */
    public BackendException(BackendError error, String message, Throwable cause) {
        super(message, cause);

        if (error == null) {
            throw new IllegalArgumentException("error is null");
        }
        this.error = error;
    }

    /**
     * Returns a short description of this exception.
     */
    @Override
    public String toString() {
        String str = getClass().getName() + '@' + error.getCode();
        String message = getMessage();
        return message != null ? (str + ": " + message) : str;
    }

    @Override
    public void printStackTrace(PrintStream s) {
        super.printStackTrace(s);
        String errorStackTrace = error.getStackTrace();
        if (errorStackTrace != null) {
            s.println(errorStackTrace);
        }
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        super.printStackTrace(s);
        String errorStackTrace = error.getStackTrace();
        if (errorStackTrace != null) {
            s.println(errorStackTrace);
        }
    }
}
