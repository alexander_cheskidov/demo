package com.sqiwy.restaurant.api.data;

/**
 * Defines life cycle constants for the order.
 */
public enum OrderStatus {

    /** Corresponds to lifetime from creation to billing. */
    OPEN,

    /** Corresponds to lifetime from billing to payment. */
    BILL,

    /** The status after the order was paid. */
    CLOSED,

    /** The status after the order was cancelled. */
    CANCELLED
}
