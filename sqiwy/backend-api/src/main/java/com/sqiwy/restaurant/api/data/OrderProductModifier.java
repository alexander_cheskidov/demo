package com.sqiwy.restaurant.api.data;

import java.math.BigDecimal;

/**
 * Represents a modifier included to an order item.
 */
public class OrderProductModifier {
    private static final Integer DEFAULT_AMOUNT = new Integer(1);

    private Integer id;
    private Integer modifierId;
    private Integer amount = DEFAULT_AMOUNT;
    private BigDecimal price;

    /**
     * Gets the id of this order item modifier.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id of this order item modifier.
     * <p>
     * Only one of <b>id</b> or <b>modifierId</b> should be used. Specify this property to increase amount of existing
     * item modifier.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the id of modifier.
     */
    public Integer getModifierId() {
        return modifierId;
    }

    /**
     * Sets the id of modifier.
     * <p>
     * Only one of <b>id</b> or <b>modifierId</b> should be used. Specify this property to create a new item modifier.
     */
    public void setModifierId(Integer modifierId) {
        this.modifierId = modifierId;
    }

    /**
     * Gets the amount of modifier. The default value is one.
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * Sets the amount of modifier.
     * <p>
     * This property should be set by the client.
     * <p>
     * The value must be within range {@link Modifier#getMinimumAmount()} and {@link Modifier#getMaximumAmount()}. The
     * sum of all amount values for the same category of modifiers must be within range
     * {@link ProductModifierGroup#getMinimumAmount()} and {@link ProductModifierGroup#getMaximumAmount()}.
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Gets the price of modifier at the moment of order creation.
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets the price of modifier at the moment of order creation.
     * <p>
     * This property is intended to set by the server.
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Gets the total price of this order item modifier.
     */
    public BigDecimal getSubtotal() {
        return (price != null) ? price.multiply(new BigDecimal(getAmount())) : BigDecimal.ZERO;
    }
}
