package com.sqiwy.restaurant.api.data;

import java.util.ArrayList;
import java.util.List;

public class Menu {
	protected List<ProductCategory> productCategoryList;
    protected List<Product> productList;
    protected List<ModifierGroup> modifierGroupList;
    protected List<Modifier> modifierList;
    protected List<ProductModifierGroup> productModifierGroupList;

    public Menu() {
    }

    public Menu(Menu other) {
        this.productCategoryList = new ArrayList<ProductCategory>();
        this.productList = new ArrayList<Product>();
        this.modifierGroupList = new ArrayList<ModifierGroup>();
        this.modifierList = new ArrayList<Modifier>();
        this.productModifierGroupList = new ArrayList<ProductModifierGroup>();

        for (ProductCategory item : other.productCategoryList){
            this.productCategoryList.add(new ProductCategory(item));
        }
        for (Product item : other.productList){
            this.productList.add(new Product(item));
        }
        for (ModifierGroup item : other.modifierGroupList){
            this.modifierGroupList.add(new ModifierGroup(item));
        }
        for (Modifier item : other.modifierList){
            this.modifierList.add(new Modifier(item));
        }
        for (ProductModifierGroup item : other.productModifierGroupList){
            this.productModifierGroupList.add(new ProductModifierGroup(item));
        }

    }

    public List<ProductModifierGroup> getProductModifierGroupList() {
        return productModifierGroupList;
    }

    public void setProductModifierGroupList(List<ProductModifierGroup> productModifierGroupList) {
        this.productModifierGroupList = productModifierGroupList;
    }

    /**
	 * @return the productCategoryList
	 */
	public List<ProductCategory> getProductCategoryList() {
		return productCategoryList;
	}

	/**
	 * @param productCategoryList the productCategoryList to set
	 */
	public void setProductCategoryList(List<ProductCategory> productCategoryList) {
		this.productCategoryList = productCategoryList;
	}

	/**
	 * @return the productList
	 */
	public List<Product> getProductList() {
		return productList;
	}

	/**
	 * @param productList the productList to set
	 */
	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	/**
	 * @return the modifierGroupList
	 */
	public List<ModifierGroup> getModifierGroupList() {
		return modifierGroupList;
	}

	/**
	 * @param modifierGroupList the modifierGroupList to set
	 */
	public void setModifierGroupList(List<ModifierGroup> modifierGroupList) {
		this.modifierGroupList = modifierGroupList;
	}

	/**
	 * @return the modifierList
	 */
	public List<Modifier> getModifierList() {
		return modifierList;
	}

	/**
	 * @param modifierList the modifierList to set
	 */
	public void setModifierList(List<Modifier> modifierList) {
		this.modifierList = modifierList;
	}
}
