package com.sqiwy.restaurant.api.data;

import java.util.ArrayList;
import java.util.List;

public class Room {
    protected int id;
    protected String name;
    protected String mapUrl;
    protected List<Table> tableList;

    public Room() {
    }

    public Room(Room other) {
        this.id = other.id;
        this.name = other.name;
        this.mapUrl = other.mapUrl;
        this.tableList = new ArrayList<Table>();
        for (Table item : other.tableList) {
            this.tableList.add(new Table(item));
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the mapUrl
     */
    public String getMapUrl() {
        return mapUrl;
    }

    /**
     * @param mapUrl the mapUrl to set
     */
    public void setMapUrl(String mapUrl) {
        this.mapUrl = mapUrl;
    }

    /**
     * @return the tableList
     */
    public List<Table> getTableList() {
        return tableList;
    }

    /**
     * @param tableList the tableList to set
     */
    public void setTableList(List<Table> tableList) {
        this.tableList = tableList;
    }
}
