package com.sqiwy.restaurant.api;

import java.io.IOException;
import java.util.List;

import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;

import com.sqiwy.restaurant.api.data.ChatMessage;
import com.sqiwy.restaurant.api.data.ChatUser;

public interface ChatRestService {

    /**
     * Returns the list of all chat users.
     * @return the list of all chat users, cannot be {@code null}
     * @throws BackendException
     * @throws IOException
     */
    @GET("/chat")
    public List<ChatUser> getChatUsers() throws BackendException;

    /**
     * Sends the request to create a new chat user for the current table session.
     * <p>
     * This method sets the {@link ChatUser#getId()} and {@link ChatUser#getJoinTime()} properties to the values
     * assigned by the server.
     * @param chatUser the chat user to be created
     * @throws BackendException
     * @throws IOException
     */
    @Multipart
    @POST("/chat/user")
    public ChatUser createChatUser(@Part("chat_user") ChatUser chatUser) throws BackendException;

    /**
     * Sends the request to update an existing chat user.
     * @param chatUser the chat user to be updated
     * @throws BackendException
     * @throws IOException
     */
    @Multipart
    @PUT("/chat/user")
    public ChatUser updateChatUser(@Part("chat_user") ChatUser chatUser) throws BackendException;

    /**
     * Sends the request to remove the chat user for the current table session.
     */
    @POST("/chat/leave")
    public Boolean exitChat() throws BackendException;

    /**
     * Sends a new chat message.
     * @throws IOException
     * @throws BackendException
     */
    @Multipart
    @POST("/chat/message")
    public Boolean sendChatMessage(@Part("message") ChatMessage message) throws BackendException;

}
