package com.sqiwy.restaurant.api;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a map of controller methods associated with incoming event actions.
 */
public class EventMapper {
    private static final Logger log = LoggerFactory.getLogger(EventMapper.class);
    private static final String actionPrefix = TableAction.PREFIX;

    private final HashMap<String, EventMethod> methods = new HashMap<String, EventMethod>();

    public EventMethod get(String action) {
        EventMethod method = methods.get(action);
        if (method == null) {
            String msg = "Unknown event action \"" + action + '"';
            throw new IllegalArgumentException(msg);
        }
        return method;
    }

    /**
     * Maps incoming event actions to the specified array of controllers.
     */
    public void setControllers(Object... controllers) {
        for (Object controller : controllers) {
            addController(controller);
        }
    }

    /**
     * Maps incoming event actions to the specified controller.
     */
    public void addController(Object controller) {
        Class<?> controllerType = controller.getClass();

        log.info("mapping event actions to the controller {}", controllerType.getName());

        for (Method method : controllerType.getMethods()) {
            EventAction actionAttr = method.getAnnotation(EventAction.class);
            if (actionAttr != null) {

                String action = actionAttr.value();
                if (action.length() == 0) {
                    action = actionPrefix + method.getName();
                }
            
                EventMethod boundMethod = new EventMethod(controller, method);
                EventMethod duplicate = methods.put(action, boundMethod);
                if (duplicate != null) {
                    String fmt = "The event action \"%s\" is duplicated for methods [%s] and [%s]";
                    String msg = String.format(fmt, action, duplicate.getMethod(), boundMethod.getMethod());
                    throw new IllegalStateException(msg);
                }

                log.debug("event action \"{}\" is associated to the method: {}", action, method);
            }
        }
    }
}
