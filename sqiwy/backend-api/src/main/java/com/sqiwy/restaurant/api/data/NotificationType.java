package com.sqiwy.restaurant.api.data;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum NotificationType {
    OPEN("open"),
    CLOSE("close"),
    CALL_HOOKAH("call_hookah"),
    CALL_WAITER("call_waiter"),
    ORDER("order"),
    CHECK("check");
    
    private String value;
    private static Map<String, NotificationType> register 
        = new HashMap<String, NotificationType>();
    
    NotificationType(String value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return value;
    }
    
    public static NotificationType getByValue(String value) {
        return register.get(value);
    }
    
    static {
        for (NotificationType t : EnumSet.allOf(NotificationType.class)) {
            register.put(t.toString(), t);            
        }
    }
}
