package com.sqiwy.restaurant.api;

import java.util.List;

import com.sqiwy.restaurant.api.data.Menu;
import com.sqiwy.restaurant.api.data.NotificationType;
import com.sqiwy.restaurant.api.data.Order;
import com.sqiwy.restaurant.api.data.SystemParameters;
import com.sqiwy.restaurant.api.data.TableSession;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;

public interface BackendRestService {
    
    @GET("/ping")
    public Boolean ping() throws BackendException;

    @GET("/events")
    public Response getEvent() throws BackendException;

    @FormUrlEncoded
    @POST("/events")
    public Response confirmEvent(@Header("X-Id") String id, @Field("data") Object result) throws BackendException;

    @FormUrlEncoded
    @POST("/ads/statistic")
    public Boolean reportCommercialDisplay(
            @Field("ad_id") int advertisementId,
            @Field("play_time") int playTime,
            @Field("click") int click) 
        throws BackendException;
    
    @GET("/menu")
    public Menu getMenu() throws BackendException;
    
    @POST("/table/session/start")
    public TableSession startTableSession() throws BackendException;
    
    @PUT("/table/session/close")
    public Boolean closeTableSession() throws BackendException;
    
    @GET("/order")
    public List<Order> getOrders() throws BackendException;
    
    @Multipart
    @POST("/order")
    public Order placeOrder(@Part("order") Order order) throws BackendException;
    
    @FormUrlEncoded
    @PUT("/order")
    public Order editOrder(@Field("order") Order order) throws BackendException;
    
    @FormUrlEncoded
    @POST("/notification")
    public Boolean notifyStaff(@Field("notification_type") NotificationType type) throws BackendException;
    
    @GET("/system/settings")
    public SystemParameters getSystemParameters() throws BackendException;
    
    
}
