package com.sqiwy.restaurant.api;

/**
 * Defines constants for handshake procedure stages.
 */
public enum HandshakeStatus {

    /** Sent from server to client immediately after establishing a connection. */
    CHALLENGE,

    /** Sent from client to server as response to the challenge. */
    RESPONSE,

    /** Sent from server to client in case of authentication was success. */
    SUCCESS,

    /** Sent from server to client in case of authentication was failure. */
    FAILURE
}
