package com.sqiwy.restaurant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sqiwy.restaurant.api.EventService;
import com.sqiwy.restaurant.api.data.ChatMessage;
import com.sqiwy.restaurant.api.data.ChatUser;
import com.sqiwy.restaurant.api.data.ChatUserChangeType;
import com.sqiwy.restaurant.api.data.EnvironmentType;
import com.sqiwy.restaurant.api.data.Order;
import com.sqiwy.restaurant.api.data.RestartMode;
import com.sqiwy.restaurant.api.data.SystemParameters;


/**
 * Mock implementation of {@link EventService}.
 */
class ClientTestEventService implements EventService {
    private static final Logger log = LoggerFactory.getLogger(ClientTestEventService.class);
    
    private String clientId;
    public ClientTestEventService(String clientId) {
        this.clientId = clientId;
    }
    
    private String getClientIdAsPrefix()  {
        return String.format("[clientId = %s]", clientId);
    }

    public void menuChanged() {
        log.info(getClientIdAsPrefix() + "event menuChanged");
    }

    public void staffCallConfirmed() {
        log.info(getClientIdAsPrefix() + "event staffCallConfirmed");
    }

    public void tableSessionOpened() {
        log.info(getClientIdAsPrefix() + "event tableSessionOpened");
    }

    public void tableSessionClosed() {
        log.info(getClientIdAsPrefix() + "event tableSessionClosed");
    }

    public void restartRequested(RestartMode mode) {
        log.info(String.format(getClientIdAsPrefix() + "event rebootRequested %s", mode));
        if (mode == RestartMode.HARD) {
            // do hard reset
        } else if (mode == RestartMode.SOFT) {
            // do soft reset
        }
    }

    public void chatUserChanged(ChatUserChangeType type, ChatUser user) {
        log.info(String.format(getClientIdAsPrefix() + "event chatUserChanged: [%d] %s %s", user.getId(), user.getNickname(), type));
    }

    public void chatMessageReceived(ChatMessage message) {
        log.info(getClientIdAsPrefix() + "event chatMessageReceived");
        log.info(String.format("ChatUser[id=%d] said to ChatUser[id=%s]: \"%s\"", 
                message.getSenderId(), message.getTargetId(), message.getText()));
    }

    public SystemParameters getInfo() {
        SystemParameters p = new SystemParameters();
        p.setEnvironment(EnvironmentType.PRODUCTION);
        p.setTablePassword("test");
        return p;
    }

    @Override
    public void orderChanged(Order order) {
        log.info("The order is changed, id = {}", order.getId());
    }
}
