package com.sqiwy.restaurant.api.data;


/**
 * Represents a chat message.
 */
public class ChatMessage {

    /**
     * The target chat user id for broadcast messages.
     */
    public static final int BROADCAST_ID = 0;

    private int senderId;
    private int targetId;
    private String text;

    /**
     * Gets the id of the sender {@link ChatUser}.
     */
    public int getSenderId() {
        return senderId;
    }

    /**
     * Sets the id of the sender {@link ChatUser}.
     */
    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    /**
     * Gets the id of the target {@link ChatUser}. The value of {@link #BROADCAST_ID} indicates a broadcast message.
     */
    public int getTargetId() {
        return targetId;
    }

    /**
     * Sets the id of the target {@link ChatUser}. The value of {@link #BROADCAST_ID} indicates a broadcast message.
     */
    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    /**
     * Gets the text of this message.
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the text of this message.
     */
    public void setText(String text) {
        this.text = text;
    }
}