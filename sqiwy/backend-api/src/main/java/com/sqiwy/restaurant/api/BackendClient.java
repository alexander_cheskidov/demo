package com.sqiwy.restaurant.api;

import java.io.IOException;
import java.net.MalformedURLException;

import com.sqiwy.restaurant.api.ClientConfig.SupportedLanguage;


/**
 * Осуществляет соединение с сервером. Взаимодействие с библиотекой происходит через два интерфейса:
 * <ul>
 * <li> {@link OperationService} - реализуется сервером
 * <li> {@link EventService} - реализуется клиентом
 * </ul>
 */
public class BackendClient {
    
    private final BackendFacade backendFacade = new BackendFacade();


    /**
     * Gets the {@link OperationService} implementation. This service should be used after method {@link #start()} call.
     * @return The {@link OperationService} implementation.
     */
    public OperationService getOperationService() {
        return backendFacade.getOperationService();
    }
    
    /**
     * Gets the {@link BackendRestService} implementation. Config must be set before calling this method. 
     * @return The {@link BackendRestService} implementation.
     */
    public BackendRestService getRestService() {
        return backendFacade.getRestService();
    }
    
    /**
     * Sets the backend client settings. This property must be initialized before method {@link #start()} call.
     * @param config The backend client settings.
     * @throws MalformedURLException 
     */
    public void setConfig(ClientConfig config) throws MalformedURLException {
        backendFacade.setConfig(config);
    }

    /**
     * Sets the {@link EventService} implementation. This property must be initialized before method
     * {@link #start()} call.
     * @param eventService The {@link EventService} implementation.
     */
    public void setEventService(EventService eventService) {
        EventServiceAdapter adapter = new EventServiceAdapter(eventService);
        backendFacade.setIncomingControllers(adapter);
    }

    /**
     * In the current implementation starts a timer that periodically polls the server and after receiving events
     * invokes {@link EventService} methods. If the client is already started then invoking this method has no
     * effect.
     * @throws IOException
     */
    public void start() throws IOException {
        backendFacade.start();
    }

    /**
     * Turns of the timer. If the timer is already stopped then invoking this method has no effect.
     */
    public void close() {
        backendFacade.close();
    }
    
    public SupportedLanguage getLanguage() {
        return backendFacade.getLanguage();
    }
    
    public void setLanguage(SupportedLanguage language) {
        backendFacade.setLanguage(language);
    }
    

}
