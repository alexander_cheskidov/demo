package com.sqiwy.restaurant.api.data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents a guest included to an order.
 */
public class OrderGuest {
    private Integer id;
    private String name;
    private List<OrderProduct> products;

    /**
     * Gets the id of this order guest.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id of this order guest.
     * <p>
     * You can specify the guest id to append or update items of existing guest. Otherwise, a new guest will be created.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the name of this order guest.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this order guest.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the list of all order items included to this guest.
     */
    public List<OrderProduct> getProducts() {
        return products;
    }

    /**
     * Sets the list of all order items included to this guest.
     */
    public void setProducts(List<OrderProduct> products) {
        this.products = products;
    }

    /**
     * Gets the total price of all order items.
     */
    public BigDecimal getSubtotal() {
        BigDecimal subtotal = BigDecimal.ZERO;
        if (products != null) {
            for (OrderProduct product : products) {
                subtotal = subtotal.add(product.getSubtotal());
            }
        }
        return subtotal;
    }
}
