package com.sqiwy.restaurant.api.data;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum EnvironmentType {

    PRODUCTION("prod"),
    QA("qa");

    private final String value;
    private static final Map<String, EnvironmentType> register;

    EnvironmentType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static EnvironmentType getByValue(String value) {
        return register.get(value);
    }

    static {
        register = new HashMap<String, EnvironmentType>(4);
        for (EnvironmentType t : EnumSet.allOf(EnvironmentType.class)) {
            register.put(t.value, t);
        }
    }
}
