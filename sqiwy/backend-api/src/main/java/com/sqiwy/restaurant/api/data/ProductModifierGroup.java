package com.sqiwy.restaurant.api.data;

public class ProductModifierGroup {
    private int id;
    private int productId;
    private int modifierGroupId;
    private int sortIndex;
    private int minimumAmount;
    private int maximumAmount;

    public ProductModifierGroup() {
    }

    public ProductModifierGroup(ProductModifierGroup other) {
        this.id = other.id;
        this.productId = other.productId;
        this.modifierGroupId = other.modifierGroupId;
        this.sortIndex = other.sortIndex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the productId
     */
    public int getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * @return the modifierGroupId
     */
    public int getModifierGroupId() {
        return modifierGroupId;
    }

    /**
     * @param modifierGroupId the modifierGroupId to set
     */
    public void setModifierGroupId(int modifierGroupId) {
        this.modifierGroupId = modifierGroupId;
    }

    /**
     * @return the sortIndex
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * @param sortIndex the sortIndex to set
     */
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    /**
     * Gets the minimum of the summary amount of modifiers from the group which can be applied to the product.
     */
    public int getMinimumAmount() {
        return minimumAmount;
    }

    /**
     * Sets the minimum of the summary amount of modifiers from the group which can be applied to the product.
     */
    public void setMinimumAmount(int minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    /**
     * Gets the maximum of the summary amount of modifiers from the group which can be applied to the product.
     */
    public int getMaximumAmount() {
        return maximumAmount;
    }

    /**
     * Sets the maximum of the summary amount of modifiers from the group which can be applied to the product.
     */
    public void setMaximumAmount(int maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    /**
     * @deprecated use check {@code (getMinimumAmount() > 0)} instead
     * @see #getMinimumAmount()
     */
    @Deprecated
    public Boolean getRequired() {
        return (minimumAmount > 0);
    }

    /**
     * @deprecated use {@link #setMinimumAmount(int)} instead
     */
    @Deprecated
    public void setRequired(Boolean required) {
    }
}
