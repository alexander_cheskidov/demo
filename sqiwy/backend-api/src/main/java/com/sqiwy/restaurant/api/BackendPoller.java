package com.sqiwy.restaurant.api;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Polls the server for events. 
 */
public class BackendPoller implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(BackendPoller.class);
    private static final long errorDelay = 2000;

    private final Thread pollThread;
    private final AtomicBoolean started;
    private final AtomicBoolean stopped;

    private BackendRestService restService;
    private EventMapper eventMapper;
    private Gson gson;

    public void setRestService(BackendRestService restService) {
        this.restService = restService;
    }

    public void setEventMapper(EventMapper eventMapper) {
        this.eventMapper = eventMapper;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    /**
     * Initializes a new instance of {@code RestEventPoller} class.
     */
    public BackendPoller() {
        pollThread = new Thread(this, "eventsPoll");
        started = new AtomicBoolean();
        stopped = new AtomicBoolean();
    }

    /**
     * Starts polling loop in the separate thread.
     * @throws Exception
     */
    public void start() {
        if (started.getAndSet(true)) {
            log.info("already started");
            return;
        }
        pollThread.start();

        log.info("started");
    }

    /**
     * Stops the polling loop.
     */
    public void close() {
        if (stopped.getAndSet(true)) {
            log.info("already stopped");
            return;
        }
        log.info("stopping");
        try {
            pollThread.join(1000);
        }
        catch (InterruptedException ignored) {
        }
        log.info("stopped");
    }

    @Override
    public void run() {
        log.info("poll thread started");

        String prevId = null;
        Object result = null;
        while (!stopped.get()) {
            try {
                log.info("connecting to event service");

                Response response = restService.getEvent();
                Map<String, String> headers = new HashMap<String, String>();
                for (Header header : response.getHeaders()) {
                    headers.put(header.getName(), header.getValue());
                }
                String nextId = headers.get("X-Id");

                if (nextId == null) {
                    // There is no new event - do not send confirmation to the server.
                    result = null;
                    prevId = null;
                }
                else {
                    String action = headers.get("X-Action");
                    if (nextId.equals(prevId)) {
                        // Send the previous result to the server.
                        log.warn("the same event is received twice, id = {}, action = {}", nextId, action);

                    }
                    else {
                        log.info("new event is received, id = '{}', action = '{}'", nextId, action);
                        // Process event to obtain the new result.
                        result = invoke(action, response);
                        prevId = nextId;
                    }
                    restService.confirmEvent(prevId, result);
                    log.info("event is confirmed, id = '{}', result = {}", prevId, result);
                }
            }
            catch (Exception e) {
                try {
                    log.error("Polling error, chillout for {} milliseconds and try again...", errorDelay, e);
                    Thread.sleep(errorDelay);
                }
                catch (InterruptedException ignored) {
                }
            }
        }
        log.info("poll thread stopped");
    }

    /**
     * Invokes the next event.
     * @return The target method return value.
     */
    private Object invoke(String action, Response response) throws Exception {
        EventMethod method = eventMapper.get(action);
        Class<?>[] parameterTypes = method.getParameterTypes();
        Object[] args = new Object[parameterTypes.length];
        if (parameterTypes.length == 1) {
            InputStreamReader reader = null;
            try {
                reader = new InputStreamReader(response.getBody().in(), "UTF-8");
                args[0] = gson.fromJson(reader, parameterTypes[0]);
            }
            finally {
                if (reader != null) {
                    try {
                        reader.close();
                    }
                    catch (IOException ignored) {
                    }
                }
            }
        }
        return method.invoke(args);
    }
}
