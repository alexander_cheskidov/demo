package com.sqiwy.restaurant.api;

/**
 * Defines command action constants for the table API.
 */
public interface TableAction {

    /**
     * The command action prefix for the table API.
     */
    public static final String PREFIX = "table.";

    /**
     * The client request to send the ping message to the server.
     */
    public static final String PING = PREFIX + "ping";

    /**
     * The client request to get the list of all events accumulated by the server.
     */
    public static final String GET_EVENTS = PREFIX + "getEvents";

    /**
     * The client request to get the menu.
     */
    public static final String GET_MENU = PREFIX + "getMenu";

    /**
     * The client request to get the system parameters.
     */
    public static final String GET_SYSTEM_PARAMETERS = PREFIX + "getSystemParameters";

    /**
     * The client request to open a new active table session for this table.
     */
    public static final String OPEN_TABLE_SESSION = PREFIX + "openTableSession";

    /**
     * The client request to close a current active table session.
     */
    public static final String CLOSE_TABLE_SESSION = PREFIX + "closeTableSession";

    /**
     * The client request to get the list of all orders for the current table session.
     */
    public static final String GET_ORDERS = PREFIX + "getOrders";

    /**
     * The client request to create a new order for the current table session.
     */
    public static final String CREATE_ORDER = PREFIX + "createOrder";

    /**
     * The client request to call a hookah to this table.
     */
    public static final String CALL_HOOKAH = PREFIX + "callHookah";

    /**
     * The client request to call a waiter to this table.
     */
    public static final String CALL_WAITER = PREFIX + "callWaiter";

    /**
     * The client request to save the statistics.
     */
    public static final String SAVE_STATISTICS = PREFIX + "saveStatistics";

    /**
     * The client request to get the list of all chat users.
     */
    public static final String GET_CHAT_USERS = PREFIX + "getChatUsers";

    /**
     * The client request to create a new chat user for the current table session.
     */
    public static final String CREATE_CHAT_USER = PREFIX + "createChatUser";

    /**
     * The client request to update an existing chat user.
     */
    public static final String UPDATE_CHAT_USER = PREFIX + "updateChatUser";

    /**
     * The client request to remove the chat user for the current table session.
     */
    public static final String EXIT_CHAT = PREFIX + "exitChat";

    /**
     * The client request to send a new chat message.
     */
    public static final String SEND_CHAT_MESSAGE = PREFIX + "sendChatMessage";

    /**
     * The server notification about change in the map of table placements.
     */
    public static final String MAP_CHANGED = PREFIX + "mapChanged";

    /**
     * The server notification about change in the menu.
     */
    public static final String MENU_CHANGED = PREFIX + "menuChanged";

    /**
     * The server notification about change in the list of orders.
     */
    public static final String ORDER_LIST_CHANGED = PREFIX + "orderListChanged";

    /**
     * The server notification that the table session opened.
     */
    public static final String TABLE_SESSION_OPENED = PREFIX + "tableSessionOpened";

    /**
     * The server notification that the table session closed.
     */
    public static final String TABLE_SESSION_CLOSED = PREFIX + "tableSessionClosed";

    /**
     * The server notification that call of a staff was confirmed.
     */
    public static final String STAFF_CALL_CONFIRMED = PREFIX + "staffCallConfirmed";

    /**
     * The server notification that a waiter was assigned to the table.
     */
    public static final String WAITER_ASSIGNED = PREFIX + "waiterAssigned";

    /**
     * The server notification that restart requested.
     */
    public static final String RESTART_REQUESTED = PREFIX + "restartRequested";

    /**
     * The server notification that a new user joined the chat.
     */
    public static final String CHAT_USER_CREATED = PREFIX + "chatUserCreated";

    /**
     * The server notification that an existing user updated his properties.
     */
    public static final String CHAT_USER_UPDATED = PREFIX + "chatUserUpdated";

    /**
     * The server notification that an existing user left the chat.
     */
    public static final String CHAT_USER_DELETED = PREFIX + "chatUserDeleted";

    /**
     * The server notification that a chat message received.
     */
    public static final String CHAT_MESSAGE_RECEIVED = PREFIX + "chatMessageReceived";
}
