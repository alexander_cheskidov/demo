package com.sqiwy.restaurant.api.data;

import java.util.Date;

public class TableSession {
    private Integer id;
    private Date openTime;
    private Date closeTime;
    
    public Integer getId() {
        return id;
    }
    public Date getOpenTime() {
        return openTime;
    }
    public Date getCloseTime() {
        return closeTime;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }
    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }
}
