package com.sqiwy.restaurant;


import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sqiwy.restaurant.api.BackendClient;
import com.sqiwy.restaurant.api.BackendException;
import com.sqiwy.restaurant.api.ClientConfig;
import com.sqiwy.restaurant.api.data.ChatMessage;
import com.sqiwy.restaurant.api.data.ChatUser;


/**
 * Небольшой пример использования клиентского api
 */
public class ClientTestChat {
    private static final Logger log = LoggerFactory.getLogger(ClientTestChat.class);

    private BackendClient client1 = new BackendClient();
    private BackendClient client2 = new BackendClient();
    private BackendClient client3 = new BackendClient();
    
    public static void main(String[] args) {
        ClientTestChat test = new ClientTestChat();
        test.start();
    }
    
    public BackendClient createClient(String clientId) throws IOException {
        BackendClient client = new BackendClient();
        
        ClientConfig config = new ClientConfig();
        config.setApiUrl("http://rest.demo.dev.sqiwy.com:8100/api/v1");
        config.setWebUrl("http://rest.demo.dev.sqiwy.com:8100");
//        config.setApiUrl("http://localhost:8100/api/v1");
//        config.setWebUrl("http://localhost:8100");
        config.setPassword("1234");
        config.setClientId(clientId);
        
        client.setConfig(config);
        client.setEventService(new ClientTestEventService(clientId));
        client.start();
        return client;
    }

    public void start() {
        
        /**
         * Пример использования апи чата с 2 подключёнными клиентами.
         * 
         * Цикл жизни чат-юзера следующий:
         * - открывается сессия на столе
         * - создаётся чат юзер
         * - учавствует в чате
         * - варианты логаута:
         *   1. человек забил, и юзер остаётся висеть закрывается по эвенту tableSessionClosed (на сервере)
         *   2. явно выполняется команда client.getOperationService().exitChat(); 
         *   
         */
        
        try {
            /**
             * На текущий момент никто не юзает эти клиентИд, можно с ними потестить
             */
            client1 = createClient("161");
            client2 = createClient("162");
            client3 = createClient("173");
            
            List<ChatUser> ul1 = client1.getOperationService().getChatUsers();
            List<ChatUser> ul2 = client2.getOperationService().getChatUsers();
            
            
            try {
                
                boolean exit = false;
                while (!exit) {
                    System.out.println("Select case (1, 2):"); 
                    exit = true;
                    switch (System.in.read()) {
                        case '1': testCase1(); break; 
                        case '2': testCase2(); break;
                    }
                }
                
            } catch (BackendException e) {
                e.printStackTrace();
            }

        }
        catch (Exception e) {
            log.error("Unhandled exception", e);
        }
        finally {
            client1.close();
            System.out.println("exit...");
        }
        
    }
    
    public void testCase1() throws BackendException, IOException, InterruptedException {
        
        /**
         * Заранее открываю сессию на столах, без активной сессии чат работать не будет.
         * 
         * !!! Сейчас если сессия открывается - приходит событие TableSessionClosed вместо TableSessionOpened - ЭТО БАГ 
         * !!! Если сессия уже была открыта и повторно вызывается client.getOperationService().openTableSession() - не происходит ничего
         * !!! Т.е. событие TableSessionOpened не происходит никогда, можно убрать упоминания о нём (если они есть) 
         * !!! то же самое относится к TableSessionClosed - сейчас он приходит, но в следующем релизе мы не будем его слать,
         * !!! а будем ждать от стола что он сам закроет сессию (отправит на closeTableSession())
         *
         */
        
        client1.getOperationService().openTableSession();
        client2.getOperationService().openTableSession();
        
        // Его в чате не будет, но он будет получать некоторые чат эвенты
        client3.getOperationService().openTableSession(); 
        
        /**
         * Ждём что сработают эвенты tableSessionOpened (которые не сработаеют, вместо них будет TableSessionClosed)
         */
        System.in.read();
        System.in.read();
        
        
        /**
         * 
         * Каждый клиент создаёт своего чат юзера.
         * Если активный юзер в рамках текущей сессии уже создан - возвращается его id 
         * 
         * !!! При это не меняется nickname и прочие поля !!! так как это поведение 
         * ошибочное по сути, не нужно его никак учитывать в работе !!!
         * 
         */
        ChatUser chatUser1 = new ChatUser();
        chatUser1.setNickname("Коля");
        chatUser1.setColor("#000FFF");
        chatUser1.setSmile(ChatUser.Smile.ANGRY);
        chatUser1.setStatus("testing chat with tester...");
        client1.getOperationService().createChatUser(chatUser1);
        
        /**
         * Ждём что сработает эвент chatUserChanged(CREATED, ChatUser)
         * Придёт всем кроме отправителя (кроме клиента1)
         * в т.ч. придёт не чат-юзерам - т.е. клиенту3
         */
        System.out.println("\nWait for event related to chat user joined (to all except client1)...");
        System.in.read();
        
        ChatUser chatUser2 = new ChatUser();
        chatUser2.setNickname("Боря");
        chatUser2.setColor("#FFF000");
        chatUser2.setSmile(ChatUser.Smile.CONFUSE);
        chatUser2.setStatus("confuse...");
        client2.getOperationService().createChatUser(chatUser2);
        
        /**
         * Ждём что сработает эвент chatUserChanged(CREATED, ChatUser)
         * Теперь придёт всем кроме клиента2
         * в т.ч. придёт не чат-юзерам - т.е. клиенту3
         */
        System.out.println("\nWait for event related to chat user joined (for all except client2)...");
        System.in.read();
        
        
        
        
        /**
         * Получаем актуальный чат-лист, как минимум 2 юзера сейчас там должно быть,
         * оба наших юзера должны быть в нём
         */
        
        System.out.println("\nHere is a chat user list after users have been added:");
        List<ChatUser> ul1 = client1.getOperationService().getChatUsers();
        System.out.println(String.format("%d users in the chat:", ul1.size()));
        for (ChatUser u : ul1) {
            System.out.println(String.format(
                    "- [ChatUser.id=%d, ChatUser.clientId=%s] %s is in the chat", 
                    u.getId(), u.getClientId(), u.getNickname()));
        }
        
        System.in.read();
        
        
        /**
         * Client1 закрывает сессию стола, юзер который сидел за столом разлогинится из чата
         * 
         * Ждём что сработают 2 эвента (всем подключённым клиентам придёт chatUserChanged(DELETED, ChatUser))
         * и лично client1 придёт TableSessionClosed 
         *
         */
        client1.getOperationService().closeTableSession();
        System.out.println("\nWait for events TableSessionClosed (only to client1) and chatUserChanged(DELETED, ChatUser)(to all clients)...");
        System.in.read();
        
        /**
         * А client2 выходит из чата валидно.
         * Всем кроме него придёт эвент chatUserChanged(DELETED, ChatUser))
         */
        
        client2.getOperationService().exitChat();
        System.out.println("\nWait for event chatUserChanged(DELETED, ChatUser) (to all except client2)...");
        System.in.read();
        
        
        /**
         * Получаем актуальный чат-лист, должно быть на 2 юзеров меньше,
         * оба наших юзера должны в нём отсутсвовать
         */
        System.out.println("\nHere is a chat user list after users have quit:");
        List<ChatUser> ul2 = client1.getOperationService().getChatUsers();
        System.out.println(String.format("%d users in the chat:", ul2.size()));
        for (ChatUser u : ul2) {
            System.out.println(String.format(
                    "- [ChatUser.id=%d, ChatUser.clientId=%s] %s is in the chat", 
                    u.getId(), u.getClientId(), u.getNickname()));
        }
        System.in.read();
        

        
        
    }

    public void testCase2() throws BackendException, IOException {
        
        /**
         * Заранее открываю сессию на столах, без активной сессии чат работать не будет.
         */
        client1.getOperationService().openTableSession();
        client2.getOperationService().openTableSession();
        
        // Его в чате не будет, но он будет получать некоторые чат эвенты
        client3.getOperationService().openTableSession();
        
        /**
         * Ждём что сработают эвенты tableSessionOpened (которые не сработаеют, вместо них будет TableSessionClosed)
         */
        System.in.read();
        System.in.read();
        
        /**
         * 
         * Каждый клиент создаёт своего чат юзера.
         * Если активный юзер в рамках текущей сессии уже создан - возвращается его id 
         * 
         * !!! При это не меняется nickname и прочие поля
         * !!! так как это поведение ошибочное по сути, лучше никак не учитывать его
         * 
         */
        ChatUser chatUser1 = new ChatUser();
        chatUser1.setNickname("Коля");
        chatUser1.setColor("#000FFF");
        chatUser1.setSmile(ChatUser.Smile.ANGRY);
        chatUser1.setStatus("testing chat with tester...");
        client1.getOperationService().createChatUser(chatUser1);
        
        
        /**
         * Ждём что сработает эвент chatUserChanged(CREATED, ChatUser)
         * Придёт всем кроме отправителя (кроме клиента1)
         */
        System.out.println("\nWait for event related to chat user joined (to all except client1)...");
        System.in.read();
        
        ChatUser chatUser2 = new ChatUser();
        chatUser2.setNickname("Боря");
        chatUser2.setColor("#FFF000");
        chatUser2.setSmile(ChatUser.Smile.CONFUSE);
        chatUser2.setStatus("confuse...");
        client2.getOperationService().createChatUser(chatUser2);
        
        
        /**
         *  Для большей абстракции обозначим паблик чат как юзера
         *   0 - id паблик чата.
         */
        
        ChatUser publicChat = new ChatUser();
        publicChat.setId(0);
        
        /**
         * Ждём что сработает эвент chatUserChanged(CREATED, ChatUser)
         * Теперь придёт всем кроме клиента2
         */
        System.out.println("\nWait for event related to chat user joined (to all except client2)...");
        System.in.read();
        
        
        /**
         * Получаем актуальный чат-лист, как минимум 2 юзера сейчас там должно быть,
         * оба наших юзера должны быть в нём
         * обращаю внимание, что получить список чат юзеров может и не чат юзер (client3 в нашем случае)
         */
        System.out.println("\nHere is a chat user list after users have been added:");
        List<ChatUser> ul1 = client3.getOperationService().getChatUsers();
        System.out.println(String.format("%d users in the chat:", ul1.size()));
        for (ChatUser u : ul1) {
            System.out.println(String.format(
                    "- [ChatUser.id=%d, ChatUser.clientId=%s] %s is in the chat", 
                    u.getId(), u.getClientId(), u.getNickname()));
        }
        
        System.in.read();
        
        
        /**
         * Коля решился написать в общий чат
         */
        ChatMessage pubMessage = chatUser1.createMessage();
        pubMessage.setTargetId(publicChat.getId());
        pubMessage.setText("hello");
        client1.getOperationService().sendChatMessage(pubMessage);
        
        /**
         * Ждём эвент chatMessageReceived(message), который придёт всем юзерам в чате:
         * (т.е. всем кроме client3)
         */
        System.out.println("\nWait for event ChatMessageReceived (for every ChatUser)...");
        System.in.read();
        
        
        /**
         * Боря решился написать Коле
         */
        ChatMessage privateMessage = chatUser2.createMessage();
        privateMessage.setTargetId(chatUser1.getId());
        privateMessage.setText("hello Kola");
        client2.getOperationService().sendChatMessage(privateMessage);
        
        /**
         * Ждём эвент chatMessageReceived(message), который придёт и клиенту1, и клиенту2
         * если клиент2 получил этот эвент, как минимум он может расчитывать что эвент отправлен и клиенту1,
         * и с большой вероятностью расчитывать что клиент1 этот эвент получил
         */
        System.out.println("\nWait for event ChatMessageReceived (for client1 and client2)...");
        System.in.read();
        
        
        
        /**
         * Client1 закрывает сессию стола, юзер который сидел за столом разлогинится из чата
         * 
         * Ждём что сработают 2 эвента (всем подключённым клиентам придёт chatUserChanged(DELETED, ChatUser))
         * и лично client1 придёт TableSessionClosed 
         *
         */
        client1.getOperationService().closeTableSession();
        System.out.println("\nWait for events TableSessionClosed (only to client1) and chatUserChanged(DELETED, ChatUser)(to all clients)...");
        System.in.read();
        
        /**
         * А client2 выходит из чата валидно.
         * Всем кроме него придёт эвент chatUserChanged(DELETED, ChatUser))
         */
        
        client2.getOperationService().exitChat();
        System.out.println("\nWait for event chatUserChanged(DELETED, ChatUser) (to all except client2)...");
        System.in.read();
        
        
    }
    

}
