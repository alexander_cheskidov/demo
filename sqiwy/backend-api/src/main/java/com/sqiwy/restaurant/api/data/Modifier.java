package com.sqiwy.restaurant.api.data;

import java.math.BigDecimal;

/**
 * Represents a modifier which can be applied to a product.
 */
public class Modifier {

    /**
     * The default value of the minimum amount of modifier.
     */
    public static final int DEFAULT_MINIMUM_AMOUNT = 0;

    /**
     * The default value of the maximum amount of modifier.
     */
    public static final int DEFAULT_MAXIMUM_AMOUNT = 1;

    protected int id;
    protected int modifierGroupId;
    protected String name;
    protected BigDecimal price;
    protected String imgUrl;
    protected String weight;
    protected int sortIndex;
    protected String description;
    protected Boolean enabled = Boolean.FALSE;

    public Modifier() {
    }

    public Modifier(Modifier other) {
        this.id = other.id;
        this.modifierGroupId = other.modifierGroupId;
        this.name = other.name;
        this.price = other.price;
        this.imgUrl = other.imgUrl;
        this.weight = other.weight;
        this.sortIndex = other.sortIndex;
        this.description = other.description;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the modifierGroupId
     */
    public int getModifierGroupId() {
        return modifierGroupId;
    }

    /**
     * @param modifierGroupId the modifierGroupId to set
     */
    public void setModifierGroupId(int modifierGroupId) {
        this.modifierGroupId = modifierGroupId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the imgUrl
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     * @param imgUrl the imgUrl to set
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     * @return the weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * @return the sortIndex
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * @param sortIndex the sortIndex to set
     */
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Gets the minimum amount of this modifier which can be included to an order item.
     * @see OrderProductModifier#setAmount(int)
     */
    public int getMinimumAmount() {
        return DEFAULT_MINIMUM_AMOUNT;
    }

    /**
     * Gets the maximum amount of this modifier which can be included to an order item.
     * @see OrderProductModifier#setAmount(int)
     */
    public int getMaximumAmount() {
        return DEFAULT_MAXIMUM_AMOUNT;
    }
}
