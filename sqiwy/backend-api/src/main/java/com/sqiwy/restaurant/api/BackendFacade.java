package com.sqiwy.restaurant.api;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

import com.google.gson.Gson;
import com.sqiwy.restaurant.api.ClientConfig.SupportedLanguage;
import com.squareup.okhttp.OkHttpClient;

/**
 * Initializes all necessary classes to use the table API.
 */
public class BackendFacade implements Closeable {

    private OperationService operationService;
    private BackendRestService restService;
    private ChatRestService chatService;
    private BackendPoller eventPoller = new BackendPoller();
    private SupportedLanguage language;

    public OperationService getOperationService() {
        return operationService;
    }

    public BackendRestService getRestService() {
        return restService;
    }

    public ChatRestService getChatService() {
        return chatService;
    }

    public BackendPoller getEventPoller() {
        return eventPoller;
    }

    /**
     * Sets the client settings.
     * @throws MalformedURLException 
     */
    public void setConfig(ClientConfig config) throws MalformedURLException {
        language = config.getDefaultLanguage();
        this.configureRestService(config);
        
        /**
         *  Operation service may only be accessed after client is configured
         */
        operationService = new OperationService(restService, chatService);
    }

    /**
     * Sets the controllers to use for processing incoming commands.
     * @see CommandDispatcher#setIncomingControllers(Object...)
     */
    public void setIncomingControllers(Object... controllers) {
        EventMapper eventMapper = new EventMapper();
        eventMapper.setControllers(controllers);
        eventPoller.setEventMapper(eventMapper);
    }

    /**
     * Starts the polling loop.
     */
    public void start() {
        eventPoller.start();
    }

    /**
     * Stops the polling loop.
     */
    public void close() {
        eventPoller.close();
    }

    public SupportedLanguage getLanguage() {
        return language;
    }

    public void setLanguage(SupportedLanguage language) {
        this.language = language;
    }
    
    private void configureRestService(ClientConfig config) 
            throws MalformedURLException {
        
        /**
         *  TODO: following code is ugly, should be considered experimental,
         *  and needs some refactoring
         */
        
        // Registering headers setter
        RequestInterceptor requestInterceptor = new BackendRequestInterceptor(this, config);
        
        // Registering error handler
        ErrorHandler myErrorHandler = new ErrorHandler() {

            @Override
            public Throwable handleError(RetrofitError cause) {
                Response r = cause.getResponse();
                if (r == null) {
                    return new BackendException(BackendError.TRANSFER, cause.getMessage(), cause);
                }
                Map<String, String> headers = new HashMap<String, String>();
                for (retrofit.client.Header h : r.getHeaders()) {
                    headers.put(h.getName(), h.getValue());
                }
                String backendError = headers.get("X-Backend-Error");
                if (backendError == null) {
                    String errmsg = String.format("%n%n%s %d, %s%n", r.getUrl(), r.getStatus(), r.getReason());
                    errmsg = String.format("%sHeaders:%n", errmsg);
                    for (Map.Entry<String, String> kv : headers.entrySet()) {
                        errmsg += String.format("%s: %s%n", kv.getKey(), kv.getValue());
                    }
                    return new BackendException(BackendError.UNEXPECTED, errmsg, cause);
                }
                StringBuilder errorBuilder = new StringBuilder();
                
                // TODO: Note: this code had been written by a Java-rookie; 
                // maybe some optimization is necessary
                
                InputStream is = null;
                try {
                    is = r.getBody().in();
                } catch (IOException e) {
                    ;
                }
                
                String errorBody = "-- could not read error message --";
                
                if (null != is) {
                    Scanner reader = new Scanner(new BufferedInputStream(is, 64 * 1024));
                    Scanner _reader = reader.useDelimiter("\\A");
                    while (reader.hasNext()) {
                        errorBuilder.append(reader.next());
                    }
                    reader.close();
                    _reader.close();
                    errorBody = errorBuilder.toString();
                    
                }
                BackendException exc = new BackendException(backendError, errorBody, cause);
                return exc;
            }
        };

        OkHttpClient httpClient = new OkHttpClient();

        httpClient.setConnectTimeout(config.getConnectTimeout(), TimeUnit.MILLISECONDS);
        httpClient.setReadTimeout(config.getReadTimeout(), TimeUnit.MILLISECONDS);

        Gson gson = new Gson();

        RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(config.getApiUrl().toString())
            .setRequestInterceptor(requestInterceptor)
            .setErrorHandler(myErrorHandler)
            .setConverter(new GsonConverter(gson))
            .setClient(new OkClient(httpClient))
            .build();

        restService = restAdapter.create(BackendRestService.class);
        chatService = restAdapter.create(ChatRestService.class);

        eventPoller.setRestService(restService);
        eventPoller.setGson(gson);
    }
}
