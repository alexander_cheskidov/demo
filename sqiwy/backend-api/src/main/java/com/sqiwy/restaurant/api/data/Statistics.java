package com.sqiwy.restaurant.api.data;

public class Statistics {

    private int advertisementId;

    private int clickCount;

    private int playTime;

    /**
     * Gets the advertisement id for this statistics.
     */
    public int getAdvertisementId() {
        return advertisementId;
    }

    /**
     * Sets the advertisement id for this statistics.
     */
    public void setAdvertisementId(int advertisementId) {
        this.advertisementId = advertisementId;
    }

    /**
     * Gets the number of clicks for this statistics.
     */
    public int getClickCount() {
        return clickCount;
    }

    /**
     * Sets the number of clicks for this statistics.
     */
    public void setClickCount(int clickCount) {
        this.clickCount = clickCount;
    }

    /**
     * Gets the play time for this statistics, in seconds.
     */
    public int getPlayTime() {
        return playTime;
    }

    /**
     * Sets the play time for this statistics, in seconds.
     */
    public void setPlayTime(int playTime) {
        this.playTime = playTime;
    }
}
