package com.sqiwy.restaurant;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sqiwy.restaurant.api.BackendClient;
import com.sqiwy.restaurant.api.BackendException;
import com.sqiwy.restaurant.api.ClientConfig;
import com.sqiwy.restaurant.api.EventService;
import com.sqiwy.restaurant.api.OperationService;
import com.sqiwy.restaurant.api.ClientConfig.SupportedLanguage;
import com.sqiwy.restaurant.api.data.Order;
import com.sqiwy.restaurant.api.data.OrderGuest;
import com.sqiwy.restaurant.api.data.OrderProduct;
import com.sqiwy.restaurant.api.data.OrderProductModifier;
import com.sqiwy.restaurant.api.data.TableSession;

public class ClientTestOrders {
    private static final Logger log = LoggerFactory.getLogger(ClientTestOrders.class);

    private final String clientCode = "500";
    private final BackendClient client = new BackendClient();
    private final ClientConfig config = new ClientConfig();
    private final EventService eventHandler = new ClientTestEventService(clientCode) {
        @Override
        public void orderChanged(Order order) {
            log.debug("the order is changed:\n" + ClientTestOrders.this.toString(order));
        }
    };
    private OperationService operationService;

    public static void main(String[] args) {
        new ClientTestOrders().start();
    }

    private void start() {
        try {
            config.setApiUrl("http://localhost:8080/api/v1");
            config.setWebUrl("http://localhost:8080");
            config.setClientId(clientCode);
            config.setPassword("1234");

            client.setConfig(config);
            client.setLanguage(SupportedLanguage.ZH);
            client.setEventService(eventHandler);
            client.start();

            operationService = client.getOperationService();

            String usage = "Usage:\np - ping\ns - start session\nc - close session\n1 - orders list\n2 - new order\n3 - add order\nx - exit";
            System.out.println(usage);
            boolean exit = false;
            while (!exit) {
                try {
                    switch (System.in.read()) {
                    case 'p':
                        operationService.ping();
                        break;
                    case 's':
                        startSession();
                        break;
                    case 'c':
                        closeSession();
                        break;
                    case '1':
                        exit = true;
                        getOrders();
                        break;
                    case '2':
                        exit = true;
                        createOrder();
                        break;
                    case '3':
                        exit = true;
                        appendOrder(createOrder());
                        break;
                    case 'x':
                        exit = true;
                        break;
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            log.error("start error", e);
        }
        finally {
            if (client != null) {
                client.close();
            }
            System.out.println("exit...");
        }
    }
    
    private void startSession() throws BackendException, IOException {
        TableSession session = operationService.openTableSession();
        log.debug("start session id = {}", session.getId());
    }

    private void closeSession() throws BackendException, IOException {
        Boolean result = operationService.closeTableSession();
        log.debug("close session result = {}", result);
    }
    
    private void getOrders() throws BackendException, IOException {
        List<Order> orders = operationService.getOrderList();
        int size = orders.size();
        
        StringBuilder sb = new StringBuilder("orders count = ").append(size).append('\n');
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append("----------------------------------------\n");
            }
            sb.append(toString(orders.get(i)));
        }
        log.debug(sb.toString());
    }

    /**
     * Creates a new order.
     */
    private Order createOrder() throws BackendException, IOException {
        OrderProductModifier m = new OrderProductModifier();
        m.setModifierId(142);

        OrderProduct p1 = new OrderProduct();
        p1.setProductId(267);
        p1.setModifiers(Arrays.asList(m));

        OrderGuest guest1 = new OrderGuest();
        guest1.setProducts(Arrays.asList(p1));

        OrderProduct p2 = new OrderProduct();
        p2.setProductId(267);
        p2.setAmount(new BigDecimal(2));

        OrderGuest guest2 = new OrderGuest();
        guest2.setProducts(Arrays.asList(p1, p2));

        Order order = new Order();
        order.setGuests(Arrays.asList(guest1, guest2));

        log.debug("request order (total = {}):\n{}", order.getSubtotal(), toString(order));

        Order reply = operationService.placeOrder(order);

        log.debug("reply order (total = {}):\n{}", reply.getSubtotal(), toString(reply));
        return reply;
    }

    /**
     * Updates existing order.
     */
    private void appendOrder(Order oldOrder) throws BackendException, IOException {
        OrderGuest oldGuest = oldOrder.getGuests().get(0);
        OrderProduct oldProduct = oldGuest.getProducts().get(0);

        OrderProduct newProduct = new OrderProduct();
        newProduct.setId(oldProduct.getId());
        newProduct.setAmount(new BigDecimal(2));

        OrderGuest newGuest = new OrderGuest();
        newGuest.setId(oldGuest.getId());
        newGuest.setProducts(Arrays.asList(newProduct));

        Order order = new Order();
        order.setId(oldOrder.getId());
        order.setGuests(Arrays.asList(newGuest));

        log.debug("request order (total = {}):\n{}", order.getSubtotal(), toString(order));

        Order reply = operationService.placeOrder(order);

        log.debug("reply order (total = {}):\n{}", reply.getSubtotal(), toString(reply));
    }


    private String toString(Order order) {
        StringWriter buffer = new StringWriter();
        PrintWriter writer = new PrintWriter(buffer);
        writer.format("order id = %s", order.getId()).println();
        writer.format("waiter = %s", order.getWaiterName()).println();
        writer.format("status = %s", order.getStatus()).println();
        writer.format("createTime = %s", order.getCreateTime()).println();
        writer.format("finishTime = %s", order.getFinishTime()).println();
        writer.format("total = %s", order.getTotal()).println();
        for (OrderGuest guest : getList(order.getGuests())) {
            writer.format("\tguest id = %s", guest.getId()).println();
            writer.format("\tname = %s", guest.getName()).println();
            for (OrderProduct product : getList(guest.getProducts())) {
                writer.format("\t\torderProduct id = %s", product.getId()).println();
                writer.format("\t\tproductId = %s", product.getProductId()).println();
                writer.format("\t\tamount = %s", product.getAmount()).println();
                writer.format("\t\tprice = %s", product.getPrice()).println();
                for (OrderProductModifier modifier : getList(product.getModifiers())) {
                    writer.format("\t\t\torderModifier id = %s", modifier.getId()).println();
                    writer.format("\t\t\tmodifierId = %s", modifier.getModifierId()).println();
                    writer.format("\t\t\tamount = %s", modifier.getAmount()).println();
                    writer.format("\t\t\tprice = %s", modifier.getPrice()).println();
                }
            }
        }
        return buffer.toString();
    }

    private <T> List<T> getList(List<T> list) {
        if (list == null) {
            list = Collections.emptyList();
        }
        return list;
    }
}
