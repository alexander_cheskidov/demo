package com.sqiwy.restaurant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sqiwy.restaurant.api.BackendClient;
import com.sqiwy.restaurant.api.ClientConfig;
import com.sqiwy.restaurant.api.ClientConfig.SupportedLanguage;

public class ClientTestEvents {
    private static final Logger log = LoggerFactory.getLogger(ClientTestEvents.class);
    private static final String clientCode = "500";

    public static void main(String[] args) {
        BackendClient client = null;
        try {
            log.debug("test begin");

            ClientConfig config = new ClientConfig();
            config.setApiUrl("http://localhost:8080/api/v1");
            config.setWebUrl("http://localhost:8080");
            config.setClientId(clientCode);
            config.setPassword("1234");

            client = new BackendClient();
            client.setConfig(config);
            client.setLanguage(SupportedLanguage.ZH);
            client.setEventService(new ClientTestEventService(clientCode));
            client.start();

            System.out.println("Press any key to exit...");
            System.in.read();
        }
        catch (Exception e) {
            log.error("test error", e);
        }
        finally {
            if (client != null) {
                client.close();
            }
        }
    }
}
