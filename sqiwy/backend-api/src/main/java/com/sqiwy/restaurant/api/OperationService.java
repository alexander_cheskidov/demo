package com.sqiwy.restaurant.api;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.sqiwy.restaurant.api.data.ChatMessage;
import com.sqiwy.restaurant.api.data.ChatUser;
import com.sqiwy.restaurant.api.data.Menu;
import com.sqiwy.restaurant.api.data.NotificationType;
import com.sqiwy.restaurant.api.data.Order;
import com.sqiwy.restaurant.api.data.SystemParameters;
import com.sqiwy.restaurant.api.data.TableSession;

/**
 * Объявляет методы для выполнения запросов от клиента к серверу. Реализацию этого интерфейса предоставляет
 * {@link BackendClient}. Каждый метод может выбросить два типа исключения: <li> {@link BackendException} если произошло
 * исключение в серверном коде <li> {@link IOException} смотри описание {@link #ping()}
 */
public class OperationService {
    private BackendRestService restService;
    private ChatRestService chatService;

    public OperationService( 
            BackendRestService restService,
            ChatRestService chatService) {
        this.chatService = chatService;
        this.restService = restService;
    }

    /**
     * Проверяет доступность сервера. Может быть использован для тестирования коррекности настроек.
     * 
     * @throws IOException траблы с сетью
     * @throws BackendException
     * 
     * @see ClientConfig#getClientId()
     * @see ClientConfig#getPassword()
     * @see ClientConfig#getMaxPacketSize()
     */
    public void ping() throws IOException, BackendException {
        restService.ping();
    }

    /**
     * получить меню
     * @return
     */
    public Menu getMenu() throws BackendException, IOException {
        return restService.getMenu();
    }

    /**
     * вызвать персонал, например официанта к столу
     * @param type тип вызова
     */
    public void callStaff(NotificationType type) throws BackendException, IOException {
        restService.notifyStaff(type);
    }

    /**
     * вызвать персонал, например официанта к столу
     * @param type тип вызова
     */
    public Boolean callStaff(String type) throws BackendException, IOException {
        return restService.notifyStaff(NotificationType.getByValue(type));
    }

    /**
     * открыть сессию стола
     */
    public TableSession openTableSession() throws BackendException, IOException {
        return restService.startTableSession();
    }

    /**
     * Closes the table session.
     * @throws BackendException Server side error.
     * @throws IOException Transport error.
     */
    public Boolean closeTableSession() throws BackendException, IOException {
        return restService.closeTableSession();
    }

    /**
     * добавить заказ сделанный на столе
     * @param order заказ
     */
    public Order placeOrder(Order order) throws BackendException, IOException {
        return restService.placeOrder(order);
    }

    /**
     * получить список всех заказов в рамках текущей сессии стола
     * @return список заказов
     */
    public List<Order> getOrderList() throws BackendException, IOException {
        return restService.getOrders();
    }

    /**
     * обработать данные статистики
     * @deprecated Use backendClient.getRestService().reportCommercialDisplay(adId, playtime, click)
     * @param category категория данных
     * @param data данные
     */
    @Deprecated
    public void statData(Map<String, Integer> data) throws BackendException, IOException {
        Integer advertisementId = data.get("ad_id");
        Integer clickCount = data.get("click");
        Integer time = data.get("play_time");
        
        restService.reportCommercialDisplay(advertisementId, time, clickCount);
    }
    
    public SystemParameters getSystemParameters() throws BackendException, IOException {
        return restService.getSystemParameters();
    }

    /**
     * Returns the list of all chat users.
     * @return the list of all chat users, cannot be {@code null}
     * @throws BackendException
     * @throws IOException
     */
    public List<ChatUser> getChatUsers() throws BackendException, IOException {
        return chatService.getChatUsers();
    }

    /**
     * Sends the request to create a new chat user for the current table session.
     * <p>
     * This method sets the {@link ChatUser#getId()} and {@link ChatUser#getJoinTime()} properties to the values
     * assigned by the server.
     * @param chatUser the chat user to be created
     * @throws BackendException
     * @throws IOException
     */
    public void createChatUser(ChatUser chatUser) throws BackendException, IOException {
        if (chatUser == null) {
            throw new IllegalArgumentException("chatUser is null");
        }
        ChatUser responseUser = chatService.createChatUser(chatUser);
        chatUser.setId(responseUser.getId());
        chatUser.setJoinTime(responseUser.getJoinTime());
    }

    /**
     * Sends the request to update an existing chat user.
     * @param chatUser the chat user to be updated
     * @throws BackendException
     * @throws IOException
     */
    public void updateChatUser(ChatUser chatUser) throws BackendException, IOException {
        if (chatUser == null) {
            throw new IllegalArgumentException("chatUser is null");
        }
        chatService.updateChatUser(chatUser);
    }

    /**
     * Sends the request to remove the chat user for the current table session.
     */
    public void exitChat() throws BackendException, IOException {
        chatService.exitChat();
    }

    /**
     * Sends a new chat message.
     * @throws IOException
     * @throws BackendException
     */
    public void sendChatMessage(ChatMessage message) throws BackendException, IOException {
        chatService.sendChatMessage(message);
    }

}
