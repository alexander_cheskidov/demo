package com.sqiwy.restaurant.api;

import com.sqiwy.restaurant.api.data.ChatMessage;
import com.sqiwy.restaurant.api.data.ChatUser;
import com.sqiwy.restaurant.api.data.ChatUserChangeType;
import com.sqiwy.restaurant.api.data.Order;
import com.sqiwy.restaurant.api.data.RestartMode;
import com.sqiwy.restaurant.api.data.SystemParameters;

/**
 * This is an adapter for interface based incoming command handler.
 */
public class EventServiceAdapter {
    private final EventService eventHandler;

    /**
     * Construct a new instance of {@code EventServiceAdapter} with the specified incoming command handler.
     * @param eventHandler the incoming command handler
     */
    public EventServiceAdapter(EventService eventHandler) {
        this.eventHandler = eventHandler;
    }

    /**
     * Indicates that the menu was changed.
     */
    @EventAction(TableAction.MENU_CHANGED)
    public void menuChanged() {
        eventHandler.menuChanged();
    }

    /**
     * Indicates that the table session opened.
     */
    @EventAction(TableAction.TABLE_SESSION_OPENED)
    public void tableSessionOpened() {
        eventHandler.tableSessionOpened();
    }

    /**
     * Indicates that the table session closed.
     */
    @EventAction(TableAction.TABLE_SESSION_CLOSED)
    public void tableSessionClosed() {
        eventHandler.tableSessionClosed();
    }

    /**
     * Indicates that the call of a staff was confirmed.
     */
    @EventAction(TableAction.STAFF_CALL_CONFIRMED)
    public void staffCallConfirmed() {
        eventHandler.staffCallConfirmed();
    }

    /**
     * Indicates that restart requested.
     */
    @EventAction(TableAction.RESTART_REQUESTED)
    public void restartRequested(RestartMode mode) {
        eventHandler.restartRequested(mode);
    }

    /**
     * Indicates that a new user joined the chat.
     */
    @EventAction(TableAction.CHAT_USER_CREATED)
    public void chatUserCreated(ChatUser user) {
        eventHandler.chatUserChanged(ChatUserChangeType.CREATED, user);
    }

    /**
     * Indicates that an existing user updated his properties.
     */
    @EventAction(TableAction.CHAT_USER_UPDATED)
    public void chatUserUpdated(ChatUser user) {
        eventHandler.chatUserChanged(ChatUserChangeType.UPDATED, user);
    }

    /**
     * Indicates that an existing user left the chat.
     */
    @EventAction(TableAction.CHAT_USER_DELETED)
    public void chatUserDeleted(ChatUser user) {
        eventHandler.chatUserChanged(ChatUserChangeType.DELETED, user);
    }

    /**
     * Calls whenever a new chat message received.
     */
    @EventAction(TableAction.CHAT_MESSAGE_RECEIVED)
    public void chatMessageReceived(ChatMessage message) {
        eventHandler.chatMessageReceived(message);
    }
    
    @EventAction("demo.getInfo")
    public SystemParameters getInfo() {
        return eventHandler.getInfo();
    }

    @EventAction
    public void orderChanged(Order order) {
        eventHandler.orderChanged(order);
    }
}
