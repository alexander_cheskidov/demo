package com.sqiwy.restaurant.api;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

/**
 * Challenge Handshake Authentication Protocol (CHAP).
 * 
 * TODO: save this file and make use of it
 */
public class Handshake {
    private static final Charset UTF8 = Charset.forName("UTF-8");
    private static final int CHALLENGE_LENGTH = 32; // in bytes

    private static SecureRandom random;
    private static final Object randomLock = new Object();
    
    private static MessageDigest digest;
    private static final Object digestLock = new Object();

    private HandshakeStatus status;
    private byte[] value;

    public HandshakeStatus getStatus() {
        return status;
    }

    public void setStatus(HandshakeStatus status) {
        this.status = status;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }

    /**
     * Sent from server to client immediately after establishing a connection.
     */
    public static Handshake createChallenge() {
        synchronized (randomLock) {
            
            if (random == null) {
                random = new SecureRandom();
            }

            byte[] challengeBytes = new byte[CHALLENGE_LENGTH];
            random.nextBytes(challengeBytes);

            Handshake challenge = new Handshake();
            challenge.status = HandshakeStatus.CHALLENGE;
            challenge.value = challengeBytes;
            return challenge;
        }
    }

    /**
     * Sent from client to server as response to the challenge
     */
    public static Handshake createResponse(String password, Handshake challenge) {
        if (password == null) {
            throw new IllegalArgumentException("The 'password' is null.");
        }
        if (challenge == null) {
            throw new IllegalArgumentException("The 'challenge' is null.");
        }
        if (challenge.value == null) {
            throw new IllegalArgumentException("The 'challenge.value' is null.");
        }
        synchronized (digestLock) {
            
            if (digest == null) {
                try {
                    digest = MessageDigest.getInstance("SHA-256");
                }
                catch (NoSuchAlgorithmException ignored) {
                    // Every implementation of the Java platform is required
                    // to support the SHA-256 algorithm.
                }
            }

            byte[] passwordBytes = (password != null) ? password.getBytes(UTF8) : new byte[0];
            byte[] challengeBytes = challenge.value;

            digest.update(passwordBytes);
            digest.update(challengeBytes);
            byte[] responseBytes = digest.digest();

            Handshake response = new Handshake();
            response.status = HandshakeStatus.RESPONSE;
            response.value = responseBytes;
            return response;
        }
    }

    /**
     * Checks the response handshake.
     */
    public static HandshakeStatus checkResponse(String password, Handshake challenge, Handshake response) {
        if (response == null) {
            throw new IllegalArgumentException("The 'response' is null.");
        }
        if (response.value == null) {
            throw new IllegalArgumentException("The 'response.value' is null.");
        }
        Handshake check = createResponse(password, challenge);
        return Arrays.equals(check.value, response.value) ? HandshakeStatus.SUCCESS : HandshakeStatus.FAILURE;
    }
}
