package com.sqiwy.restaurant.api;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * The server sends an instance of this class to the client to indicate a request processing error.
 */
public class BackendError {

    /** These errors do not depend on the user actions. */
    public static final String SYSTEM = "error.system";

    /** Only this kind of errors depends on the user actions. */
    public static final String USER = "error.user";

    /** Indicates that an unchecked exception was occurred. This is a programmer error. */
    public static final String UNEXPECTED = SYSTEM + ".unexpected";

    /** Indicates that the server was not able to understand the client message or vice versa. */
    public static final String UNEXPECTED_PROTOCOL = UNEXPECTED + ".protocol";

    /** Indicates a transport layer error. */
    public static final String TRANSFER = SYSTEM + ".transfer";

    /** Indicates that a response was not received during a specified timeout. */
    public static final String TRANSFER_TIMEOUT = TRANSFER + ".timeout";

    /** Indicates that the handshaking procedure has failed. */
    public static final String AUTHENTICATION = SYSTEM + ".authentication";

    /** Indicates that a retrieved entity does not exist or not accessible. */
    public static final String ENTITY_NOT_FOUND = SYSTEM + ".entityNotFound";

    /** Indicates that a retrieved table does not exist or marked as deleted. */
    public static final String TABLE_NOT_FOUND = ENTITY_NOT_FOUND + ".table";

    /** Indicates that there is no an active table session. */
    public static final String TABLE_SESSION_NOT_FOUND = ENTITY_NOT_FOUND + ".tableSession";

    /** Indicates that retrieved chat user does not exist or marked as deleted. */
    public static final String CHAT_USER_NOT_FOUND = ENTITY_NOT_FOUND + ".chatUser";

    /** Indicates that a created entity already exists. */
    public static final String ENTITY_ALREADY_EXISTS = SYSTEM + ".entityAlreadyExists";

    /** Indicates that chat was banned for the current table session. */
    public static final String CHAT_BANNED = SYSTEM + ".chat.banned";

    /** Indicates that a chat nickname is used by another user. */
    public static final String CHAT_NICK_BUSY = USER + ".chat.nickBusy";

    /** Name for labeling the back-end application. */
    private static final String APP_BACKEND = "Backend";

    private String code;
    private String stackTrace;

    /**
     * Returns the code of this error.
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code of this error.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Returns the stack trace of the exception which caused this error.
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * Sets the stack trace of the exception which caused this error.
     */
    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    /**
     * Attaches the stack trace of the specified {@code Throwable} to this error.
     * @param t the {@code Throwable} to use for attach the stack trace
     */
    public void attachStackTrace(Throwable t) {
        attachStackTrace(t, APP_BACKEND);
    }

    /**
     * Attaches the stack trace of the specified {@code Throwable} to this error.
     * @param t the {@code Throwable} to use for attach the stack trace
     * @param appName name of the application that was thrown the {@code Throwable}
     */
    public void attachStackTrace(Throwable t, String appName) {
        StringWriter buf = new StringWriter();
        PrintWriter out = new PrintWriter(buf);
        if (stackTrace != null) {
            out.println(stackTrace);
        }
        out.append("Caused by ").append(appName).append(": ");
        t.printStackTrace(out);
        stackTrace = buf.toString();
    }

    /**
     * Constructs a new instance of {@code BackendError}.
     */
    public BackendError() {
    }

    /**
     * Constructs a new instance of {@code BackendError} with the specified code.
     * @param code the error code
     */
    public BackendError(String code) {
        this.code = code;
    }

    /**
     * Constructs a new instance of {@code BackendError} with the specified code and stack trace.
     * @param code the error code
     * @param stackTrace the stack trace of the exception which caused this error
     */
    public BackendError(String code, String stackTrace) {
        this.code = code;
        this.stackTrace = stackTrace;
    }

    /**
     * Returns a short description of this error.
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + '.' + code;
    }
}
