package com.sqiwy.restaurant.api.data;

public class ProductCategory {
    protected int id;
    protected Integer parentId;
    protected String name;
    protected int sortIndex;
    protected String imgUrl;
    protected Boolean simpleList = Boolean.FALSE;
    protected Boolean enabled = Boolean.FALSE;

    public ProductCategory() {
    }

    public ProductCategory(ProductCategory other) {
        this.id = other.id;
        this.parentId = other.parentId;
        this.name = other.name;
        this.sortIndex = other.sortIndex;
        this.imgUrl = other.imgUrl;
        this.simpleList = other.simpleList;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the sortIndex
     */
    public int getSortIndex() {
        return sortIndex;
    }

    /**
     * @param sortIndex the sortIndex to set
     */
    public void setSortIndex(int sortIndex) {
        this.sortIndex = sortIndex;
    }

    /**
     * @return the imgUrl
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     * @param imgUrl the imgUrl to set
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public void setSimpleList(Boolean simpleList) {
        this.simpleList = simpleList;
    }

    public Boolean getSimpleList() {
        return simpleList;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
