package com.sqiwy.restaurant.api;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Represents association between event action and controller method.
 */
public class EventMethod {
    private final Object controller;
    private final Method method;
    private final Class<?>[] parameterTypes;

    /**
     * Gets the controller method.
     */
    public Method getMethod() {
        return method;
    }

    /**
     * Gets the controller method parameter type, can be {@code null}.
     */
    public Class<?>[] getParameterTypes() {
        return parameterTypes;
    }

    public EventMethod(Object controller, Method method) {
        this.controller = controller;
        this.method = method;
        this.parameterTypes = method.getParameterTypes();
        if (this.parameterTypes.length > 1) {
            String msg = String.format("The controller method [%s] has wrong number of parameters.", method);
            throw new IllegalArgumentException(msg);
        }
    }

    /**
     * Invokes the controller method.
     */
    public Object invoke(Object... args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        return method.invoke(controller, args);
    }
}
