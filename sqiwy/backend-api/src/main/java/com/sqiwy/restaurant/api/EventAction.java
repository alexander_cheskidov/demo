package com.sqiwy.restaurant.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Maps a command action to a controller method.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface EventAction {

    /**
     * Gets the unique name of a command action. <br>
     * If the value is not set, then the name is derived from the method name.
     * @return the name of a command action
     * @see CommandHeader#getAction()
     * @see CommandHeader#setAction(String)
     */
    public String value() default "";
}
