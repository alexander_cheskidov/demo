package com.sqiwy.restaurant.api.data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents an order item included to a guest.
 */
public class OrderProduct {
    private static final BigDecimal DEFAULT_AMOUNT = BigDecimal.ONE;

    private Integer id;
    private Integer productId;
    private BigDecimal amount = DEFAULT_AMOUNT;
    private BigDecimal price;
    private List<OrderProductModifier> modifiers;
    
    /**
     * Gets the id of this order item.
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id of this order item.
     * <p>
     * Only one of <b>id</b> or <b>productId</b> should be used. Specify this property to increase amount of existing
     * order item.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets the id of product associated to this order item.
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * Sets the id of product associated to this order item.
     * <p>
     * Only one of <b>id</b> or <b>productId</b> should be used. Specify this property to create a new order item.
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * Gets the amount of product. The default value is one.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the amount of product.
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Sets the amount of product.
     * <p>
     * This property should be set by the client.
     */
    public void setAmount(int amount) {
        this.amount = new BigDecimal(amount);
    }

    /**
     * Gets the price of product at the moment of order creation.
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets the price of product at the moment of order creation.
     * <p>
     * This property is intended to set by the server.
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Gets the total price of this order item.
     */
    public BigDecimal getSubtotal() {
        BigDecimal subtotal = (price != null) ? price.multiply(getAmount()) : BigDecimal.ZERO;
        if (modifiers != null) {
            for (OrderProductModifier modifier : modifiers) {
                subtotal = subtotal.add(modifier.getSubtotal());
            }
        }
        return subtotal;
    }

    /**
     * Gets the list of all modifiers included to this order item.
     */
    public List<OrderProductModifier> getModifiers() {
        return modifiers;
    }

    /**
     * Sets the list of all modifiers included to this order item.
     */
    public void setModifiers(List<OrderProductModifier> modifiers) {
        this.modifiers = modifiers;
    }
}
