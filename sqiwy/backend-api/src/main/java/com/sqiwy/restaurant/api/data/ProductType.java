package com.sqiwy.restaurant.api.data;

/**
 * Defines type constants for the product. 
 */
public enum ProductType {
    DISH, SONG, GOOD
}
