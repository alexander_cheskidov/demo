from django.conf.urls import patterns, url
from django.http import HttpResponse

import api.actions.chat as api_chat
import api.actions.devices as api_table


def test(request, block):
    if int(block):
        import time
        time.sleep(10)
        return HttpResponse('django blocking')
    else:
        return HttpResponse('django nonblocking')

urlpatterns = patterns(

    '',

    url(r'^test/(1|0)$', test),

    url(r'^$', 'core.views.home', name='home'),

    url(r'^api/v1/chat$', api_chat.index),
    url(r'^api/v1/chat/user$', api_chat.user),
    url(r'^api/v1/chat/leave$', api_chat.leave),
    url(r'^api/v1/chat/message$', api_chat.message),

    # rest api calls
    url(r'^api/v1/ping', api_table.ping),
    url(r'^api/v1/ads/statistic', api_table.ad_display),
    url(r'^api/v1/menu', api_table.menu_actions),
    url(r'^api/v1/order', api_table.order_actions),
    url(r'^api/v1/notification', api_table.notification_actions),
    url(r'^api/v1/table/session/(start|close)', api_table.table_session_action),
    url(r'^api/v1/system/settings', api_table.system_settings),


    # waiters terminal
    url(r'^tables/$', 'workarea.views.tables'),
    url(r'^notifies/$', 'workarea.views.notifies'),
    url(r'^application/remove_notify$', 'workarea.views.remove_notify'),
    url(r'^application/getTables$', 'workarea.views.get_tables'),
    url(r'^application/updateStatus$', 'workarea.views.update_table_status'),
    url(r'^application/closeTable$', 'workarea.views.close_table'),
    url(r'^application/submitNotify$', 'workarea.views.submit_notify'),
    url(r'^application/updateNotify$', 'workarea.views.update_notify'),

    # auth
    url(r'^login/$', 'staff.views.login', name='login'),
    url(r'^login_by_pin/$', 'staff.views.login_by_pin', name='login_by_pin'),
    url(r'^login_by_pin/validate/$', 'staff.views.validate', name='validate_pin'),
    url(r'^logout/$', 'staff.views.logout', name='logout'),

    # setts
    url(r'^settings/$', 'core.views.setts', name='settings'),

    # staff
    url(r'^staff/$', 'staff.views.index'),
    url(r'^staff/add/$', 'staff.views.add'),
    url(r'^staff/edit/(\d+)$', 'staff.views.edit'),
    url(r'^staff/delete/(\d+)$', 'staff.views.delete'),

    # res
    url(r'^res/$', 'res.views.index', name='resources_list'),
    url(r'^res/add/$', 'res.views.add'),
    url(r'^res/edit/(\d+)$', 'res.views.edit'),
    url(r'^res/delete/(\d+)$', 'res.views.delete'),

    # ads
    url(r'^ads/$', 'ads.views.index'),
    url(r'^ads/statistic$', 'ads.views.statistic', name='ads_statistic'),
    url(r'^ads/add/$', 'ads.views.add'),
    url(r'^ads/edit/(\d+)$', 'ads.views.edit'),
    url(r'^ads/delete/(\d+)$', 'ads.views.delete'),

    # ads
    url(r'^ads/adv/$', 'ads.views.adv_index'),
    url(r'^ads/adv/add/$', 'ads.views.adv_add'),
    url(r'^ads/adv/edit/(\d+)$', 'ads.views.adv_edit'),
    url(r'^ads/adv/delete/(\d+)$', 'ads.views.adv_delete'),

    # menu
    url(r'^menu/$', 'menu.views.index', name='menu'),

    url(r'^menu/products/load$', 'menu.views.load_products'),
    url(r'^menu/products/edit_category$', 'menu.views.edit_product_category'),
    url(r'^menu/products/update_category$', 'menu.views.update_product_category'),
    url(r'^menu/products/delete_category$', 'menu.views.delete_product_category'),
    url(r'^menu/products/edit$', 'menu.views.edit_product'),
    url(r'^menu/products/update$', 'menu.views.update_product'),
    url(r'^menu/products/delete$', 'menu.views.delete_product'),

    url(r'^menu/modifiers/load$', 'menu.views.load_modifiers'),
    url(r'^menu/modifiers/edit_category$', 'menu.views.edit_modifier_category'),
    url(r'^menu/modifiers/update_category$', 'menu.views.update_modifier_category'),
    url(r'^menu/modifiers/delete_category$', 'menu.views.delete_modifier_category'),
    url(r'^menu/modifiers/edit$', 'menu.views.edit_modifier'),
    url(r'^menu/modifiers/update$', 'menu.views.update_modifier'),
    url(r'^menu/modifiers/delete$', 'menu.views.delete_modifier'),

    #devices
    url(r'^devices/$', 'devices.views.index', name='devices'),
    url(r'^devices/load$', 'devices.views.load'),
    url(r'^devices/edit$', 'devices.views.edit'),
    url(r'^devices/room$', 'devices.views.room'),
    url(r'^devices/delete$', 'devices.views.delete'),
    url(r'^devices/delete_room$', 'devices.views.delete_room'),
    url(r'^devices/reboot$', 'devices.views.reboot'),
    #url(r'^devices/layout$', 'devices.views.layout'),

#     url(r'^tables/room/$', 'devices.views.room_tables', name='room_tables'),
#     url(r'^tables/room/load_tables$', 'devices.views.load'),
#     url(r'^tables/room/save_table$', 'devices.views.save_room_table'),
#     url(r'^tables/room/remove_table$', 'devices.views.remove_room_table'),

    # url(r'^sqiwy_backend/', include('sqiwy_backend.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    url(r'^sdk/trigger_event/$', 'sdk.views.trigger_event'),
    url(r'^sdk/count_requests/$', 'sdk.views.count_requests'),
    url(r'^sdk/dummy_db_connect/$', 'sdk.views.dummy_db_connect'),
    url(r'^sdk/send_command/$', 'sdk.views.send_command'),
)
