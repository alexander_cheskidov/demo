import re
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from devices.models import TableSession
from menu.models import Product, Modifier
from staff.models import Staff


class OrderStatus(object):

    # Corresponds to lifetime from creation to billing.
    OPEN = u'open'

    # Corresponds to lifetime from billing to payment.
    BILL = u'bill'

    # The status after the order was paid.
    PAID = u'paid'

    # The status after the order was cancelled.
    CANCELLED = u'cancelled'


class Order(models.Model):

    class Meta:
        db_table = 'orders'

    device_session = models.ForeignKey(TableSession, related_name='orders',
                                       db_column='table_session_id')
    waiter = models.ForeignKey(Staff)
    create_time = models.DateTimeField(default=timezone.now)
    finish_time = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=30, default=OrderStatus.OPEN)
    total = models.DecimalField(max_digits=10, decimal_places=2)
    external_id = models.CharField(max_length=36, blank=True, null=True)


class OrderGuestManager(models.Manager):

    def generate(self, order, name=None, **kwargs):
        """The wrapper for models.Manager.create method.
        Set the name before guest will be created.
        @rtype: orders.models.OrderGuest
        """
        if not name:
            anonymous = unicode(_(u'Guest'))
            pattern = ur'^{} (?P<number>\d+)$'.format(anonymous)
            pattern = re.compile(pattern)
            number = 0
            for guest in order.guests.all():
                match = pattern.match(guest.name)
                if match:
                    number = max(number, int(match.group('number')))
            name = u'{} {}'.format(anonymous, number + 1)
        return self.create(order=order, name=name, **kwargs)


class OrderGuest(models.Model):

    objects = OrderGuestManager()

    class Meta:
        db_table = 'order_guests'

    order = models.ForeignKey(Order, related_name='guests')
    name = models.CharField(max_length=50)
    external_id = models.CharField(max_length=36, blank=True)

    @property
    def total(self):
        return sum(p.total for p in self.products.all())


class OrderProduct(models.Model):

    class Meta:
        db_table = 'order_products'

    order_guest = models.ForeignKey(OrderGuest, related_name='products')
    product = models.ForeignKey(Product)
    amount = models.DecimalField(max_digits=5, decimal_places=2)
    amount_done = models.DecimalField(max_digits=5, decimal_places=2)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    external_id = models.CharField(max_length=36, blank=True, null=True)

    @property
    def total(self):
        modifiers_total = sum(m.total for m in self.modifiers.all())
        return self.price * self.amount + modifiers_total


class OrderProductModifier(models.Model):

    class Meta:
        db_table = 'order_product_modifiers'

    order_product = models.ForeignKey(OrderProduct, related_name='modifiers')
    modifier = models.ForeignKey(Modifier)
    amount = models.IntegerField()
    amount_done = models.IntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    external_id = models.CharField(max_length=36, blank=True, null=True)

    @property
    def total(self):
        return self.price * self.amount
