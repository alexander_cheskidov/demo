
DEBUG = False

LANGUAGE_CODE = 'ru-ru'

STATIC_ROOT = '/home/a/work/sqiwy/static_files/'

DATABASE = {
    'CONN_MAX_AGE': 300,
    'ENGINE': 'django.db.backends.mysql',
    'NAME': 'sqiwy_restaurant',
    'USER': 'root',
    'PASSWORD': '',
    'HOST': '',
    'PORT': '',
    'OPTIONS': {'init_command': 'SET storage_engine=MyISAM;'}
}


ZH_DATABASE = {
    'CONN_MAX_AGE': 300,
    'ENGINE': 'django.db.backends.mysql',
    'NAME': 'sqiwy_restaurant_china',
    'USER': 'root',
    'PASSWORD': '',
    'HOST': '',
    'PORT': '',
    'OPTIONS': {'init_command': 'SET storage_engine=MyISAM;'}
}

RESOURCES_ROOT = '/home/a/work/sqiwy/resources/'

WEB_HOST = '0.0.0.0'
WEB_PORT = 8100


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(name)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        # 'mail_admins': {
        #     'level': 'ERROR',
        #     'filters': ['require_debug_false'],
        #     'class': 'django.utils.log.AdminEmailHandler'
        # },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False,
        },
        'console': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'sqiwy': {
            'handlers': ['console'],
            'level': 'WARN',
        },
        'sqiwy.restaurant.api': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        # 'sqiwy.restaurant.api.event_service': {
        #     'handlers': ['console'],
        #     'level': 'DEBUG',
        #     'propagate': False,
        # },

    }
}
