# -*- coding: utf-8 -*-

from django.contrib.auth import authenticate
from django import forms
from django.forms import ModelForm, widgets
from django.utils.translation import ugettext_lazy as _
from staff.models import Staff


class LoginByPinForm(forms.Form):

    pin = forms.CharField(required=True, label=_(u'ПИН код'))

    def __init__(self, *args, **kwargs):
        super(LoginByPinForm, self).__init__(*args, **kwargs)
        self._staff = None

    def clean(self):
        staff = authenticate(password=self.cleaned_data['pin'])
        if staff is None:
            raise forms.ValidationError(_(u'Неправильный пин-код'))
        assert isinstance(staff, Staff)
        if staff.is_deleted:
            raise forms.ValidationError(_(u'Пользователь был заблокирован. Обратитесь к администратору.'))
        self._staff = staff
        return self.cleaned_data

    def get_user(self):
        assert self.is_valid()
        return self._staff


class StaffForm(ModelForm):

    PIN_MIN_LENGTH = 4
    PIN_MAX_LENGTH = 10

    class Meta:
        model = Staff
        fields = ['external_id', 'role', 'name', 'email']
        widgets = {
            'external_id': widgets.HiddenInput(),
        }

    password = forms.CharField(required=True, label=_(u'Пароль'),
                               min_length=PIN_MIN_LENGTH,
                               max_length=PIN_MAX_LENGTH)

    def clean_password(self):
        # TODO: insert superglobal mutex here

        password = self.cleaned_data['password']
        # If empty, it is sure to fail validation, don't do any checks
        if password:
            if Staff.objects \
                    .exclude(pk=self.instance.pk)\
                    .filter(password__startswith=password).count() > 0:
                _msg = u'Введённый ПИН-код является началом более длинного ' \
                       u'пинкода другого участника системы, введите другой пинкод'
                raise forms.ValidationError(_(_msg))

            check_pins = []
            for i in range(self.PIN_MIN_LENGTH, len(password) + 1):
                check_pins.append(password[:i])

            if Staff.objects.exclude(pk=self.instance.pk)\
                    .filter(password__in=check_pins).count() > 0:
                _msg = u'Введённый ПИН-код перекрывает собой другой более короткий ' \
                       u'ПИН-код другого участника системы, введите другой пинкод'
                raise forms.ValidationError(_(_msg))

        return password

    def save(self, commit=True):
        passwd = self.cleaned_data.pop('password', None)
        staff = super(StaffForm, self).save(commit=False)
        if passwd:
            staff.set_password(passwd)
        if commit:
            staff.save()


class EditStaffForm(StaffForm):

    password = forms.CharField(required=False, label=_(u'Пароль'),
                               max_length=StaffForm.PIN_MAX_LENGTH)
