from __future__ import absolute_import
from django.core.management import BaseCommand, CommandError
from staff.forms import StaffForm


class Command(BaseCommand):

    def handle(self, *args, **options):
        username = raw_input("Username (spaces will be trimmed): ").strip()
        passwd = raw_input("Password (spaces will be trimmed): ").strip()
        validator = StaffForm({'login': username, 'password': passwd})
        if validator.is_valid():
            u = validator.save()
            print "User %s - %s [%s] created" % (u.id, u.name, u.role)
        else:
            raise CommandError(unicode(dict(validator.errors)))
