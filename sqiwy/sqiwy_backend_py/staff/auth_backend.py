from staff.models import Staff


class StaffAuthBackend(object):

    def authenticate(self, password=None):
        try:
            return Staff.objects.get(password=password)
        except Staff.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return Staff.objects.get(pk=user_id)
        except Staff.DoesNotExist:
            return None
