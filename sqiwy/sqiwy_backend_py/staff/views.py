# -*- coding: utf-8 -*-
import json
from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse, Http404
from django.contrib import auth

from staff.forms import LoginByPinForm, StaffForm, EditStaffForm
from staff.models import Staff
from staff.decorators import require_manager


# noinspection PyDictCreation
@require_manager
def index(request):
    context = {'staff': Staff.objects.all()}
    return render(request, 'staff/index.html', context)


# noinspection PyDictCreation
@require_manager
def add(request):
    if request.method == 'POST':
        form = StaffForm(request.POST)

        if form.is_valid():
            form.save()
            return redirect('/staff/')
    else:
        form = StaffForm()

    context = {'form': form, 'instance': None}
    return render(request, 'staff/edit.html', context)


# noinspection PyDictCreation
@require_manager
def edit(request, pk):
    member = get_object_or_404(Staff, pk=pk)
    if request.method == 'POST':
        form = EditStaffForm(request.POST, instance=member)
        if form.is_valid():
            form.save()
            return redirect('/staff/')
    else:
        form = EditStaffForm(instance=member)

    context = {}
    context['form'] = form
    context['instance'] = member
    return render(request, 'staff/edit.html', context)


@require_manager
def delete(request, pk):
    member = get_object_or_404(Staff, pk=pk)
    if request.user == member:
        raise Http404()
    if request.method == 'POST' and request.POST.get('confirm'):
        member.delete()
        return redirect('/staff/')
    elif request.method == 'GET':
        context = {'instance': member}
        return render(request, 'staff/delete.html', context)


# Authorizing views:

def login(request):
    if request.method == 'POST':
        form = LoginByPinForm(request.POST)
        if form.is_valid():
            auth.login(request, form.get_user())
            return redirect('home')
    else:
        form = LoginByPinForm()
    return render(request, 'auth/login.html', {'form': form})


def validate(request):
    try:
        Staff.objects.get(pin=request.POST.get('pin', ''))
        resp = {'status': 'ok'}
    except Staff.DoesNotExist:
        resp = {'status': 'error'}
    return HttpResponse(json.dumps(resp))


def login_by_pin(request):
    if request.method == 'POST':
        form = LoginByPinForm(request.POST)
        form.request = request
        if form.is_valid():
            return redirect('home')
    else:
        form = LoginByPinForm()
    return render(request, 'auth/login_by_pin.html', {'form': form})


def logout(request):
    auth.logout(request)
    return redirect('login')
