# -*- coding: utf-8 -*-
from django.contrib.auth.models import BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _


ROLES = {
    'manager': _(u'Менеджер'),
    'waiter': _(u'Официант'),
    'hookah': _(u'Кальянщик'),
}

ROLES_CHOICES = [(k, v) for k, v in ROLES.items()]


class StaffManager(BaseUserManager):
    def create_user(self, **kwargs):
        pass


class Staff(models.Model):

    objects = StaffManager()

    class Meta:
        db_table = 'staff'

    name = models.TextField(_(u'Имя'))
    email = models.CharField(max_length=100, blank=True)
    role = models.CharField(_('role'), max_length=32, choices=ROLES_CHOICES)
    password = models.CharField(max_length=128, null=True)
    is_deleted = models.BooleanField(default=False)
    external_id = models.CharField(max_length=36, null=True, blank=True)
    last_login = models.DateTimeField(blank=True, null=True)

    def delete(self, using=None):
        self.is_deleted = True
        self.password = None
        self.save(using=using)

    def set_password(self, raw_password):
        self.password = raw_password

    def check_password(self, raw_password):
        return raw_password == self.password

    def get_full_name(self):
        return self.name

    def is_anonymous(self):
        """
        Always returns False. This is a way of comparing User objects to
        anonymous users.
        """
        return False

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True
