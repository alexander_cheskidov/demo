from django.contrib.auth.decorators import login_required, user_passes_test


require_waiter_or_manager = login_required

require_manager = user_passes_test(lambda user: user and user.is_authenticated() and user.role == 'manager')