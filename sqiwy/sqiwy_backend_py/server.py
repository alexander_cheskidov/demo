from django.conf import settings
from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.web.wsgi import WSGIResource
from twisted.internet import reactor
from twisted.web.static import File

from init import init_app
from wsgi import application
from api.event_service import EventService
from core.log import get_logger
from external.iiko.api_v1 import PollResource, PushResource


log = get_logger('server')


class Root(Resource):

    def __init__(self, wsgi_resource):
        Resource.__init__(self)
        self.wsgi_resource = wsgi_resource
        self.iiko_poll = PollResource()
        self.iiko_push = PushResource()

    def getChild(self, path, request):
        # TODO: this seems hacky. Needs a better solution
        path0 = request.prepath.pop(0)
        request.postpath.insert(0, path0)

        # Redirecting all events requests to our asynchronous
        # EventService
        if request.path.startswith('/api/v1/events'):
            return EventService.get_instance()
        elif request.path == settings.IIKO_POLL_URL:
            return self.iiko_poll
        elif request.path == settings.IIKO_PUSH_URL:
            return self.iiko_push

        # Else return out django app
        return self.wsgi_resource


def main():

    init_app()

    reactor.suggestThreadPoolSize(50)
    wsgi_resource = WSGIResource(reactor, reactor.getThreadPool(), application)
    root = Root(wsgi_resource)
    root.putChild(settings.RESOURCES_URL.strip('/'), File(settings.RESOURCES_ROOT))
    root.putChild(settings.STATIC_URL.strip('/'), File(settings.STATIC_ROOT))

    factory = Site(root)

    log.info('Starting server at %s:%s...', settings.WEB_HOST, settings.WEB_PORT)
    reactor.listenTCP(
        settings.WEB_PORT, factory, backlog=1000,
        interface=settings.WEB_HOST)

    reactor.run()


if __name__ == '__main__':
    main()
