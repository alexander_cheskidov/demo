class BackendException(Exception):

    # TODO: constructor can be removed if we will only use child exceptions
    def __init__(self, *args):
        if not hasattr(self, 'backend_error_code'):
            self.backend_error_code = args[0]
            args = args[1:]
        Exception.__init__(self, *args)

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        result = self.backend_error_code
        if self.message:
            result = u'{}: {}'.format(result, self.message)
        return result


class ExternalError(BackendException):
    """Raised if an error occurred in external restaurant system."""

    def __init__(self, backend_error_code, remote_stack_trace, *args):
        self.backend_error_code = backend_error_code
        self.remote_stack_trace = remote_stack_trace
        super(ExternalError, self).__init__(*args)

    def __unicode__(self):
        result = super(ExternalError, self).__unicode__()
        if self.remote_stack_trace:
            result = u'{}\n{}'.format(result, self.remote_stack_trace)
        return result


class UnexpectedError(BackendException):
    """Raised to indicate unexpected behavior. This is programmer error."""
    backend_error_code = u'error.system.unexpected'


class TimeoutError(BackendException):
    """Raised if request time is expired."""
    backend_error_code = u'error.system.transfer.timeout'


class EntityNotFoundError(BackendException):
    """Raised if a retrieved entity does not exist or not accessible."""
    backend_error_code = u'error.system.entityNotFound'


class ChatUserNotFound(BackendException):
    backend_error_code = u'error.system.entityNotFound.chatUser'


class TableSessionNotFound(BackendException):
    backend_error_code = u'error.system.entityNotFound.tableSession'


class TableNotFound(BackendException):
    backend_error_code = u'error.system.entityNotFound.table'


class AuthenticationError(BackendException):
    backend_error_code = u'error.system.authentication'


class BackendSystemError(BackendException):
    backend_error_code = u'error.system'


class BackendError(object):

    """
    @deprecated
    Class is here only for informational purposes and should not be used
    """

    # These errors do not depend on the user actions.
    SYSTEM = "error.system"

    # Only this kind of errors depends on the user actions.
    USER = "error.user"

    # Indicates that an unchecked exception was occurred. This is a programmer error.
    UNEXPECTED = SYSTEM + ".unexpected"

    # Indicates that the server was not able to understand the client message or vice versa.
    UNEXPECTED_PROTOCOL = UNEXPECTED + ".protocol"

    # Indicates a transport layer error.
    TRANSFER = SYSTEM + ".transfer"

    # Indicates that a response was not received during a specified timeout.
    TRANSFER_TIMEOUT = TRANSFER + ".timeout"

    # Indicates that the handshaking procedure has failed.
    AUTHENTICATION = SYSTEM + ".authentication"

    # Indicates that a retrieved entity does not exist or not accessible.
    ENTITY_NOT_FOUND = SYSTEM + ".entityNotFound"

    # Indicates that a retrieved table does not exist or marked as deleted.
    TABLE_NOT_FOUND = ENTITY_NOT_FOUND + ".table"

    # Indicates that there is no an active table session.
    TABLE_SESSION_NOT_FOUND = ENTITY_NOT_FOUND + ".tableSession"

    # Indicates that retrieved chat user does not exist or marked as deleted.
    CHAT_USER_NOT_FOUND = ENTITY_NOT_FOUND + ".chatUser"

    # Indicates that a created entity already exists.
    ENTITY_ALREADY_EXISTS = SYSTEM + ".entityAlreadyExists"

    # Indicates that chat was banned for the current table session.
    CHAT_BANNED = SYSTEM + ".chat.banned"

    # Indicates that a chat nickname is used by another user.
    CHAT_NICK_BUSY = USER + ".chat.nickBusy"

    # Name for labeling the back-end application.
    APP_BACKEND = "Backend"
