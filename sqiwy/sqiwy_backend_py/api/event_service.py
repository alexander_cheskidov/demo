from collections import defaultdict

from twisted.web.resource import Resource
from twisted.internet.defer import DeferredQueue

from twisted.web.server import NOT_DONE_YET

from twisted.internet import reactor

from core.events import Event, Command
from api.tools import api_encode, api_decode
from core.log import get_logger


log = get_logger('event_service')


class EventService(object):
    """
    This class behaves as an interface to underlying implementations;
    the only released for now is a twisted-compatible eventService
    """

    _instance = None

    @staticmethod
    def get_instance():
        """
        @return: EventService implementation
        @rtype: EventService
        """
        if EventService._instance is None:
            raise Exception('EventService is not initialized! run init_event_service(reactor) first.')
        return EventService._instance

    def send_command(self, client_ids, command):
        raise NotImplemented()


class TwistedEventService(Resource, EventService):

    class _Command(Command):
        encoded_command = None

    def __init__(self):

        if EventService._instance is not None:
            raise Exception('EventService already started')

        # This have to be process' main thread
        EventService._instance = self
        Resource.__init__(self)
        self._queues = defaultdict(lambda: DeferredQueue())
        self._pending_confirmation = defaultdict(lambda: {})

    def _put_event(self, client_ids, event):
        for client_id in client_ids:
            log.debug('Putting event "%s [%s]" to the queue for client: %s',
                      event.action, event.correlation_id, client_id)
            self._queues[client_id].put(event)

    def send_command(self, client_ids, command):
        # Events have to be encoded prior to be put to EventService's
        # queue, so we catch any possible exceptions instantly
        command.encoded_command = api_encode(command.body)

        reactor.callFromThread(self._put_event, client_ids, command)

    def getChild(self, path, request):
        return self

    def render_GET(self, request):

        self._identify_client(request)

        if 'put' in request.args:
            # TODO: this is demo code, consider it's removal
            from devices.models import RestartMode
            from devices.events import RestartRequested
            obj = RestartRequested(RestartMode(RestartMode.HARD))
            self.send_command([request.client_id], obj)
            return 'have put "%s" to the Q' % obj

        elif 'action' in request.args:
            action = request.args['action'][0]
            if action == 'showstatus':
                if request.client_id is None:
                    return '%d events total in %d queues' % (
                        sum([len(_q.pending) for _q in self._queues.values()]),
                        len(self._queues),
                    )
                else:
                    return '%d events in queue %s ' % (len(self._queues[request.client_id].pending), request.client_id)
            elif action == 'delete':
                pass

        else:
            # Check if there are unconfirmed events and pick one
            event = self._get_pending_event(request.client_id)

            # Return unconfirmed event and be done for now
            if event is not None:
                self._finish_request(request, event)

            # Else, see the events queue
            else:
                queue = self._queues[request.client_id]
                event_getter = queue.get()

                # The following callback is actually act like a timeout callback
                # settings "timeout" to 60 seconds
                # the client will block for a specified amount of time
                canceller = reactor.callLater(60, self._on_timeout, request, queue, event_getter)
                event_getter.addCallback(self._return_event, request, canceller)
                request.notifyFinish().addErrback(self._on_request_error, request, canceller, queue, event_getter)
            return NOT_DONE_YET

    def render_POST(self, request):
        self._identify_client(request)

        correlation_id = request.getHeader('X-Id')
        if not correlation_id:
            raise ValueError(u'The HTTP header is required: X-Id')

        event = self._pop_pending_event(request.client_id, correlation_id)
        log.debug('Client %s confirmed his event %s',
                  request.client_id, event.correlation_id)

        # Handle command response
        if isinstance(event, Command):
            data = request.args.get('data')
            if data is not None:
                data = api_decode(data[0])
            event.put_result(data)

        return b''

    def _identify_client(self, request):

        if 'client_id' in request.args:
            client_id = request.args['client_id'][0]
        else:
            client_id = request.requestHeaders.getRawHeaders('X-Device-Code', None)
            if client_id is not None:
                client_id = client_id[0]

        # TODO: @identify_client should do that (and validate client_id )
        request.client_id = client_id

    def _get_pending_event(self, client_id):

        if client_id not in self._pending_confirmation:
            return None

        pending_events = self._pending_confirmation[client_id]

        try:
            return pending_events.itervalues().next()
        except StopIteration:
            return None

    def _pop_pending_event(self, client_id, correlation_id):

        if client_id not in self._pending_confirmation:
            return None

        pending_events = self._pending_confirmation[client_id]

        # Remove event from pending events
        event = pending_events.pop(correlation_id, None)

        # Remove confirmation pending queue if nothing left in it
        # NOTE: for now, dict() creation\removal is performed for every single event, which is not optimal
        if len(pending_events) < 1:
            del self._pending_confirmation[client_id]

        return event

    def _return_event(self, event, request, canceller):
        """
        Method is called when everything is supposedly went good.
        We return event to client.
        @param event: event for the client
        @param request: twisted's internet request
        @param canceller: timeout-canceller registered by us in twisted
        """

        # Cancel timeout callback:
        canceller.cancel()

        # Add event to list of events awaiting confirmation
        self._pending_confirmation[request.client_id][event.correlation_id] = event

        log.debug('Send event "%s [%s]" to client', event.action, event.correlation_id)
        assert isinstance(event, Event)
        self._finish_request(request, event)

    def _on_request_error(self, err, request, canceller, queue, event_getter):
        """
        Method is called by twisted reactor when something bad happens to
        connection.
        We have to cancel everything.
        @param err: err with description (TODO probably should log this)
        @param request:
        @param canceller: timeout-canceller registered by us in twisted
        @param queue:
        @param event_getter: a deferred task, which would call _return_event
        """
        log.debug("client %s dropped connection: %s", request.client_id, unicode(err))
        canceller.cancel()
        queue._cancelGet(event_getter)

    def _on_timeout(self, request, queue, event_getter):
        log.debug('Timeout triggered: Nothing in queue for client %s, return empty list', request.client_id)
        queue._cancelGet(event_getter)
        self._finish_request(request, None)

    def _finish_request(self, request, event):
        """Send a next event in HTTP response.
        @type request: twisted.web.server.Request
        @type event: core.events.Event
        """
        if event:
            request.setHeader('X-Id', event.correlation_id)
            request.setHeader('X-Action', event.action)
            request.write(event.encoded_command)
        request.finish()


def init_event_service():
    TwistedEventService()
