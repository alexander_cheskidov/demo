import hashlib
import json
import time
import sys
import traceback
from cStringIO import StringIO

from twisted.web.http import Request
from django.core.cache import cache
from django.http import HttpResponse, HttpRequest

from api.errors import (
    AuthenticationError, BackendSystemError, TableNotFound,
    TableSessionNotFound, BackendException, UnexpectedError)

from api.serialization import default_json_serializer
from core.log import get_logger
from devices.models import Device
from core.models import Settings


log = get_logger('api.tools')


def api_request(f):
    def dec(request, *args, **kwargs):
        """
        @type request: HttpRequest
        """
        try:
            start_time = time.time()
            log.info('Incomming api request: %s %s', request.method, request.build_absolute_uri(''))
            log.debug('Request headers: ')
            for k, v in request.META.items():
                if k.startswith('HTTP_'):
                    log.debug('%s : %s', k, v)
            log.debug('--')

            # TODO: validate language
            request.language = request.META.get('HTTP_ACCEPT_LANGUAGE', '')
            log.debug('Request language: %s', request.language)

            result = f(request, *args, **kwargs)
            log.info('Request processed in %.6f sec., starting serializing', time.time() - start_time)

            enc_took_time = -1.0
            try:
                enc_start_time = time.time()
                response_data = api_encode(result)
                enc_took_time = time.time() - enc_start_time
                return HttpResponse(response_data)
            finally:
                ex_time = time.time() - start_time
                log.info('Request finished in %.6f sec. (serializing: %.6f sec.)', ex_time, enc_took_time)
                log.info('--')

        except Exception as e:
            buff = StringIO()
            traceback.print_exc(None, buff)
            buff = buff.getvalue()
            log.error(buff)
            response = HttpResponse(buff, status=500)
            try:
                error_code = e.backend_error_code
            except AttributeError:
                error_code = UnexpectedError.backend_error_code
            response['X-Backend-Error'] = error_code
            response['Content-Type'] = 'text/plain; charset=utf-8'
            return response

    return dec


def get_password():
    pswd = cache.get('api.handshake_token', None)
    if pswd is None:
        pswd = Settings.objects.get_dict()['handshake_token']
        cache.add('api.handshake_token', pswd, 300)
    return pswd


def get_challenge_with_hash(date, client_id):
    passwd = get_password()
    glue = 'ipa$@'
    challenge = date + glue + passwd + glue + client_id
    return challenge, hashlib.sha1(challenge).hexdigest()


def identify_client(f):

    def dec(request, *args, **kwargs):

        """
        @type request: HttpRequest
        @type request.device: Table
        """
        client_id = None
        incomming_challenge = None
        request_date = None
        if isinstance(request, HttpRequest):
            incomming_challenge = request.META.get('HTTP_X_DEVICE_PASS')
            client_id = request.META.get('HTTP_X_DEVICE_CODE', '')
            request_date = request.META['HTTP_DATE']

        elif isinstance(request, Request):
            incomming_challenge = request.requestHeaders.getRawHeaders('X-Device-Pass')[0]
            client_id = request.requestHeaders.getRawHeaders('X-Device-Code')[0]
            request_date = request.requestHeaders.getRawHeaders('Date')[0]

        if incomming_challenge is None:
            raise AuthenticationError('Incomming challenge == null')

        log.debug("Incomming challenge %s", incomming_challenge)

        request.client_id = client_id

        challenge, challenge_sha1 = get_challenge_with_hash(request_date, client_id)
        log.debug('Challenge build data: sha1("%s") = %s ', challenge, challenge_sha1)

        if challenge_sha1 != incomming_challenge:
            raise AuthenticationError('Failed challenge')

        return f(request, *args, **kwargs)
    return dec


def api_encode(data):
    # For now always serializing using json dumps
    indent = None
    # if settings.DEBUG:
    #     indent = 4
    log.debug("Encoding data...")
    try:
        enc_data = default_json_serializer.encode(data)
    except Exception as e:
        log.error(unicode(e), exc_info=True)
        raise BackendSystemError()
    log.debug("Encoded data:")
    log.debug(enc_data)
    log.debug("--")
    return enc_data


def api_decode(data):
    # For now always deserializing using json loads
    log.debug("Decoding data: %s", data)
    log.debug("--")
    j = json.JSONDecoder()
    # for converter in converters:
    #     pass
    return j.decode(data)


def identify_device(f=None, ensure_active_session=False):
    def wrap(_f):
        def dec(request, *args, **kwargs):

            try:
                device = Device.objects.get(code=request.client_id)
            except Device.DoesNotExist:
                raise TableNotFound()

            if ensure_active_session:
                session = device.get_active_session()
                if not session:
                    raise TableSessionNotFound()

            device.update_last_access()
            request.device = device
            return _f(request, *args, **kwargs)
        return dec
    if f is None:
        return wrap
    else:
        return wrap(f)