"""
This is the only module besides .v1.py in api package which should know about other modules and
classes.
"""

from __future__ import absolute_import
from decimal import Decimal
import json
import datetime

from django.db.models.fields.files import ImageFieldFile

from core.events import Event
from core.log import get_logger


log = get_logger('api.serialization')


def encode_datetime(o):
    """
    @type o: datetime.datetime
    """
    return o.strftime('%Y-%m-%dT%H:%M:%SZ')


class GenericConverter(object):

    @property
    def types(self):
        return {}

    @property
    def encoders(self):
        return {
            datetime.datetime: encode_datetime,
            Decimal: str,
            ImageFieldFile: str,
        }


class JsonSerializer(json.JSONEncoder):

    def __init__(self, *args, **kwargs):
        super(JsonSerializer, self).__init__(*args, **kwargs)
        self._encoders = {}
        self._types = {}

    def set_converter(self, converter):
        self._encoders.update(converter.encoders)
        self._types.update(converter.types)

    def default(self, o):

        # Others are decoded by related callback
        if o.__class__ in self._encoders:
            return self._encoders[o.__class__](o)
        return super(JsonSerializer, self).default(o)


default_json_serializer = JsonSerializer()
default_json_serializer.set_converter(GenericConverter())


def _install_converters():
    from api.converters import chat, devices, orders, menu

    # Setting up converters for api calls
    default_json_serializer.set_converter(chat)
    default_json_serializer.set_converter(menu)
    default_json_serializer.set_converter(devices)
    default_json_serializer.set_converter(orders)

_install_converters()
