from __future__ import absolute_import
from django.core.exceptions import ObjectDoesNotExist

from api.errors import ChatUserNotFound
from api.models.chat import get_chat_user
from api.tools import api_request, identify_client, api_decode, identify_device
from core.log import get_logger
from chat.models import ChatUser


log = get_logger('api.actions.chat')


@api_request
@identify_client
@identify_device
def index(request):
    if request.method == 'GET':
        chat_users = ChatUser.objects.get_active_users()
        return chat_users


@api_request
@identify_client
@identify_device(ensure_active_session=True)
def user(request):

    def _set_chat_user_data(_u, data):
        _u.nickname = data['nickname']
        _u.smile = data['smile']
        _u.color = data['color']
        _u.status = data['status']
        return _u

    if request.method == 'POST':
        chat_user_data = api_decode(request.POST['chat_user'])
        chat_user = _set_chat_user_data(ChatUser(), chat_user_data)
        chat_user.save()
        request.device.get_active_session().set_chat_user(chat_user)
        chat_user.notify_created()
        return chat_user

    if request.method == 'PUT':
        chat_user_data = api_decode(request.PUT['chat_user'])
        try:
            chat_user = ChatUser.objects.get(id=chat_user_data['id'])
        except ObjectDoesNotExist:
            raise ChatUserNotFound()
        chat_user = _set_chat_user_data(chat_user, chat_user_data)
        chat_user.save()
        chat_user.notify_updated()
        return chat_user


@api_request
@identify_client
@identify_device(ensure_active_session=True)
def leave(request):

    if request.method == 'POST':
        session = request.device.get_active_session()
        if not session.chat_user:
            raise ChatUserNotFound()

        session.chat_user.leave()
        return True


@api_request
@identify_client
@identify_device(ensure_active_session=True)
def message(request):

    if request.method == 'POST':

        chat_message = api_decode(request.POST['message'])
        sender = get_chat_user(chat_message['senderId'])
        if chat_message['targetId']:
            target = get_chat_user(chat_message['targetId'])
        else:
            target = None # == Public chat

        chat_message = sender.create_message(target, chat_message['text'])
        chat_message.send()
        return True
