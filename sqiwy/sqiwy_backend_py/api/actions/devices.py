from __future__ import absolute_import
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _

from ads.models import Statistic
from ads.models import Ad
from api.errors import EntityNotFoundError, TableSessionNotFound
from api.tools import api_request, identify_client, api_decode, identify_device
from core.log import get_logger
from core.models import Settings
from external.services import ExternalService
from devices.models import TableSession
from menu.models import Product, ProductCategory, Modifier, ModifierCategory
from menu.models import ProductModifierCategory
from workarea.models import Notification, NOTIFICATION_TYPE


log = get_logger('api.actions.devices')


@api_request
@identify_client
@identify_device
def table_report(request):
    pass


@api_request
@identify_client
@identify_device
def ping(request):
    pass


@api_request
@identify_client
@identify_device
def system_settings(request):
    if request.method == 'GET':
        return Settings()


@api_request
@identify_client
@identify_device
def table_session_action(request, action):

    table = request.device

    if request.method == 'POST':
        if action == 'start':
            Notification.objects.create_notification(table, NOTIFICATION_TYPE.OPEN)
            return table.start_session(request.language)

    if request.method == 'PUT':
        if action == 'close':
            active_session = request.device.get_active_session()
            if active_session:
                Notification.objects.create_notification(table, NOTIFICATION_TYPE.CLOSE)
                active_session.close()
                return True
            else:
                raise TableSessionNotFound()


@api_request
@identify_client
@identify_device
def menu_actions(request):

    # TODO: quickhack, remove when localization is done
    _using_db = request.language.upper()

    if request.method == 'GET':
        categories = list(ProductCategory.objects.using(_using_db).all())
        products = list(Product.objects.using(_using_db).all())
        modifier_categories = list(ModifierCategory.objects.using(_using_db).all())
        modifiers = list(Modifier.objects.using(_using_db).all())
        products_modifier_categories = list(ProductModifierCategory.objects.using(_using_db).all())
        menu_data = {
            'productCategoryList': categories,
            'productList': products,
            'modifierGroupList': modifier_categories,
            'modifierList': modifiers,
            'productModifierGroupList': products_modifier_categories,
        }
        return menu_data


@api_request
@identify_client
@identify_device
def ad_display(request):
    try:
        ad = Ad.objects.get(id=request.POST['ad_id'])
    except Ad.DoesNotExist:
        raise EntityNotFoundError()
    Statistic.objects.add_entry(
        request.device, ad,
        request.POST['play_time'],
        request.POST['click'])


@api_request
@identify_client
@identify_device(ensure_active_session=True)
def order_actions(request):

    if request.method == 'GET':
        orders_list = []
        # TODO: session should be specified in a request, not guessed
        # by backend
        table_session = request.device.get_active_session()
        qs = table_session.orders.all()
        qs = qs.prefetch_related('guests')
        orders_list = list(qs)
        return orders_list

    if request.method == 'PUT':
        # Edit the order
        # TODO: implement
        return None

    if request.method == 'POST':
        # Create or update the order
        order_data = api_decode(request.POST['order'])
        # Remove this when devices will be send correct order id
        try:
            del order_data['id']
        except KeyError:
            pass
        session = request.device.get_active_session()
        service = ExternalService.get_instance()
        try:
            return service.save_order_part(order_data, session)
        except TableSession.DoesNotExist as e:
            raise TableSessionNotFound(e)
        except ObjectDoesNotExist as e:
            raise EntityNotFoundError(e)


@api_request
@identify_client
@identify_device
def notification_actions(request):
    if request.method == 'POST':
        notification = Notification.objects.create_notification(
            request.device, request.POST['notification_type'])
        if notification.notification_type == NOTIFICATION_TYPE.CALL_WAITER:
            message = _(u'A waiter called from the table "{}"').format(request.device.name)
            ExternalService.get_instance().send_message(message)
