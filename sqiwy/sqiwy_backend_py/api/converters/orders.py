from __future__ import absolute_import
from orders.models import Order, OrderProduct, OrderGuest, OrderProductModifier


def encode_order(order):
    """
    @type order: Order
    """
    return {
        'id': order.id,
        'waiterName': order.waiter.name,
        'status': order.status.upper(),
        'createTime': order.create_time,
        'finishTime': order.finish_time,
        'total': order.total,
        'guests': list(order.guests.all())
    }


def encode_order_guest(order_guest):

    """
    @type order_guest: OrderGuest
    """
    return {
        'id': order_guest.id,
        'name': order_guest.name,
        'products': list(order_guest.products.all())
    }


def encode_order_product(order_product):
    """
    @type order_product: OrderProduct
    """
    return {
        'id': order_product.id,
        'productId': order_product.product.id,
        'amount': order_product.amount,
        'price': order_product.price,
        'modifiers': list(order_product.modifiers.all())
    }


def encode_order_product_modifier(order_product_modifier):
    """
    @type order_product_modifier: OrderProductModifier
    """
    return {
        'id': order_product_modifier.id,
        'modifierId': order_product_modifier.modifier.id,
        'amount': order_product_modifier.amount,
        'price': order_product_modifier.price,
    }


encoders = {
    Order: encode_order,
    OrderGuest: encode_order_guest,
    OrderProduct: encode_order_product,
    OrderProductModifier: encode_order_product_modifier,
}

types = {
    Order: 'Order',
}
