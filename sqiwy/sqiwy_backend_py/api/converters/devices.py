from __future__ import absolute_import
from devices.models import RestartMode, TableSession
from core.models import Settings


def encode_table_session(o):
    return {
        'id': o.id,
        'openTime': o.start_time,
    }


def encode_restart_mode(o):
    return o.get_mode()


def encode_settings(o):
    app_settings = Settings.objects.get_dict()
    env_map = {
        'qa': 'QA',
        'prod': 'PRODUCTION',
    }
    return {
        'tablePassword': app_settings['table_password'],
        'environment': env_map[app_settings['environment']],
    }


encoders = {
    TableSession: encode_table_session,
    RestartMode: encode_restart_mode,
    Settings: encode_settings,
}

types = {
    RestartMode: 'RestartMode',
    TableSession: 'TableSession',
}
