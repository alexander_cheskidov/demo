from __future__ import absolute_import
from chat.models import ChatUser, ChatMessage


def encode_chat_user(o):
    assert isinstance(o, ChatUser)
    device = o.device
    return {
        'id': o.id,
        'clientId': device and device.code,
        'nickname': o.nickname,
        'status': o.status,
        'joinTime': o.join_time,
        'color': o.color,
        'smile': o.smile,
    }


def encode_chat_messge(o):
    return {
        'id': o.id,
        'senderId': o.sender_id,
        'targetId': o.target_id,
        'text': o.text
    }


encoders = {
    ChatUser: encode_chat_user,
    ChatMessage: encode_chat_messge,
}


types = {
    ChatUser: 'ChatUser',
    ChatMessage: 'ChatMessage',
}
