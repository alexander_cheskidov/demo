from __future__ import absolute_import
from menu.models import ProductCategory, Product, ModifierCategory, Modifier, ProductModifierCategory


def encode_product_category(category):
    """
    @type category: ProductCategory
    """
    return {
        'id': category.id,
        'parentId': category.parent_id,
        'name': category.name,
        'sortIndex': category.sort_index,
        'imgUrl': category.image,
        'simpleList': category.is_simple_list,
        'enabled': True,
    }


def encode_product(product):
    """
    @type product: Product
    """
    return {
        'id': product.id,
        'productCategoryId': product.category_id,
        'name': product.name,
        'significance': product.significance,
        'price': product.price,
        'imgUrl': product.image,
        'sortIndex': product.sort_index,
        'weight': product.weight,
        'type': product.type,
        'fullname': product.name,
        'description': product.description,
        'enabled': product.is_enabled,
        'caloricity': product.caloricity,
    }


def encode_modifier_category(modifier_category):
    """
    @type modifier_category: ModifierCategory
    """
    return {
        'id': modifier_category.id,
        'parentId': modifier_category.parent_id,
        'name': modifier_category.name,
        'type': modifier_category.type,
        'imgUrl': modifier_category.image,
        'sortIndex': modifier_category.sort_index,
        'enabled': True,
    }


def encode_modifier(modifier):
    """
    @type modifier: Modifier
    """
    return {
        'id': modifier.id,
        'modifierGroupId': modifier.category_id,
        'name': modifier.name,
        'price': modifier.price,
        'imgUrl': modifier.image,
        'weight': modifier.weight,
        'sortIndex': modifier.sort_index,
        'description': modifier.description,
        'enabled': modifier.is_enabled,
    }


def encode_product_modifier_category(product_modifier_category):
    """
    @type product_modifier_category: ProductModifierCategory
    """
    return {
        'id': product_modifier_category.id,
        'productId': product_modifier_category.product_id,
        'modifierGroupId': product_modifier_category.modifier_category_id,
        'sortIndex': product_modifier_category.sort_index,
        'minimumAmount': product_modifier_category.min_amount,
        'maximumAmount': product_modifier_category.max_amount,
    }


encoders = {
    ProductCategory: encode_product_category,
    Product: encode_product,
    ModifierCategory: encode_modifier_category,
    Modifier: encode_modifier,
    ProductModifierCategory: encode_product_modifier_category,
}


types = {}
