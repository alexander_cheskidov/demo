# -*- coding: utf-8 -*-
import json
from django.db import transaction
from django.db.models import Count
from django.template.response import TemplateResponse, HttpResponse
from menu.forms import ProductForm, ProductCategoryForm, ModifierForm, ModifierCategoryForm
from menu.forms import ProductModifierCategoryList
from menu.models import Product, ProductCategory, Modifier, ModifierCategory
from staff.decorators import require_manager

VALIDATION_ERROR = 422


def encode_product_tree(categories):
    """
    Returns a JSON representation of whole products tree.
    """
    tree = []
    for category in categories:
        assert isinstance(category, ProductCategory)
        children = encode_product_tree(category.subcategories.all())
        for product in category.items.annotate(has_modifiers=Count('productmodifiercategory')):
            children.append({
                'id': product.id,
                'is_enabled': product.is_enabled,
                'sort_index': product.sort_index,
                'name': product.name,
                'price': '%.2f' % (product.price if product.price else 0),
                'weight': product.weight,
                'no_image': not product.image,
                'has_modifiers': product.has_modifiers,
            })
        tree.append({
            'id': category.id,
            'sort_index': category.sort_index,
            'name': category.name,
            'no_image': not category.image,
            'children': children,
        })
    return tree


def encode_modifier_tree(categories):
    """
    Returns a JSON representation of whole modifiers tree.
    """
    tree = []
    for category in categories:
        assert isinstance(category, ModifierCategory)
        children = encode_modifier_tree(category.subcategories)
        for modifier in category.items.all():
            children.append({
                'id': modifier.id,
                'is_enabled': modifier.is_enabled,
                'sort_index': modifier.sort_index,
                'name': modifier.name,
                'price': str(modifier.price) if modifier.price is not None else '',
                'min_amount': modifier.min_amount,
                'max_amount': modifier.max_amount
            })
        tree.append({
            'id': category.id,
            'sort_index': category.sort_index,
            'name': category.name,
            'min_amount': category.min_amount,
            'max_amount': category.max_amount,
            'children': children
        })
    return tree


def update_menu_item(request, item_cls, form_cls):
    """Saves changes for EasyUI.datagrid inline editing."""
    instance = item_cls.objects.get(pk=request.POST['id'])
    field = request.POST['field']
    value = request.POST['value']
    if field == 'is_enabled':
        data = {}
        if value == 'true':
            data['is_enabled'] = True
    else:
        data = {field: value}
        if getattr(instance, 'is_enabled', None):
            data['is_enabled'] = True
    form = form_cls(data, instance=instance)
    if form.is_valid():
        instance = form.save()
        return HttpResponse(instance.id)
    else:
        errors = form.errors[field]
        return HttpResponse(errors, status=VALIDATION_ERROR)


def delete_menu_item(request, item_cls):
    pk = request.POST['id']
    instance = item_cls.objects.get(pk=pk)
    instance.delete()
    return HttpResponse()


@require_manager
def index(request):
    return TemplateResponse(request, 'menu/index.html')


@require_manager
def load_products(request):
    tree = encode_product_tree(ProductCategory.objects.topcategories())
    return HttpResponse(json.dumps(tree))


@require_manager
def load_modifiers(request):
    tree = encode_modifier_tree(ModifierCategory.objects.topcategories())
    return HttpResponse(json.dumps(tree))


@require_manager
def edit_product_category(request):
    if request.method == 'POST':
        pk = request.POST['id']
        prodcat = ProductCategory.objects.get(pk=pk) if pk else ProductCategory()
        form = ProductCategoryForm(request.POST, request.FILES, instance=prodcat)
        if form.is_valid():
            prodcat = form.save()
            return HttpResponse(prodcat.id)
        status = VALIDATION_ERROR
    else:
        pk = request.GET.get('id')
        if pk:
            form = ProductCategoryForm(instance=ProductCategory.objects.get(pk=pk))
        else:
            parent_id = request.GET.get('parent_id')
            form = ProductCategoryForm(initial={'parent_id': parent_id})
        status = 200

    return TemplateResponse(request, 'menu/product_category.html', {'form': form}, status=status)


@require_manager
def update_product_category(request):
    return update_menu_item(request, ProductCategory, ProductCategoryForm)


@require_manager
def delete_product_category(request):
    return delete_menu_item(request, ProductCategory)


@require_manager
def edit_product(request):
    if request.method == 'POST':
        pk = request.POST['id']
        product = Product.objects.get(pk=pk) if pk else Product()
        form = ProductForm(request.POST, request.FILES, instance=product)
        modifiers = ProductModifierCategoryList(request.POST, instance=product)
        if form.is_valid() and modifiers.is_valid():
            with transaction.atomic():
                product = form.save()
                modifiers.save()
                return HttpResponse(product.id)
        status = VALIDATION_ERROR
    else:
        pk = request.GET.get('id')
        if pk:
            product = Product.objects.get(pk=pk)
            initial = None
        else:
            product = None
            initial = {'category_id': request.GET.get('category_id')}
        form = ProductForm(initial=initial, instance=product)
        modifiers = ProductModifierCategoryList(instance=product)
        status = 200

    context = {'form': form, 'modifiers': modifiers}
    return TemplateResponse(request, 'menu/product.html', context, status=status)


@require_manager
def update_product(request):
    return update_menu_item(request, Product, ProductForm)


@require_manager
def delete_product(request):
    return delete_menu_item(request, Product)


@require_manager
def edit_modifier_category(request):
    if request.method == 'POST':
        pk = request.POST['id']
        modifcat = ModifierCategory.objects.get(pk=pk) if pk else ModifierCategory()
        form = ModifierCategoryForm(request.POST, request.FILES, instance=modifcat)
        if form.is_valid():
            modifcat = form.save()
            return HttpResponse(modifcat.id)
        status = VALIDATION_ERROR
    else:
        pk = request.GET.get('id')
        if pk:
            form = ModifierCategoryForm(instance=ModifierCategory.objects.get(pk=pk))
        else:
            parent_id = request.GET.get('parent_id')
            form = ModifierCategoryForm(initial={'parent_id': parent_id})
        status = 200

    return TemplateResponse(request, 'menu/modifier_category.html', {'form': form}, status=status)


@require_manager
def update_modifier_category(request):
    return update_menu_item(request, ModifierCategory, ModifierCategoryForm)


@require_manager
def delete_modifier_category(request):
    return delete_menu_item(request, ModifierCategory)


@require_manager
def edit_modifier(request):
    if request.method == 'POST':
        pk = request.POST['id']
        modifier = Modifier.objects.get(pk=pk) if pk else Modifier()
        form = ModifierForm(request.POST, request.FILES, instance=modifier)
        if form.is_valid():
            modifier = form.save()
            return HttpResponse(modifier.id)
        status = VALIDATION_ERROR
    else:
        pk = request.GET.get('id')
        if pk:
            form = ModifierForm(instance=Modifier.objects.get(pk=pk))
        else:
            category_id = request.GET.get('category_id')
            form = ModifierForm(initial={'category_id': category_id})
        status = 200

    return TemplateResponse(request, 'menu/modifier.html', {'form': form}, status=status)


@require_manager
def update_modifier(request):
    return update_menu_item(request, Modifier, ModifierForm)


@require_manager
def delete_modifier(request):
    return delete_menu_item(request, Modifier)
