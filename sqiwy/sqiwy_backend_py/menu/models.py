# -*- coding: utf-8 -*-
import itertools
import os.path
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.utils.translation import ugettext_lazy as _
from core.log import get_logger
from core.models import check_expired_images
from core.models import StateChanger

log = get_logger(__name__)


class MyFileSystemStorage(FileSystemStorage):

    def get_available_name(self, name):
        """
        Returns a filename that's free on the target storage system, and
        available for new content to be written to.
        """
        dir_name, file_name = os.path.split(name)
        file_root, file_ext = os.path.splitext(file_name)
        # If the filename already exists, add an underscore and a number
        # (before the file extension, if one exists) to the filename
        # until the generated filename doesn't exist.
        count = itertools.count(1)
        while self.exists(name):
            # file_ext includes the dot.
            name = "%s-%s%s" % (file_root, next(count), file_ext)
            name = os.path.join(dir_name, name)

        return name


class ProductCategoryManager(models.Manager):

    def get_queryset(self):
        return super(ProductCategoryManager, self).get_queryset().filter(is_deleted=False)

    def topcategories(self):
        """
        Returns a sorted list of all top level categories of products.
        @rtype: django.db.models.query.QuerySet
        """
        return self.filter(parent_id=None).order_by('sort_index', 'name')


class ProductCategory(models.Model):

    objects = ProductCategoryManager()

    class Meta:
        db_table = 'product_categories'

    parent = models.ForeignKey('self', null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    sort_index = models.IntegerField(default=0)
    name = models.TextField()
    external_id = models.CharField(max_length=36, blank=True)
    image = models.ImageField(max_length=255, upload_to='images/menu/')
    is_simple_list = models.BooleanField(default=False)

    @property
    def subcategories(self):
        """
        Returns a sorted list of all undeleted child categories of products.
        @rtype: django.db.models.query.QuerySet
        """
        return self.productcategory_set.order_by('sort_index', 'name')

    @property
    def items(self):
        """
        Returns a sorted list of all undeleted child products.
        @rtype: django.db.models.query.QuerySet
        """
        return self.product_set.order_by('sort_index', 'name')

    def delete(self, cascade=True):
        """
        Marks this category of products as deleted.
        """
        self.is_deleted = True
        self.save()
        if cascade:
            for subcategory in self.subcategories.all():
                subcategory.delete()
            self.items.all().update(is_deleted=True)


PRODUCT_SIGNIFICANCE_CHOICES = (
    (-1, _(u'[-1] Элемент списка (без картинки)')),
    (0, _(u'[0] Стандартный')),
    (1, _(u'[1] Увеличенный')),
    (2, _(u'[2] Двойной - горизонтальный')),
    (3, _(u'[3] Двойной - вертикальный')),
    (4, _(u'[4] Большой')),
)

PRODUCT_TYPE_DISH = u'DISH'
PRODUCT_TYPE_GOOD = u'GOOD'
PRODUCT_TYPE_SERVICE = u'SERVICE'

PRODUCT_TYPES = (
    (PRODUCT_TYPE_DISH, _(u'Dish')),
    (PRODUCT_TYPE_GOOD, _(u'Good')),
    (PRODUCT_TYPE_SERVICE, _(u'Service')),
)


class ProductManager(models.Manager):

    # TODO remove ordering
    def get_queryset(self):
        qs = super(ProductManager, self).get_queryset()
        return qs.filter(is_deleted=False).order_by('sort_index', 'name')


class EnabledProductManager(models.Manager):

    def get_queryset(self):
        qs = super(EnabledProductManager, self).get_queryset()
        return qs.filter(is_deleted=False, is_enabled=True)


class Product(models.Model):

    objects = ProductManager()
    enabled_objects = EnabledProductManager()
    _img = None

    class Meta:
        db_table = 'products'

    category = models.ForeignKey(ProductCategory, db_column='product_category_id')
    is_enabled = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    sort_index = models.IntegerField(default=100)
    significance = models.IntegerField(choices=PRODUCT_SIGNIFICANCE_CHOICES, default=0)
    type = models.CharField(max_length=30, choices=PRODUCT_TYPES, default=PRODUCT_TYPE_DISH)
    name = models.TextField()
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    caloricity = models.IntegerField(null=True)
    weight = models.CharField(max_length=50, blank=True)
    external_id = models.CharField(max_length=36, blank=True)
    image = models.ImageField(max_length=255, blank=True, upload_to='images/menu/', storage=MyFileSystemStorage())

    @property
    def product_modifier_categories(self):
        """
        Returns a sorted list of all associations between this product and categories of modifiers.
        @rtype: django.db.models.query.QuerySet
        """
        return self.productmodifiercategory_set.order_by('sort_index', 'modifier_category__name')

    def __init__(self, *args, **kwargs):
        super(Product, self).__init__(*args, **kwargs)
        self._img = self.image

    def get_previous_images(self):
        if self._img != self.image:
            return [self._img]
        return []

    def delete(self, using=None):
        self.productmodifiercategory_set.all().delete()
        self.is_deleted = True
        MenuState().set_changed()
        self.save(using=using, update_fields=['is_deleted'])


DEFAULT_MODIFIER_MINIMUM = 0
DEFAULT_MODIFIER_MAXIMUM = 1


class ModifierCategoryManager(models.Manager):

    def get_queryset(self):
        return super(ModifierCategoryManager, self).get_queryset().filter(is_deleted=False)

    def topcategories(self):
        """
        Returns a sorted list of all top level categories of modifiers.
        @rtype: django.db.models.query.QuerySet
        """
        return self.filter(parent_id=None).order_by('sort_index', 'name')


class ModifierCategory(models.Model):

    objects = ModifierCategoryManager()

    class Meta:
        db_table = 'modifier_categories'

    parent = models.ForeignKey('self', null=True, blank=True)
    is_deleted = models.BooleanField(default=False)
    sort_index = models.IntegerField()
    name = models.TextField()
    min_amount = models.IntegerField(default=DEFAULT_MODIFIER_MINIMUM)
    max_amount = models.IntegerField(default=DEFAULT_MODIFIER_MAXIMUM)
    external_id = models.CharField(max_length=36, blank=True)
    image = models.ImageField(max_length=255, upload_to='images/menu/')

    @property
    def type(self):
        return 'or' if self.max_amount == 1 else 'and'

    @property
    def subcategories(self):
        """
        Returns a sorted list of all undeleted child categories of modifiers.
        @rtype: django.db.models.query.QuerySet
        """
        return self.modifiercategory_set.order_by('sort_index', 'name')

    @property
    def items(self):
        """
        Returns a sorted list of all undeleted child modifiers.
        @rtype: django.db.models.query.QuerySet
        """
        return self.modifier_set.order_by('sort_index', 'name')

    def delete(self, cascade=True):
        """
        Marks this category of modifiers as deleted.
        """
        self.is_deleted = True
        self.save()
        if cascade:
            for subcategory in self.modifiercategory_set.all():
                subcategory.delete()
            self.modifier_set.update(is_deleted=True)


class ModifierManager(models.Manager):

    # TODO remove ordering
    def get_queryset(self):
        qs = super(ModifierManager, self).get_queryset()
        return qs.filter(is_deleted=False).order_by('sort_index', 'name')


class EnabledModifierManager(models.Manager):

    def get_queryset(self):
        qs = super(EnabledModifierManager, self).get_queryset()
        return qs.filter(is_deleted=False, is_enabled=True)


class Modifier(models.Model):

    objects = ModifierManager()
    enabled_objects = EnabledModifierManager()

    class Meta:
        db_table = 'modifiers'

    category = models.ForeignKey(ModifierCategory, db_column='modifier_category_id')
    is_enabled = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    sort_index = models.IntegerField(default=100)
    name = models.TextField()
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    weight = models.CharField(max_length=50)
    external_id = models.CharField(max_length=36, blank=True, null=True)
    image = models.ImageField(max_length=255, upload_to='images/menu/')

    @property
    def min_amount(self):
        return DEFAULT_MODIFIER_MINIMUM

    @property
    def max_amount(self):
        return DEFAULT_MODIFIER_MAXIMUM


class ProductModifierCategory(models.Model):

    class Meta:
        db_table = 'products_modifier_categories'

    product = models.ForeignKey(Product)
    modifier_category = models.ForeignKey(ModifierCategory)
    sort_index = models.IntegerField(default=0)
    min_amount = models.IntegerField(blank=True, null=True)
    max_amount = models.IntegerField(blank=True, null=True)

    def get_min_amount(self):
        """
        Gets the minimum of the summary amount of modifiers from the category which can be applied to the product.
        """
        return self.min_amount if self.min_amount else self.modifier_category.min_amount

    def set_min_amount(self, min_amount):
        """
        Sets the minimum of the summary amount of modifiers from the category which can be applied to the product.
        """
        self.min_amount = min_amount if min_amount != self.modifier_category.min_amount else None

    def get_max_amount(self):
        """
        Gets the maximum of the summary amount of modifiers from the category which can be applied to the product.
        """
        return self.max_amount if self.max_amount else self.modifier_category.max_amount

    def set_max_amount(self, max_amount):
        """
        Sets the maximum of the summary amount of modifiers from the category which can be applied to the product.
        """
        self.max_amount = max_amount if max_amount != self.modifier_category.max_amount else None


class MenuState(StateChanger):

    name = 'menuchanged'


def mark_menu_changed_ondelete(sender, instance, **kwargs):
    MenuState().set_changed()


def mark_menu_changed(sender, instance, created, **kwargs):
    if created:
        if instance.image:
            MenuState().set_changed()
    if len(instance.get_previous_images()):
        MenuState().set_changed()


post_save.connect(mark_menu_changed, Product,
                  dispatch_uid='Product__mark_menu_changed')

post_delete.connect(mark_menu_changed_ondelete, Product,
                    dispatch_uid='Product__mark_menu_changed')

post_save.connect(check_expired_images, ProductCategory,
                  dispatch_uid='ProductCategory__check_expired_images')

post_save.connect(check_expired_images, Product,
                  dispatch_uid='Product__check_expired_images')

post_save.connect(check_expired_images, ModifierCategory,
                  dispatch_uid='ModifierGroup__check_expired_images')

post_save.connect(check_expired_images, Modifier,
                  dispatch_uid='Modifier__check_expired_images')
