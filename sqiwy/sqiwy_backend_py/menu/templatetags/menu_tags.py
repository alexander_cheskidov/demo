from django import template
from django.forms.util import flatatt
from django.utils.html import format_html
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter(is_safe=True)
def menu_label(field):
    """Renders HTML label tag for the specified field.
    @type field: django.forms.forms.BoundField
    """
    attrs = {'class': u'form-label'}
    if field.help_text:
        attrs['title'] = field.help_text
    return field.label_tag(attrs=attrs)


@register.filter(is_safe=True)
def menu_input(field):
    """Renders HTML input tag for the specified field.
    @type field: django.forms.forms.BoundField
    """
    outerAttrs = {'class': u'form-input'}
    innerClass = u'form-error'
    if field.form.is_bound:
        if field.errors:
            outerTitle = u'\n'.join(field.errors)
            innerClass += u' input-fail'
        else:
            outerTitle = _(u'OK')
            innerClass += u' input-pass'
        outerAttrs['title'] = outerTitle
    outerAttrs = flatatt(outerAttrs)
    innerAttrs = flatatt({'class': innerClass})

    # TODO: think about this: non-unicode string does not work on linux
    result = format_html(u'<span{0}><span{1}></span>{2}</span>', outerAttrs, innerAttrs, field.as_widget())
    return mark_safe(result)
