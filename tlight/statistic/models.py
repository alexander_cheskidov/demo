"""
Этот модуль для демонстрационных целей;
он объясняет какие основные сущности есть в системе чтобы было больше понятно происходящее
в модуле partners.
"""
import logging
import yaml
from users.models import User


class Affiliate(User):
    """
    Affiliate - пользователь нашей партнерской программы; он распространяет по своим каналам
    ссылку на предложение рекламодателя и за действия ("лиды") получает от нас выплаты (мы в свою очередь от
    рекламодателей (Advertiser))
    """

    def __init__(self):
        self.id = 1
        self.username = 'test affiliate'


class Advertiser(User):
    """
    Advertiser - пользователь нашей партнерской программы, который надеется получить дополнительный
    приток пользователей своей услуги и платит за это.
    Рекламодатель, дает нам некие промо материалы (или берет наши типовые,
    или просто дает ссылку на свой сайт) и ожидает что мы распространим их и привлечем к рекламируемым ресурсам
    посетителей.
    """
    def __init__(self):
        self.id = 2
        self.username = 'test advertiser'


class AdvertiserOffer:
    """
    AdvertiserOffer - дата-модель которая содержит конкретные данные о конкретном предложении (товаре\услуге)
     конкретного рекламодателя.
    """

    # settings хранит настройки конкретного предолжения и рекламодателя
    settings = ''

    def __init__(self):
        self.settings = 'product_id: 123'

    def get_settings(self):
        settings = (self.settings or '').strip()
        settings = yaml.load(settings) or {}
        return settings


class Lead:

    """
    Класс олицетворяет собой ключевое "действие" (он же "лид") которое оплачивается за которое
    рекламодатель платит нам, а мы платим партнерам.
    Действие это одно из:
     - факт скачивания софта (когда рекламируется софт)
     - факт регистрации (когда рекламируются онлайн курсы)
     - заказ некоего товара
    В этот демо приложении "лид" это заказ в интернет магазине,
    рекламодателей в данном случае в первую очередь интересуют
     - advertiser_offer - содержит информацию о товаре
     - customer_name - имя
     - customer_phone - телефон
     * остальная информация опциональна
    """

    class STATUS:
        NEW = 0
        CONFIRMED = 10
        CANCELLED = -10
        
    id = None
    status = None
    status_comment = None
    customer_name = None
    customer_phone = None
    customer_phone_clean = None
    customer_comment = ''
    customer_country = None
    advertiser_offer = None
    affiliate = None
    ip_address = None

    def __init__(self):
        self.advertiser_offer =AdvertiserOffer()

    def change_status(self, status, comment=None):
        self.status = status
        self.status_comment = comment
        self.save()
        
    def save(self, **kwargs):
        logging.debug('called Lead(id=%s).save()', self.id)

    def update_fields(self, **kwargs):
        """
        Обновляются поля класса ТОЛЬКО если их значение действительно изменилось.
        """
        update_fields = {}
        for field, value in kwargs.items():
            if getattr(self, field) != value:
                update_fields[field] = value
                setattr(self, field, value)
        if update_fields:
            self.__class__.objects.filter(id=self.id).update(**update_fields)
