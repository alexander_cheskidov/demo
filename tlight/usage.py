"""
В данном примере акцент на модуле partners.
partners.advertisers - содержит классы, каждый из которых реализует интеграцию нашей системы с системой рекламодателя.
"Система рекламодателя" это общее понятие - это может быть crm система под ключ разного калибра, коробочное решение
или saas, но все они так или иначе могут принять на обработку заказ и имеют фид, который можно опрашивать
и получать актуальное состояние лидов\заказов.

Все классы-модули наследуются от partners.advertisers.system.System, в соответствии с бизнес процессами каждый
наследник переопределяет и реализует методы:
System.send_lead(lead) - отправла лида в систему рекламодателя (всегда по 1-му)
System.check_leads(leads) - опрашивает систему рекламодателя о состоянии лидов, и само состояние лидов так же меняется
                     в случае изменения ключевых параметров этот метод уполномочен вызвать Lead.change_status()

partners.systems.Systems - это фабрика и менеджер инстансов-наследников System.
он инстанциируется при старте приложения и всегда доступен как partners.systems.systems (можно сказать что он синглтон)

Инстансы классов, у которых переопределен .check_leads_worker - при вызове systems.check_leads буду выполняться
в отдельном потоке для ускорения работы.
Это сделано так как при обращении к сторонней системе System.check_leads(leads) может ожидать
данных продолжительное время.

В данном модуле мы предполагаем, что хотим отправить рекламодателю m1shop "лид" и сразу же проверить его статус.
"""

from partners.systems import systems
from users.models import User

from statistic.models import AdvertiserOffer, Lead, Affiliate, Advertiser

m1shop_advertiser = Advertiser()
m1shop_advertiser.system = 'm1shop'
m1shop_advertiser.username = 'M1Shop cpa'

# получаем от systems инстанс реализующий работу именно с м1шоп; он находится по m1shop_advertisers.system
m1shop = systems.factory(m1shop_advertiser)

# делаем фейковый лид-заказ с тестовыми данными
lead = Lead()
lead.affiliate = Affiliate()
lead.advertiser_offer = AdvertiserOffer()
lead.customer_country = 'RU'
lead.customer_name = 'tester'
lead.customer_phone = '+7 999 1111111'

m1shop.send_lead(lead)  # отправка лида

m1shop.check_leads([lead])  # получение статуса
