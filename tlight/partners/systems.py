from collections import OrderedDict, defaultdict
import threading
import logging
from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.utils import timezone

from .advertisers.system import System
from .systems_installed import INSTALLED_ADVERTISERS

from misc.lib import Profiler
from users.models import User
from statistic.models import Lead

log = logging.getLogger('tlight.sytems')


class SystemsManager:

    def __init__(self, systems):
        self._systems = OrderedDict()
        for system in systems:
            self.add_system(system)

    def add_system(self, system: System):
        if isinstance(system, str):
            import importlib
            adv_module_name, adv_class_name = system.rsplit('.', 1)
            adv_module = importlib.import_module(adv_module_name)
            system = getattr(adv_module, adv_class_name)
            assert issubclass(system, System)
        self._systems[system.name] = system

    def choices(self):
        choices = []
        for system in self._systems.values():
            choices.append((system.name, system.verbose_name))
        return tuple(choices)

    def factory(self, user) -> System:
        system = self.get(user.system)
        if isinstance(system, System):
            system = type(system)
        return system(user)

    def get(self, system):
        try:
            return self._systems[system]
        except KeyError:
            raise ObjectDoesNotExist('system "%s" not found' % system)

    def send_new_leads(self):

        with Profiler('prepare leads'):
            # grouped_leads - by advertiser; { advertiser: leads }
            leads_count, grouped_leads = self._get_leads_to_send()
            log.info('Total leads to send: %d; advertisers: %d', leads_count, len(grouped_leads))

        for advertiser, leads in grouped_leads.items():

            system = self.factory(advertiser)
            with Profiler('%s send leads' % system):

                for lead in leads:
                    try:
                        lead.customer_name = lead.customer_name or '[не указано]'  # TODO: hackfix
                        system.check_and_send_lead(lead)

                    except System.CancelLead as e:
                        lead.change_status(Lead.STATUS.CANCELLED, e.real_comment, commit=False)
                        lead.status_reason = e.comment
                        lead.save()
                        log.info('Lead(id=%s) cancelled', lead.id)

                    except Exception as e:
                        log.error('Lead(id=%s), %s, %s', lead.id, system, e, exc_info=True)

    def _get_leads_to_send(self):

        leads = Lead.objects.prefetch_related(
            'advertiser',
            'advertiser_offer',
            'advertiser_offer__offer',
            'user')

        leads = leads.filter(
            source__in=self._get_relevant_lead_sources(),
            real_status=Lead.STATUS.NEW,
            advertiser_lead_id__isnull=True)

        sorted_leads = defaultdict(lambda: [])
        leads_count = len(leads)
        for lead in leads:
            sorted_leads[lead.advertiser].append(lead)
        return leads_count, sorted_leads

    def check_leads(self):

        with Profiler('prepare leads', log):
            leads_count, grouped_leads = self._get_leads_to_check()
            log.info('Total leads to check: %d; advertisers: %d', leads_count, len(grouped_leads))

            workers = CheckLeadsWorkersPool()

            for advertiser in User.objects.exclude(system=''):

                system = self.factory(advertiser)
                leads = grouped_leads.get(advertiser, [])

                if not leads and system.skip_check_empty_leads:
                    continue

                workers.get_worker(system).add_leads(system, leads)

        workers.do_work()

    def _get_leads_to_check(self):

        leads = Lead.objects.prefetch_related(
            'offer',
            'advertiser',
            'advertiser_offer',
            'user')

        leads = leads.filter(
            source__in=self._get_relevant_lead_sources(),
            real_status=Lead.STATUS.NEW,
            advertiser_lead_id__isnull=False)  # adv lead id is NULL when no attempts to send lead to advertiser was made

        # adv lead id == '0' when advertiser rejected the lead due to technical or other problems
        # adv lead id == '' when there is a bug in the integration or advertiser is not good
        leads = leads.filter(~Q(advertiser_lead_id__in=['0', '']))

        sorted_leads = defaultdict(lambda: [])
        leads_count = len(leads)
        for lead in leads:

            lead_create_time_delta = timezone.now() - lead.created_at
            if lead_create_time_delta > timedelta(days=30):
                if lead.real_status == Lead.STATUS.NEW:
                    lead.change_status(Lead.STATUS.CANCELLED, 'very old lead (autocancel)')
                    continue

            sorted_leads[lead.advertiser].append(lead)

        return leads_count, sorted_leads

    @staticmethod
    def _get_relevant_lead_sources():
        return [
            Lead.Source.CPA,
            Lead.Source.AFFILIATE,
            Lead.Source.FACEBOOK,
        ]


class CheckLeadsWorker(threading.Thread):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._leads = []

    def add_leads(self, system, leads):
        self._leads.append((system, leads))

    def run(self):
        with Profiler('Worker %s run ' % self.name, log):
            for system, leads in self._leads:
                with Profiler('%s check leads' % system, log):
                    self._check_leads(system, leads)

    def _check_leads(self, system, leads):
        try:
            log.info('Advertiser %s: %d leads to check', system, len(leads))
            system.check_leads(leads)

        except Exception as e:
            log.error('%s, %s', system, e, exc_info=True)


class CheckLeadsWorkersPool:

    def __init__(self):
        self.workers = {}

    def get_worker(self, system) -> CheckLeadsWorker:
        if system.check_leads_worker not in self.workers:
            worker = CheckLeadsWorker()
            worker.setName(system.check_leads_worker)
            self.workers[system.check_leads_worker] = worker
        return self.workers[system.check_leads_worker]

    def do_work(self):
        for worker in self.workers.values():
            worker.start()
        for worker in self.workers.values():
            worker.join()


with Profiler('init advertisers systems'):
    systems = SystemsManager(INSTALLED_ADVERTISERS)
