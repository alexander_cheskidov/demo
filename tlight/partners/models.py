import json
import logging
import hashlib
from urllib.parse import urlsplit

from django.conf import settings
from django.db import models
from django.db.models import F
from django.db.models.signals import post_save
from django.http import QueryDict
from django.utils import timezone

from misc import requests
from statistic.models import Lead
from users.models import User

log = logging.getLogger('cpa.partners.models')


class AffiliateCallbackData(object):

    __slots__ = (
        'id',
        'id2',
        'status',
        'status_id',
        'status_code',
        'payout',
        'fee',  # legacy
        'comment',
        'ex', # legacy
        'extra',  # legacy
        'subaccount',  # legacy
        'offer_id',
        'sub1',
        'sub2',
        'sub3',
        'sub4',
        'sub5',
        'order_page',
        'ip_address',
        'country',
        'created_at',
        'created_at_unixtime',
        'trash',
        'trash_reason',  # legacy
        # 'customer_name',
        # 'customer_phone',

        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'utm_term',
    )

    def __init__(self, lead, overrides=None):
        overrides = overrides or {}
        assert isinstance(lead, Lead)

        statuses = {
            Lead.STATUS.NEW: ('new', 2),
            Lead.STATUS.CANCELLED: ('cancelled', 3),
            Lead.STATUS.CONFIRMED: ('confirmed', 1),
        }
        lead_status = overrides.get('status', lead.status)
        status, status_id = statuses[lead_status]

        self.id = lead.id
        self.id2 = lead.order_id
        self.status = status
        self.status_id = status_id
        self.status_code = lead_status

        affiliate_payout = lead.affiliate_payout
        if int(affiliate_payout) == affiliate_payout:
            affiliate_payout = int(affiliate_payout)
        self.payout = str(affiliate_payout)
        self.fee = self.payout
        self.comment = lead.status_reason
        self.ex = lead.sub2
        self.extra = lead.sub2
        self.subaccount = lead.sub1
        self.offer_id = lead.offer.id
        self.sub1 = lead.sub1
        self.sub2 = lead.sub2
        self.sub3 = lead.sub3
        self.sub4 = lead.sub4
        self.sub5 = lead.sub5
        self.order_page = lead.order_page
        self.ip_address = lead.ip_address
        self.country = lead.customer_country
        self.created_at = timezone.localtime(lead.created_at).isoformat()  # .strftime('%Y-%m-%dT%H:%M:%S%z')
        self.created_at_unixtime = int(lead.created_at.timestamp())

        if lead.is_trash and lead_status == Lead.STATUS.CANCELLED:
            self.trash_reason = lead.trash_status or None
            self.trash = lead.trash_status or None

        else:
            self.trash_reason = None
            self.trash = None

        self.utm_source = lead.utm_source
        self.utm_medium = lead.utm_medium
        self.utm_campaign = lead.utm_campaign
        self.utm_content = lead.utm_content
        self.utm_term = lead.utm_term

    def __iter__(self):
        for key in self.__slots__:
            yield (key, getattr(self, key))


def lead_to_data(lead, overrides=None):
    return AffiliateCallbackData(lead, overrides)


class PostbackStatus(object):

    NEW = 'NEW'
    FINISHED = 'FINISHED'
    ERROR = 'ERROR'

    CHOICES = (
        (NEW, NEW),
        (FINISHED, FINISHED),
        (ERROR, ERROR),
    )


class PostbackManager(models.Manager):

    def create_for_lead(self, lead, subid_token, **overrides):

        assert lead.id is not None

        if lead.magic1:
            log.info('Lead(id=%s) do not create postback because: magic1=True', lead.id)
            return

        subid = '{}.{}'.format(lead.id, subid_token.lower())

        url, data = self._prepare_url_and_data(lead, overrides)
        if not url:
            log.info('Postback(subid=%s) not created - no url found', subid)
            return None

        postback, created = Postback.objects.get_or_create(lead=lead, subid=subid, user=lead.user)
        if created:
            postback.url = url
            postback.data = json.dumps(data)
            postback.method = 'POST'
            postback.save()
            log.debug('Postback(subid=%s) created', postback.subid)

        else:
            log.debug('Postback(subid=%s) existed', postback.subid)

        return postback

    def _prepare_url_and_data(self, lead, overrides):

        user = lead.user
        postback_url_source = user
        stream = lead.stream
        if stream and not user.postback_override:
            if not stream.postback_use_global:
                postback_url_source = stream

        url = None
        data = lead_to_data(lead, overrides)

        if data.status_code == Lead.STATUS.NEW:
            url = postback_url_source.postback_url_new

        if data.status_code == Lead.STATUS.CONFIRMED:
            url = postback_url_source.postback_url_confirmed

        if data.status_code == Lead.STATUS.CANCELLED and lead.is_trash:
            url = postback_url_source.postback_url_trash

        if data.status_code == Lead.STATUS.CANCELLED and not url:
            url = postback_url_source.postback_url_cancelled

        if url:
            data = dict(data)
            del data['order_page']
            data.pop('trash_reason', None)
            for k, v in data.items():
                if v is None:
                    data[k] = ''
            url = url.format(**data)
            return url, data

        else:
            return None, None


class Postback(models.Model):

    SEND_TIMEOUT = 30

    objects = PostbackManager()

    class Meta:
        db_table = 'partners_postbacks'

    created_at = models.DateTimeField(default=timezone.now)
    last_attempt_at = models.DateTimeField(null=True)
    attempts_count = models.IntegerField(default=0)
    url = models.TextField(max_length=4096)
    data = models.TextField(null=True)
    method = models.CharField(max_length=20, choices=(('GET', 'GET'), ('POST', 'POST')), default='POST')
    status = models.CharField(max_length=20, choices=PostbackStatus.CHOICES, default=PostbackStatus.NEW)
    response_code = models.PositiveSmallIntegerField(default=0)
    response_text = models.CharField(max_length=500)
    user = models.ForeignKey(User, related_name='postbacks')
    lead = models.ForeignKey(Lead, related_name='postbacks')
    subid = models.CharField(max_length=50, null=True)

    def __str__(self):
        lead = None
        if self.lead_id:
            lead = self.lead_id
        return 'Postback(user=%s, lead=%s, url=%s)' % (str(self.user), lead, self.url)

    def send(self):

        self.response_text = ''

        netloc = urlsplit(self.url).netloc
        if netloc == 'al-api.com':
            self._send_apileads()
        else:
            self._send()
        self.last_attempt_at = timezone.now()
        self.attempts_count = F('attempts_count') + 1
        self.save(update_fields=[
            'status',
            'response_code',
            'response_text',
            'last_attempt_at',
            'attempts_count',
        ])

    def _send(self):

        # This headers are workaround for sensitive web-frameworks, which expects them
        # and fail if they are not present
        headers = {
            'Accept-Language': 'en-US,en;q=0.5',
        }

        try:

            if self.method == 'GET':
                response = requests.get(
                    self.url, headers=headers, timeout=Postback.SEND_TIMEOUT)

            elif self.method == 'POST':
                data = QueryDict(urlsplit(self.url).query)
                response = requests.post(
                    self.url, data=data, headers=headers, timeout=Postback.SEND_TIMEOUT)

            response_code = response.status_code
            response_text = response.text[:500]
            log.debug('Postback._send() %s\nresponse: [%s] %s', self.url, response_code, response.text)

            self.response_code = response.status_code
            self.response_text = response_text

            response.raise_for_status()
            self.status = PostbackStatus.FINISHED

        except requests.RequestException as e:
            self.status = PostbackStatus.ERROR
            if not self.response_text:
                self.response_text = str(e)

    def _send_apileads(self):
        # Apileads adaptation;
        # - request format differs from usual - POST body is json-encoded

        data = json.loads(self.data)

        if data['status'] == 'confirmed':
            status = 'confirm'
            comment = ''

        elif data['status'] == 'cancelled':
            status = 'reject'
            comment = (self.lead.status_reason or '').strip()
            if data.get('trash'):
                status = 'trash'

        else:
            self.status = PostbackStatus.ERROR
            self.response_text = 'unsupported status "%s"' % data['status']
            return

        check_sum = ''.join(map(str, [data['id2'], status, comment, settings.APILEADS_KEY]))
        check_sum = hashlib.sha1(check_sum.encode()).hexdigest()

        custom_data = {
            "id": data['id2'],
            "status": status,
            "comment": comment,
            "check_sum": check_sum
        }
        headers = {
            'Content-Type': 'application/json',
        }

        try:

            response = requests.post(
                self.url, json=custom_data, headers=headers, timeout=Postback.SEND_TIMEOUT)

            response_status_code = response.status_code
            response_text = response.text[:500]
            log.debug('Postback._send_apileads() %s\nresponse: [%s] %s', self.url, response_status_code, response.text)

            self.response_text = response_text

            response.raise_for_status()
            self.status = PostbackStatus.FINISHED

        except requests.RequestException as e:
            self.status = PostbackStatus.ERROR
            if not self.response_text:
                self.response_text = str(e)


def create_postback_for_lead(sender, instance, created, **kwargs):
    lead = instance

    if not lead.send_postback:
        return

    if not lead.submitted and lead.status != Lead.STATUS.CONFIRMED:
        return

    if lead.status == Lead.STATUS.NEW:
        Postback.objects.create_for_lead(lead, 'new')

    elif lead.status_changed:
        Postback.objects.create_for_lead(lead, 'new', status=Lead.STATUS.NEW)
        Postback.objects.create_for_lead(lead, 'changed')


post_save.connect(create_postback_for_lead, Lead, dispatch_uid='create_postback_for_lead')


def validate_postbacks(obj):
    import re
    from django import forms
    postback_url_fields = [
        'postback_url_new',
        'postback_url_confirmed',
        'postback_url_cancelled',
        'postback_url_trash',
    ]
    for postback_url_field in postback_url_fields:
        postback_url = getattr(obj, postback_url_field)
        # postback_url = postback_url.replace('{ex}', '{sub2}')
        # postback_url = postback_url.replace('{extra}', '{sub2}')
        postback_fields = re.findall('\{(\w+)\}', postback_url)
        postback_fields = dict(zip(postback_fields, postback_fields))
        try:
            postback_url.format(**postback_fields)
            all_macro = dict(zip(AffiliateCallbackData.__slots__, AffiliateCallbackData.__slots__))
            postback_url.format(**all_macro)
        except (KeyError, ValueError, IndexError):
            raise forms.ValidationError('Некорректные макросы в постбэк URL "%s"' % postback_url)
