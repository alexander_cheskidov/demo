
INSTALLED_ADVERTISERS = [
    'partners.advertisers.leadvertex.Leadvertex',
    'partners.advertisers.leadvertex_glsender.Leadvertex_Glsender',
    'partners.advertisers.leadvertex_irs.Leadvertex_IRS',
    'partners.advertisers.leadvertex_north_adv.Leadvertex_North',
    'partners.advertisers.leadvertex_neposeda.Leadvertex_Neposeda',
    'partners.advertisers.retailcrm.Retailcrm',
    'partners.advertisers.funcpa.FUNCPA',
    'partners.advertisers.kma.kma.KMA',
    'partners.advertisers.m1shop.M1shop',
    'partners.advertisers.monsterleads.Monsterleads',
    'partners.advertisers.remedio.Remedio',
    'partners.advertisers.cpakitchen.CpaKitchen',
]
