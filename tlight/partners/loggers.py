import logging

from django.conf import settings


def get_logger(system_name):
    log = logging.getLogger('cpa.partners.{}'.format(system_name))
    file_path = '{}{}.log'.format(settings.SYSTEM_LOGGER_PATH, system_name)
    if not list(filter(lambda x: isinstance(x, logging.FileHandler), log.handlers)):
        log.addHandler(logging.FileHandler(file_path))
    return log
