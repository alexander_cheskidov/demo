from django.core.management.base import BaseCommand

from core.models import cron_script_run
from misc.lib import Profiler
from partners.systems import systems


class Command(BaseCommand):

    @cron_script_run('partners_advertisers', 60, 60)
    def handle(self, *args, **options):

        with Profiler('systems.send_new_leads()'):
            systems.send_new_leads()

        with Profiler('systems.check_leads()'):
            systems.check_leads()
