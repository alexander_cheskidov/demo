
from django.core.management.base import BaseCommand

from core.models import cron_script_run
from partners.models import Postback, PostbackStatus


class Command(BaseCommand):

    @cron_script_run('partners_postbacks', 300, 300)
    def handle(self, *args, **options):
        qs = Postback.objects.filter(status__in=[PostbackStatus.NEW, PostbackStatus.ERROR])
        qs = qs.order_by('id')
        for postback in qs:
            postback.send()
