"""
    api doc: <confidential>
"""
import json
import re

from misc import requests
from partners.advertisers.system import System
from statistic.models import Lead


class Remedio(System):

    name = 'remedio'
    verbose_name = 'Remedio'
    can_send_lead_realtime = True

    host = 'http://remedium.pro'
    send_lead_url = host + '/aapi/order/create.php'
    check_status_url = host + '/aapi/order/status.php'

    def send_lead(self, lead):
        assert isinstance(lead, Lead)
        advertiser_offer = lead.advertiser_offer
        advertiser_settings = advertiser_offer.get_settings()
        goods_id = advertiser_settings['goods_id']
        api_key = advertiser_settings['api_key']

        data = {
            'goods_id': goods_id,
            'ip': lead.ip_address,
            'msisdn': lead.customer_phone_clean,
            'name': lead.customer_name,
            'api_key': api_key,
            'partner_name': lead.subaccount,
            'client_type': lead.useragent_type,
            'land_id': ''
        }

        response = requests.post(self.send_lead_url, data=data)
        response_text = re.sub('^[^\{]*\{', '{', response.text)
        response = json.loads(response_text)

        if response['msg'] == 'success':
            lead.advertiser_lead_id = response['order_id']
            lead.save(update_fields=['advertiser_lead_id'])

        else:
            self.advertiser_rejected_lead(lead, response)

    def check_leads(self, leads):
        for leads in self.chunks(leads, 50):
            self._check_leads(leads)

    def _check_leads(self, leads):
        advertiser_settings = self.advertiser.get_system_settings()
        api_key = advertiser_settings['api_key']
        leads_map = {str(lead.advertiser_lead_id): lead for lead in leads}

        response = requests.get(self.check_status_url, params={
            'api_key': api_key,
            'orders': ','.join(leads_map.keys()),
        })
        response.raise_for_status()
        response = response.json()

        if response['code'] == 'error':
            raise self.Error(response)

        for lead_data in response['data']:

            if 'order_id' not in lead_data:
                continue

            lead = leads_map[str(lead_data['order_id'])]

            lead.advertiser_status = lead_data['status']
            lead.save(update_fields=['advertiser_status'])

            status = self.decode_postback_status(lead_data['status'])
            if status:
                lead.change_status(status, lead_data.get('comment'))

    def decode_postback_status(self, status):
        if status in ['hold', 'processing']:
            return Lead.STATUS.NEW
        elif status == 'bill':
            return Lead.STATUS.CONFIRMED
        elif status in ['trash', 'cancelled', 'unknown']:
            return Lead.STATUS.CANCELLED

    def get_required_config_keys(self):
        return ['goods_id', 'api_key']
