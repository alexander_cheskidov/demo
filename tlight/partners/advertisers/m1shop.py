"""
    api documentation: <confidential>
"""
from misc import requests
from partners.advertisers.system import System
from statistic.models import Lead


class M1shop(System):

    name = 'm1shop'
    verbose_name = 'M1shop'
    can_send_lead_realtime = True

    send_lead_url = 'http://m1-shop.ru/send_order'
    check_status_url = 'http://m1-shop.ru/get_order_status'

    def send_lead(self, lead):
        assert isinstance(lead, Lead)
        advertiser_offer = lead.advertiser_offer
        advertiser_settings = advertiser_offer.get_settings()

        data = {
            'ref': advertiser_settings['ref'],
            'api_key': advertiser_settings['api_key'],
            'product_id': advertiser_settings['offer_id'],
            'phone': lead.customer_phone_clean,
            'name': lead.customer_name,
            'ip': lead.ip_address,
            's': lead.user.id,
            'address': lead.customer_address,
            'referer': lead.referer,
        }

        response = requests.post(self.send_lead_url, data)

        if response.status_code == 400:
            self.advertiser_rejected_lead(lead, response.text)
            return

        response.raise_for_status()
        response = response.json()

        lead.advertiser_lead_id = response['id']
        lead.save()

    def check_leads(self, leads):
        for leads in self.chunks(leads, 20):
            self._check_leads(leads)

    def _check_leads(self, leads):
        advertiser = self.advertiser

        advertiser_settings = advertiser.get_system_settings()
        leads_map = {lead.advertiser_lead_id: lead for lead in leads}

        params = {
            'ref': advertiser_settings['ref'],
            'api_key': advertiser_settings['api_key'],
            'id': ','.join(leads_map.keys())
        }

        response = requests.get(self.check_status_url, params=params)
        response.raise_for_status()
        response = response.json()

        if response['result'] == 'error':
            raise self.Error(response['message'])

        for lead_data in response['result']:
            error = lead_data.get('error', None)
            if error:
                self.get_logger().warning(error)
                continue

            status = str(lead_data['status'])
            comment = [
                lead_data.get('callcenter_comment', ''),
                lead_data.get('comment', ''),
            ]
            comment = '; '.join(filter(lambda s: s.strip(), comment)).strip()

            lead = leads_map[str(lead_data['m1_id'])]

            lead.update_fields(advertiser_status=status, real_status_reason=comment)

            if status == '0':  # статус 0 - заказ еще в обработке
                continue

            if status == '2':
                lead.change_status(Lead.STATUS.CANCELLED, comment)

            elif status == '1':
                lead.change_status(Lead.STATUS.CONFIRMED)

            elif status == ['3', '4']:
                lead.change_status(Lead.STATUS.CONFIRMED)

    def get_required_config_keys(self):
        return ['ref', 'api_key', 'offer_id']
