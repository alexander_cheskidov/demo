from datetime import timedelta
from django.utils import timezone
from decimal import Decimal
from urllib.parse import urlsplit, urljoin

from misc import requests
from partners.advertisers.system import System
from partners.advertisers.tradecore import Tradecore
from statistic.models import Lead


class Leadvertex(System):

    name = 'leadvertex'
    verbose_name = 'LeadVertex'
    can_send_lead_realtime = True
    check_leads_worker = 'leadvertex'

    @staticmethod
    def get_order_id(lead):
        return Tradecore.get_order_id(lead)

    def send_lead(self, lead):

        data = self._get_lead_data(lead)

        system_settings = lead.advertiser_offer.get_settings()

        if system_settings.get('domain'):
            data['domain'] = system_settings['domain']

        if system_settings.get('offer_id'):

            if system_settings['offer_id'] == 'shop':  # MARKET
                good = (lead.customer_comments or '').strip()
                data['goods[0][goodID]'] = good
                data['goods[0][quantity]'] = 1
                data['goods[0][price]'] = ''
                data.pop('price', None)
                data.pop('total', None)

            else:
                data['goods[0][goodID]'] = system_settings['offer_id']
                data['goods[0][quantity]'] = 1
                data['goods[0][price]'] = str(lead.get_price() or 0)

        params = {
            'webmasterID': system_settings['webmaster_id'],
            'token': system_settings['api_key'],
        }

        url = urljoin(system_settings['url'], '/api/webmaster/v2/addOrder.html')
        response = requests.post(url, data=data, params=params)
        self.get_logger().debug('send lead: raw responce: %s', str(response.text))

        created_delta = timezone.now() - lead.created_at
        if response.status_code == 500 and created_delta > timedelta(hours=2):
            self.advertiser_rejected_lead(lead, 'error 500')
            return

        if 400 <= response.status_code < 500:
            self.advertiser_rejected_lead(lead, response.text)
            return

        result = response.json()
        if response.status_code == 200 and 'OK' in result:
            lead.update_fields(advertiser_lead_id=result['OK'])

        else:
            raise Exception(result)

    def _get_lead_data(self, lead):

        item_price = lead.advertiser_offer.price
        items_count = 1
        delivery_price = lead.advertiser_offer.delivery_price
        total_price = (item_price or 0) * items_count + (delivery_price or 0)
        domain = urlsplit(lead.order_page)

        data = {
            'country': lead.customer_country,
            # 'region': None,
            # 'city': None,
            'postIndex': None,
            'address': lead.customer_address,
            # 'house': 'дом/строение. String[10]',
            # 'flat': 'квартира/офис. String[10]',
            'fio': lead.customer_name,
            'phone': lead.customer_phone_clean,
            'email': lead.customer_email,
            'price': item_price,
            'total': total_price,
            'quantity': items_count,
            # 'additional1': 'Moved to flavour',
            # 'additional2': 'Moved to flavour',
            # 'additional3': 'Валюта: {}'.format(currency),
            # 'additional4': 'Доставка: {} {}'.format(delivery_price or '--', currency),
            # 'additional3': 'дополнительное поле 3. String[255]',
            # 'additional4': 'дополнительное поле 4. String[255]',
            # 'additional5': 'дополнительное поле 5. String[255]',
            # 'additional6': 'дополнительное поле 6. String[255]',
            # 'additional7': 'дополнительное поле 7. String[255]',
            # 'additional8': 'дополнительное поле 8. String[255]',
            # 'additional9': 'дополнительное поле 9. String[255]',
            # 'additional10': 'дополнительное поле 10. String[255]',
            # 'additional11': 'дополнительное поле 11. String[500]',
            # 'additional12': 'дополнительное поле 12. String[500]',
            # 'additional13': 'дополнительное поле 13. String[500]',
            # 'additional14': 'дополнительное поле 14. String[500]',
            # 'additional15': 'дополнительное поле 15. String[500]',
            'comment': lead.customer_comments,  # 'комментарий к заказу. String[500]',
            # 'russianpostTrack': 'трек-номер Почты России. String[14]',
            # 'novaposhtaTrack': 'трек-номер Нова Пошта. String[14]',
            # 'kazpostTrack': 'трек-номер Казпочты. String[14]',
            # 'timezone': 'integer Смещение времени клиента в минутах относительно UTC (чтобы не звонить ему ночью).',
            'utm_source': lead.utm_source,
            'utm_medium': lead.utm_medium,
            'utm_campaign': lead.utm_campaign,
            'utm_term': lead.utm_term,
            'utm_content': lead.utm_content,
            'domain': domain.netloc + domain.path,
            'timeSpent': lead.visit_duration1,
            'ip': lead.ip_address,
            # 'referer': '',
        }

        assert isinstance(lead, Lead)
        if lead.timezone:
            data['timezone'] = lead.timezone * -60

        system_settings = lead.advertiser_offer.get_settings()
        data['additional1'] = system_settings.get('additional1', lead.affiliate.id)
        data['additional2'] = system_settings.get('additional2', lead.id)

        for i in range(3, 16):
            additional_field = 'additional%d' % i
            if additional_field in system_settings:
                data[additional_field] = system_settings[additional_field]

        return data

    def get_required_config_keys(self):
        return ['offer_id', 'api_key', 'webmaster_id', 'url']

    def check_leads(self, leads):
        for advertiser_offer, leads in self.group_by_advertiser_offer(leads, 100):
            self._check_statuses(leads, advertiser_offer)

    def _check_statuses(self, leads, advertiser_offer):

        system_settings = advertiser_offer.get_settings()
        leads = list(leads)
        url = urljoin(system_settings['url'], '/api/webmaster/v2/getOrdersByIds.html')
        leads_map = {lead.advertiser_lead_id: lead for lead in leads}

        params = {
            'webmasterID': system_settings['webmaster_id'],
            'token': system_settings['api_key'],
            'ids': ','.join(leads_map.keys()),
        }

        response = requests.get(url, params=params)

        if 'offer was deactivated' in response.text.lower():
            for lead in leads:
                lead.change_status(Lead.STATUS.CANCELLED, response.text)

        else:
            log = self.get_logger()
            log.debug('url len: %d', len(response.url))
            log.debug('getOrdersByIds: %s', response.text)
            result = response.json()
            log.debug('items count: %d', len(result))

            for lead_id, lead_data in result.items():
                self._update_status(leads_map[lead_id], lead_data)

    def _update_status(self, lead, data):

        assert isinstance(lead, Lead)

        # payout = Decimal(data['payment']['sum'])
        # lead.update_fields(
        #     advertiser_status=data['status'],
        #     advertiser_reported_payout=payout)

        settings = self.advertiser.get_system_settings()

        status = data['status']
        comment = None
        if 'fields' in data:
            comment = data['fields'].get(settings.get('comment_field', 'comment'))

        lead.update_fields(advertiser_status=status, real_status_reason=comment)

        if status in ['processing']:
            pass

        elif status in ['canceled', 'spam']:
            lead.change_status(Lead.STATUS.CANCELLED, comment)

        elif status in ['accepted', 'return', 'paid']:
            lead.change_status(Lead.STATUS.CONFIRMED)


class Leadvertex2(Leadvertex):

    name = 'leadvertex2'
    verbose_name = 'LeadVertex (2)'
    can_send_lead_realtime = True

    def _get_lead_data(self, lead):
        data = super()._get_lead_data(lead)
        data['additional1'] = lead.id
        data['additional2'] = lead.affiliate.id
        return data

    def get_required_config_keys(self):
        return ['api_key', 'webmaster_id', 'url']
