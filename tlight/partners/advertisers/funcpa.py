"""
    api doc: <confidential>
"""
from misc import requests
from partners.advertisers.system import System
from statistic.models import Lead


class FUNCPA(System):
    name = 'funcpa'
    verbose_name = 'FUNCPA'
    host = 'http://api.funcpa.ru/v1/'

    def send_request(self, method, url, **kwargs):
        response = method('{}{}'.format(self.host, url), **kwargs)
        response.raise_for_status()
        return response.json()

    def send_lead(self, lead):
        assert isinstance(lead, Lead)
        advertiser_offer = lead.advertiser_offer
        advertiser_settings = advertiser_offer.get_settings()

        data = {
            'key': advertiser_settings['api_key'],
            'stream': advertiser_settings['channel'],
            'ip': lead.ip_address,
            'clientname': lead.customer_name,
            'phone': lead.customer_phone_clean,
            'comments': lead.customer_comments,
            'sub1': lead.user.id,
        }

        response = self.send_request(requests.post, 'order', data=data)

        if response['status'] == 400:
            self.advertiser_rejected_lead(lead, str(response))
            return

        if response['status'] != 200:
            raise self.CommunicationError('code {}: {}'.format(response['status'], response['error']))

        lead.advertiser_lead_id = response['id']
        lead.save(update_fields=['advertiser_lead_id'])

    def check_leads(self, leads):
        for leads in self.chunks(leads, 20):
            self._check_leads(leads)

    def _check_leads(self, leads):
        advertiser = self.advertiser

        advertiser_settings = advertiser.get_system_settings()
        leads_map = {lead.advertiser_lead_id: lead for lead in leads}

        params = {
            'key': advertiser_settings['api_key'],
            'ids': ','.join(leads_map.keys())
        }

        response = self.send_request(requests.get, 'orders/statuses',  params=params)

        for lead_data in response['orders']:

            lead = leads_map[str(lead_data['id'])]
            status = str(lead_data['order_status'])

            lead.update_fields(advertiser_status=status)

            if status == '1':  # заказ еще в обработке
                continue

            if status == '3':
                lead.change_status(Lead.STATUS.CONFIRMED)
                return

            lead.change_status(Lead.STATUS.CANCELLED, lead_data.get('comments'))

            # if status == '5':
            #     lead.is_trash = True
            #     lead.save(update_fields=['is_trash'])

    def get_required_config_keys(self):
        return ['api_key', 'channel']
