from misc import requests
from partners.advertisers.system import System
from statistic.models import Lead


class Luckyshop(System):

    name = 'luckyshop'
    verbose_name = 'Luckyshop'
    send_host = 'http://affiliate.luckyshop.ru/api-reseller/lead.html'
    check_host = 'http://affiliate.luckyshop.ru/api-reseller/lead-status-batch.html'

    def send_lead(self, lead):
        assert isinstance(lead, Lead)

        advertiser_offer = lead.advertiser_offer
        advertiser_settings = advertiser_offer.get_settings()

        data = {

            "api_key": advertiser_settings['api_key'],  # секретный ключ АПИ *
            "offer_id": advertiser_settings['offer_id'],  # наименование товара *
            "user_id": lead.user_id,
            "name": lead.customer_name,  # ФИО заказчика
            "phone": lead.customer_phone_clean,  # номер телефона заказчика товара *
            "user_agent": lead.useragent,  # Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0
            "ip": lead.ip_address,  # ip клиента который сделал заказ
        }
        response = requests.post(self.send_host, data=data)
        response.raise_for_status()
        raw_response = response.json()  # {"success":'true', "data": { "click_id": 46361517, "country": "RU", "is_desktop": true } }

        result = raw_response
        if result and result.get('success') is True:
            lead.advertiser_lead_id = result['data']['click_id']
            lead.save(update_fields=['advertiser_lead_id'])
        else:
            self.advertiser_rejected_lead(lead, result.get('errors'))

    def check_leads(self, leads):
        for leads in self.chunks(leads, chunk_size=20):
            self._check_statuses(leads)

    def _check_statuses(self, leads):
        log = self.get_logger()
        leads_map = {lead.advertiser_lead_id: lead for lead in leads}
        advertiser_settings = self.advertiser.get_system_settings()

        click_id = ','.join([lead.advertiser_lead_id for lead in leads])
        data = {
            'api_key': advertiser_settings['api_key'],
            'click_id': click_id,
        }
        log.debug('data: %s', data)
        response = requests.get(self.check_host, params=data)

        raw_response = response.json()
        if raw_response['success']:
            for lead_data in raw_response['leads']:
                self._update_status(leads_map[str(lead_data['click_id'])], lead_data)
        else:
            log.debug(raw_response)

    @staticmethod
    def _update_status(lead, data):

        """
        -new(новый заказ), == 0
        -wait(покупатель думает) == 0
        -rejected(отказ клиента), == -10
        -confirmed(клиент подтвердил), == 10
        -trash(несуществующий номер), == -10
        -nopaid(отказ от оплаты), == 10
        -fraud(товар не заказывали), == -10
        -paid(оплачено и получено) == 10
        """
        assert isinstance(lead, Lead)
        lead.update_fields(advertiser_status=data['status'])

        status = lead.advertiser_status

        # 0
        if status in ['new', 'wait']:
            pass

        # -10
        elif status in ['rejected', 'trash', 'fraud']:
            status_reason = data['comment']
            lead.change_status(Lead.STATUS.CANCELLED, status_reason)

        # 10
        elif status in ['confirmed', 'nopaid', 'paid']:
            lead.change_status(Lead.STATUS.CONFIRMED)

    def get_required_config_keys(self):
        return ['offer_id', 'api_key']
