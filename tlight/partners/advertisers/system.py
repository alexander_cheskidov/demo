import logging
from datetime import timedelta

from collections import defaultdict
from django.conf import settings
from django.utils import timezone

from misc.lib import split_to_chunks
from offers.models import OfferStatus
from partners.loggers import get_logger
from statistic.models import Lead

log = logging.getLogger('tlight.partners.system')


class PostbackData:

    def __init__(self, system):
        self.system = system
        self.click_id = None
        self.lead_id = None
        self.lead_our_id = None
        self.status = None
        self.comment = None
        self.advertiser_status = None
        self.customer_name = None
        self.customer_phone = None
        self.country = None
        self.payout = None
        self.ip_address = ''
        self.ignore_postback = False

    def is_empty(self):
        """
            Advertiser sent any significant data (or not)
            :return:
        """
        return not bool(any([
            self.click_id,
            self.lead_id,
            self.lead_our_id,
            self.comment,
            self.advertiser_status,
            self.payout,
        ]))


class System:

    class Error(Exception):
        pass

    class CommunicationError(Error):
        def __init__(self, err):
            super().__init__(err)

    class CancelLead(Exception):
        def __init__(self, real_comment, comment=None):
            self.real_comment = real_comment
            self.comment = comment or real_comment

    name = None
    verbose_name = None
    can_send_lead_realtime = False
    skip_check_empty_leads = True
    provides_status_reason = False
    check_leads_worker = '__default__'

    def __init__(self, advertiser):
        assert advertiser.system == self.name
        self.advertiser = advertiser

    def __str__(self):
        return 'System(name=%s, advertiser=%s)' % (self.name, self.advertiser)

    @staticmethod
    def get_order_id(lead):
        return lead.id

    @classmethod
    def get_logger(cls):
        return get_logger(cls.name)

    def check_lead(self, lead):

        if not lead.submitted:
            time_passed = timezone.now() - lead.created_at
            if time_passed < timedelta(minutes=10):
                return False

        if lead.real_status:
            return False

        if lead.is_blocked:
            raise self.CancelLead('Лид заблокирован')

        if lead.affiliate.block_leads:
            raise self.CancelLead('Прием лидов заблокирован')

        if not lead.advertiser.is_active:
            raise self.CancelLead('Рекламодатель отключен', 'Оффер отключен')

        if lead.advertiser_offer.archived:
            raise self.CancelLead('Оффер отключен')

        if lead.advertiser_offer.offer.status == OfferStatus.ARCHIVED:
            raise self.CancelLead('Оффер отключен')

        if lead.source in [Lead.Source.POSTBACK]:
            return False

        # if lead.source in [Lead.Source.AFFILIATE, Lead.Source.FACEBOOK]:
        if not lead.customer_phone:
            return False

        if settings.DEBUG:
            return False

        return True

    def check_and_send_lead(self, lead):
        if self.check_lead(lead):
            self.send_lead(lead)

    def try_to_send_lead_realtime(self, lead):
        if not self.can_send_lead_realtime:
            return
        try:
            self.check_and_send_lead(lead)
        except Exception as e:
            log.error(e, exc_info=True)

    def send_lead(self, lead):
        pass

    def check_leads(self, leads):
        pass

    def get_postback_data(self, request):
        return request.GET.dict()

    def decode_postback_status(self, status):
        return status

    def decode_postback_data(self, request) -> PostbackData:
        data = self.get_postback_data(request)
        status = self.decode_postback_status(data.get('status'))
        result = PostbackData(self)
        result.click_id = data.get('data1')
        result.lead_id = data.get('id')
        result.lead_our_id = data.get('data5')
        result.status = data.get('our_status', status)
        result.comment = data.get('comment')
        result.advertiser_status = data.get('status')
        result.customer_name = data.get('name', result.customer_name)
        result.customer_phone = data.get('phone', result.customer_phone)
        result.country = data.get('country', result.country)
        result.payout = data.get('payout', data.get('sum', result.payout))
        result.ip_address = data.get('ip_address', result.ip_address)
        return result

    def get_required_config_keys(self):
        return []

    def validate_system_settings(self, advertiser):
        pass

    def advertiser_rejected_lead(self, lead, comment=None):
        lead.advertiser_lead_id = '0'
        lead.advertiser_data = comment or None
        lead.save(update_fields=[
            'advertiser_lead_id',
            'advertiser_data'
        ])

    def group_by_advertiser_offer(self, leads, chunk_size=None):

        result = defaultdict(lambda: [])
        for lead in leads:
            result[lead.advertiser_offer].append(lead)

        for advertiser_offer, leads in result.items():
            for chunk in self.chunks(leads, chunk_size):
                yield advertiser_offer, chunk

    @staticmethod
    def chunks(leads, chunk_size=None):
        if chunk_size:
            return split_to_chunks(leads, chunk_size)
        else:
            return [leads]
