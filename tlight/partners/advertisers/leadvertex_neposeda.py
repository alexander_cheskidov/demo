from .leadvertex import Leadvertex


class Leadvertex_Neposeda(Leadvertex):

    name = 'leadvertex_neposeda'
    verbose_name = 'LeadVertex (neposeda)'
    can_send_lead_realtime = True

    def _get_lead_data(self, lead):
        system_settings = lead.advertiser_offer.get_settings()
        data = super()._get_lead_data(lead)
        data['domain'] = system_settings.get('offer_name')
        data['additional1'] = lead.id
        data.pop('additional2', None)
        return data

    def get_required_config_keys(self):
        return ['offer_name', 'api_key', 'webmaster_id', 'url']
