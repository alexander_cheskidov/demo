"""
    api documentation: <confidential>
"""

import json
from urllib.parse import urljoin

import pytz
import re
from django.utils import timezone

from misc import requests
from partners.advertisers.system import System
from statistic.models import Lead


class Retailcrm(System):

    name = 'retailcrm'
    verbose_name = 'RetailCrm (API V5)'
    can_send_lead_realtime = True

    def send_lead(self, lead):
        assert isinstance(lead, Lead)

        advertiser_offer = lead.advertiser_offer
        advertiser_settings = advertiser_offer.get_settings()

        moscow_tz = pytz.timezone('Europe/Moscow')
        created_at = timezone.localtime(lead.created_at, moscow_tz)
        created_at = created_at.strftime('%Y-%m-%d %H:%M:%S')

        data = {
            'order': json.dumps({
                # 'number': order_id,
                'externalId': lead.id,
                'createdAt': created_at,
                'firstName': lead.customer_name,
                'phone': lead.customer_phone_clean,
                'countryIso': lead.customer_country,
                'customerComment': lead.customer_comments,
                'source': {
                    'source': 'tlight',
                    'medium': lead.affiliate.id,
                    'keyword': lead.id,
                },
                'customFields': {
                    'source': 'tlight',
                    'medium': lead.affiliate.id,
                    'term': lead.id,
                },
                'items': [{
                    'offer': {
                        'id': advertiser_settings['offer_id'],
                        'quantity': 1,
                        # 'comment': lead.customer_comments,
                    }
                }],
            }),
        }

        params = {
            'apiKey': advertiser_settings['api_key'],
        }

        send_lead_url = urljoin(advertiser_settings['crm_url'], '/api/v5/orders/create')
        response = requests.post(send_lead_url, params=params, data=data, verify=False)

        if response.status_code == 403:
            self.advertiser_rejected_lead(lead, "403: Доступ запрещен")
            return

        if response.status_code == 400:
            self.advertiser_rejected_lead(lead, response.text)
            return

        response.raise_for_status()

        raw_response = response.text
        raw_response = re.sub(r'^[^{]+\{', '{', raw_response)

        data = json.loads(raw_response)

        if data.get('id'):
            lead.advertiser_lead_id = data['id']
            lead.save(update_fields=['advertiser_lead_id'])

        elif data.get('errors'):
            self.advertiser_rejected_lead(lead, str(data))

    def check_leads(self, leads):
        for advertiser_offer, leads in self.group_by_advertiser_offer(leads, 20):
            self._check_leads(leads, advertiser_offer)

    def _check_leads(self, leads, advertiser_offer):

        advertiser_settings = advertiser_offer.get_settings()
        leads_map = {lead.id: lead for lead in leads}

        params = {
            'externalIds[]': list(leads_map.keys()),
            'apiKey': advertiser_settings['api_key'],
        }

        check_status_url = urljoin(advertiser_settings['crm_url'], '/api/v5/orders/statuses')
        response = requests.get(check_status_url, params=params, verify=False)

        raw_response = response.text
        raw_response = re.sub(r'^[^{]+\{', '{', raw_response)
        data = json.loads(raw_response).get('orders', [])

        params = {
            'filter[externalIds][]': list(leads_map.keys()),
            'apiKey': advertiser_settings['api_key'],
        }

        orders_feed_url = urljoin(advertiser_settings['crm_url'], '/api/v5/orders')
        more_data = requests.get(orders_feed_url, params=params, verify=False).json()
        more_data = {int(item['externalId']): item for item in more_data['orders']}

        for lead_data in data:
            lead_id = int(lead_data['externalId'])
            lead_data.update(more_data[lead_id])
            self._update_status(leads_map[lead_id], lead_data)

    def _update_status(self, lead, data):

        lead.update_fields(advertiser_status=data['status'])

        if lead.advertiser.get_system_settings().get('status_decoder') == 'artspace':
            status = _decode_status_artspace(data)
        else:
            status = _decode_status_default(data)

        if status:
            lead.change_status(status, data.get('managerComment'))

    def get_required_config_keys(self):
        return ['offer_id', 'crm_url', 'api_key']


def _decode_status_default(data):
    status = data['status']
    status_group = data['group']

    if status in ['no-call2'] or status_group in ['new', 'approval', 'approval-1', 'waiting']:
        pass

    elif status_group in ['cancel', 'test']:
        return Lead.STATUS.CANCELLED

    elif status_group in ['assembling', 'complete', 'delivery', 'vozvrat', 'vozvrat-deneg']:
        return Lead.STATUS.CONFIRMED


def _decode_status_artspace(data):
    status = data['status']

    if status in [
        'betapro-4',
        'ff-betapro',
        'betapro-export-error',
        'betapro-1',
        'betapro-complete',
        'betapro-999',
        'send-to-assembling',
        'assembling',
        'assembling-complete',
        'send-to-delivery',
        'delivering',
        'redirect',
        'complete',
        'complete-order',
    ]:
        return Lead.STATUS.CONFIRMED

    if status in [
        'reworker-11',
        'cancel-after-complete',
        'cancel-at-registration',
        'no-product',
        'already-buyed',
        'delyvery-did-not-suit',
        'prices-did-not-suit',
        'already-buyed',
        'delyvery-did-not-suit',
        'prices-did-not-suit',
        'cancel-other',
        'reworker-12',
        'test-inoe',
        'reworker-14',
    ]:
        return Lead.STATUS.CANCELLED
