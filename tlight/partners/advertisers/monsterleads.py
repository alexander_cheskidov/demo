"""
    api documentation: <confidential>
"""
import time
from misc import requests
from partners.advertisers.system import System
from statistic.models import Lead


class Monsterleads(System):

    name = 'monsterleads'
    verbose_name = 'Monsterleads'
    host = 'http://api.monsterleads.pro/method'
    cancel_reasons = {
        '1': 'Не заказывал',
        '2': 'Нашел дешевле',
        '3': 'Не устр. дост.',
        '4': 'Hекорректные данные',
        '5': 'Дубль',
        '6': 'Неверное GEO',
        '7': 'Нету доставки в этот регион',
        '8': 'Консультация по товару',
        '9': 'Консультация по возврату',
        '10': 'Абонент недоступен больше недели',
        '11': 'Изменение GEO',
        '12': 'Другая причина',
        '13': 'Консультация по пред. заявке',
        '14': 'Тест',
        '15': 'Дорого',
        '16': 'Передумал',
        '17': 'Не предоставляет всех данных для подтверждения',
        '18': 'не говорит по-русски',
        '19': 'Не поддерживаемое GEO'
    }

    def send_lead(self, lead):
        time.sleep(2)
        assert isinstance(lead, Lead)
        advertiser_offer = lead.advertiser_offer
        advertiser_settings = advertiser_offer.get_settings()

        data = {
            'api_key': advertiser_settings['api_key'],
            'tel': lead.customer_phone_clean,
            'ip': lead.ip_address,
            'code': advertiser_settings['channel'],
            'geo': lead.customer_country,
            'mail': lead.customer_email,
            'client': lead.customer_name,
            'adress': lead.customer_address,  # from api docs
            'comments': lead.customer_comments,
        }

        params = {
            'format': 'json'
        }

        response = requests.post('{}/{}'.format(self.host, 'order.add'), data=data, params=params)
        response.raise_for_status()
        response = response.json()

        if response.get('error_code', ''):
            self.advertiser_rejected_lead(lead, response)

        elif 'lead_hash' in response:
            lead.advertiser_lead_id = response['lead_hash']
            lead.save(update_fields=['advertiser_lead_id'])

        else:
            self.get_logger().error(str(response))

    def check_leads(self, leads):
        for advertiser_offer, leads in self.group_by_advertiser_offer(leads, 25):
            self._check_leads(leads, advertiser_offer)

    def _check_leads(self, leads, advertiser_offer):

        advertiser_settings = advertiser_offer.get_settings()
        leads_map = {lead.advertiser_lead_id: lead for lead in leads}

        params = {
            'api_key': advertiser_settings['api_key'],
            'lead_list': ','.join(leads_map.keys()),
            'search_by': 'hash',
            'format': 'json'
        }

        response = requests.get('{}/{}'.format(self.host, 'lead.list'), params=params)
        response.raise_for_status()
        response = response.json()

        for lead_data in response:
            self._update_lead(leads_map[lead_data['hash']], lead_data)

    def _update_lead(self, lead, lead_data):

        status = str(lead_data['status'])
        lead.update_fields(
            advertiser_status=status,
            real_status_reason=lead_data.get('data4'))

        if status == '1':
            pass

        elif status == '2':
            lead.change_status(Lead.STATUS.CONFIRMED)

        elif status == '3':
            cancel_reasons = self.cancel_reasons.get(lead_data['cancel_reason'], '')
            if not lead_data['valid']:
                cancel_reasons = '[trash] ' + cancel_reasons.strip()
            lead.change_status(Lead.STATUS.CANCELLED, cancel_reasons)

    def get_required_config_keys(self):
        return ['api_key', 'channel']

    def get_postback_data(self, request):
        data = request.GET.dict()
        data['data1'] = data.pop('data', data.get('data1'))
        data['id'] = data.pop('lead_id', data.get('id'))
        return data

    def decode_postback_status(self, status):
        if status in ['waiting']:
            return Lead.STATUS.NEW
        elif status in ['approved']:
            return Lead.STATUS.CONFIRMED
        elif status in ['declined']:
            return Lead.STATUS.CANCELLED
