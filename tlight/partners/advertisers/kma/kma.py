import time
from datetime import timedelta

from django.utils import timezone

from misc import requests
from partners.advertisers.kma.models import KMAAuth
from partners.advertisers.system import System
from statistic.models import Lead


class KMA(System):

    name = 'kmabiz'
    verbose_name = 'KMA.biz'
    host = 'http://api.kma1.biz/'

    def send_lead(self, lead):
        assert isinstance(lead, Lead)

        if lead.advertiser_offer.link:
            return

        advertiser_offer = lead.advertiser_offer
        advertiser_settings = advertiser_offer.get_settings()

        data = {
            'method': 'addlead',
            'name': lead.customer_name,
            'phone': lead.customer_phone_clean,
            'ip': lead.ip_address,
            'channel': advertiser_settings['channel'],
            'data1': lead.affiliate.id,
        }

        authid, authhash = self.perform_authorization(advertiser_offer.advertiser)
        data['authid'] = authid
        data['authhash'] = authhash

        response = self.send_request(data)

        if 'orderid' in response:
            lead.advertiser_lead_id = response['orderid']
            lead.save(update_fields=['advertiser_lead_id'])

        elif response['code'] == 8:
            lead.advertiser_lead_id = 0
            lead.advertiser_data = str(response)
            lead.save()

        elif response['code'] == 4:
            raise self.Error(response['msg'])

    def check_leads(self, leads):
        for leads in self.chunks(leads, 50):
            self._check_leads(leads)

    def _check_leads(self, leads):

        advertiser = self.advertiser
        leads_map = {lead.advertiser_lead_id: lead for lead in leads}

        data = {
            'method': 'getstatuses',
            'ids': ','.join(leads_map.keys()),
        }

        authid, authhash = self.perform_authorization(advertiser)
        data['authid'] = authid
        data['authhash'] = authhash

        response = self.send_request(data)

        if response['code']:
            self.get_logger().error(self.Error(response), exc_info=True)
            return

        for lead_data in response['statuses']:

            lead = leads_map[lead_data['id']]
            status = str(lead_data['status'])
            comment = lead_data.get('comment', None)
            lead.update_fields(advertiser_status=status, real_status_reason=comment)

            if lead.advertiser_status == 'P':
                continue

            elif lead.advertiser_status == 'A':
                lead.change_status(Lead.STATUS.CONFIRMED)

            else:
                lead.change_status(Lead.STATUS.CANCELLED, comment)

    def get_required_config_keys(self):
        return ['username', 'password', 'channel']

    def perform_authorization(self, user):

        self.get_logger().debug('perform_authorization( %s )', user)

        _default_last_login = timezone.now().replace(year=2000)
        auth_credentials = KMAAuth.objects.get_or_create(user=user, defaults={'last_login': _default_last_login})[0]
        hour_ago = timezone.now() - timedelta(hours=1)

        if auth_credentials.last_login < hour_ago:

            self.get_logger().debug('perform_authorization( %s ): auth expired, get new auth token', user)

            system_settings = user.get_system_settings()

            username = system_settings['username']
            password = system_settings['password']

            data = {
                'method': 'auth',
                'username': username,
                'pass': password,
            }

            response = self.send_request(data)

            if response['code'] == 4:
                return auth_credentials.authid, auth_credentials.authhash

            if response['code']:
                raise self.Error(response['msg'])

            auth_credentials.authid = response['authid']
            auth_credentials.authhash = response['authhash']
            auth_credentials.last_login = timezone.now()
            auth_credentials.save()

        return auth_credentials.authid, auth_credentials.authhash

    def send_request(self, data):
        time.sleep(0.5)
        response = requests.post(self.host, data)
        response.raise_for_status()
        return response.json()

    def decode_postback_status(self, status):

        if status in ['N', 'P']:
            # P - pending
            return Lead.STATUS.NEW

        elif status in ['A', 'C']:
            return Lead.STATUS.CONFIRMED

        elif status == 'payed':
            return Lead.STATUS.CONFIRMED

        elif status in ['D', 'F']:
            # F - fake/double/fraud
            # D - declined
            return Lead.STATUS.CANCELLED

    def get_postback_data(self, request):
        data = request.GET.dict()
        data['id'] = data['orderid']
        return data
