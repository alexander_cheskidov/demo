from django.db import models
from django.utils import timezone

from users.models import User


class KMAAuth(models.Model):

    user = models.OneToOneField(User, primary_key=True)
    authid = models.CharField(max_length=64)
    authhash = models.CharField(max_length=64)
    last_login = models.DateTimeField(default=timezone.now)
