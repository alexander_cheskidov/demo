import json
import hashlib
from misc import requests
from partners.advertisers.system import System
from statistic.models import Lead


class CpaKitchen(System):
    name = 'cpakitchen'
    verbose_name = 'Cpa Kitchen'
    send_host = 'http://happy-crm.ru/api/send_order.php'
    check_host = 'http://happy-crm.ru/api/get_orders.php'

    def send_lead(self, lead):
        assert isinstance(lead, Lead)

        advertiser_offer = lead.advertiser_offer
        advertiser_settings = advertiser_offer.get_settings()

        price = lead.advertiser_offer.price
        if price is not None:
            price = str(price)

        data = {
            "test": False,  # тестовый режим, false отключен true включен

            "secret": advertiser_settings['api_key'],  # секретный ключ АПИ *
            "offer": advertiser_settings['offer_id'],  # наименование товара *
            "phone": lead.customer_phone_clean,  # номер телефона заказчика товара *
            "order_id": str(lead.id),  # id заявки внешний для синхронизации*
            "country": lead.customer_country,  # Гео локация *

            "name": lead.customer_name,  # ФИО заказчика
            "addr": lead.customer_address,  # адрес
            "user_agent": lead.useragent,  # Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0
            "web_id": str(lead.affiliate.id),  # id web-а
            "remote_ip": lead.ip_address,  # ip клиента который сделал заказ

            "price": price,    # стоимость товара
            # "site": "http://example.com",     #  url лендинга
            # "platform": "1",    # платформа с которой был сделан заказ '0' => 'моб',  '1' => 'десктоп', '2' => 'noname',
        }
        data = json.dumps(data)
        params = {
            'uid': advertiser_settings['uid'],
            'hash': self.generate_hash(data, advertiser_settings['uid']),
        }
        response = requests.post(self.send_host, params=params, data={'data': data})
        response.raise_for_status()
        raw_response = response.json()  # {"result": {"success":'TRUE', "message": "Success created ID:00000", "id": "00000"} }

        result = raw_response.get('result')
        if result and result.get('success') == 'TRUE':
            lead.advertiser_lead_id = result['id']
            lead.save(update_fields=['advertiser_lead_id'])
        else:
            self.advertiser_rejected_lead(lead, result.get('message'))

    def check_leads(self, leads):
        for leads in self.chunks(leads, 45):
            self._check_statuses(leads)

    def _check_statuses(self, leads):
        log = self.get_logger()
        advertiser_settings = self.advertiser.get_system_settings()
        leads_map = {lead.advertiser_lead_id: lead for lead in leads}
        data = []
        for lead_id, lead in leads_map.items():
            if lead.advertiser_lead_id:
                data.append({
                    'id': int(lead.advertiser_lead_id),
                    'country': lead.customer_country.lower()
                })

        data = json.dumps(data)
        log.debug('data: %s', data)

        params = {
            'uid': advertiser_settings['uid'],
            's': advertiser_settings['api_key'],
            'hash': self.generate_hash(data, advertiser_settings['uid']),
        }
        response = requests.post(self.check_host, params=params, data={'data': data})

        raw_response = response.json()

        try:
            for lead_id, data in raw_response.items():
                self._update_status(leads_map[lead_id], data)
        except AttributeError:
            log.debug(raw_response)

    @staticmethod
    def _update_status(lead, data):

        """
        0 - Новая
        1 - Подтвержден
        2 - Отменён
        3 - Перезвонить
        4 - Недозвон
        5 - Спам
        6 - Черный список
        """
        assert isinstance(lead, Lead)

        lead.update_fields(advertiser_status=data['status'])

        status = lead.advertiser_status

        if status in [0, 3, 4]:
            pass

        elif status in [2, 5, 6]:
            status_reason = data.get('description')
            lead.change_status(Lead.STATUS.CANCELLED, status_reason)

        elif status in [1]:
            lead.change_status(Lead.STATUS.CONFIRMED)

    def get_required_config_keys(self):
        return ['offer_id', 'api_key', 'uid']

    @staticmethod
    def generate_hash(data, uid):
        hash_str = '{}{}'.format(len(data), hashlib.md5(str(uid).encode()).hexdigest())
        return hashlib.sha256(hash_str.encode()).hexdigest()
