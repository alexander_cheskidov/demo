import re
import logging
import time
from decimal import Decimal

from collections import Iterable
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q


class Profiler(object):

    def __init__(self, name, logger=None):
        self.time = None
        self.name = name
        self.logger = logger
        if not self.logger:
            self.logger = logging.getLogger()

    def __enter__(self):
        self.time = time.time()

    def __exit__(self, exp_type, exp_value, traceback):
        self.time = time.time() - self.time
        self.logger.info('Profiler: %s: %.6f sec', self.name, self.time)


def split_to_chunks(ll, chunk_size):
    chunk_size_counter = 0
    chunk = []
    for item in ll:
        chunk.append(item)
        chunk_size_counter += 1
        if chunk_size_counter == chunk_size:
            yield chunk
            chunk = []
            chunk_size_counter = 0

    if chunk:
        yield chunk


_decimal_cleaner = re.compile('[^,\-\.\d]')

def str_to_decimal(s):
    s = _decimal_cleaner.sub('', s)
    if len(s) == 0:
        s = '0'
    s = s.replace(',', '.')
    return Decimal(s)


def safe_divide(dividend, divisor) -> float:
    dividend = dividend or 0
    divisor = divisor or 0
    try:
        return float(dividend) / float(divisor)
    except ZeroDivisionError:
        return 0.0


def url_param_bool(value):
    if isinstance(value, str):
        value = value.strip().lower()
    if value in ['0', 'false', 'none', 'null']:
        return False
    return bool(value)


def weighted_random_choice(items):
    import random
    if isinstance(items, dict):
        items = items.items()
    else:
        items = enumerate(items)
    total = 0
    result = None
    for item, weight in items:
        if not weight:
            continue
        total += weight
        if random.random() * total < weight:
            result = item
    return result


def generate_choicefield_choices(choices, blank_field=None):
    choices = tuple(choices)
    if isinstance(blank_field, str):
        choices = (('', blank_field), ) + choices
    elif isinstance(blank_field, Iterable):
        choices = (blank_field, ) + choices
    elif blank_field:
        choices = (('', '--------'), ) + choices
    return choices


def get_country_formfield(blank_field='Выберите страну', label='Страна', required=False, multiple=False):
    from django import forms
    from offers.countries import COUNTRY_CHOICES

    choices = []
    for cc, country_name in COUNTRY_CHOICES:
        country_name = '[%s] %s' % (cc, country_name)
        choices.append((cc, country_name))

    if multiple:
        blank_field = None
    choices = generate_choicefield_choices(choices, blank_field)

    if multiple:
        return forms.MultipleChoiceField(choices, label=label, required=required)
    else:
        return forms.ChoiceField(choices, label=label, required=required)


DEFAULT_ITEMS_PER_PAGE = 100


def paginate(items, page_number, items_per_page=DEFAULT_ITEMS_PER_PAGE):

    page_number = page_number or 1

    if items_per_page is None:
        items_per_page = DEFAULT_ITEMS_PER_PAGE

    items_per_page = int(items_per_page or 0)
    pager = Paginator(items, items_per_page)

    try:
        page = pager.page(page_number)
    except PageNotAnInteger:
        page = pager.page(1)
    except EmptyPage:
        page = pager.page(pager.num_pages)

    return page


def mask_to_django_query(field, value):
    value = value.strip()
    if '%' in value:
        if value.startswith('%') and value.endswith('%'):
            field += '__icontains'
            value = value.strip('%')
        elif value.startswith('%'):
            field += '__iendswith'
            value = value.strip('%')
        elif value.endswith('%'):
            field += '__istartswith'
            value = value.strip('%')
    return Q(**{field: value})