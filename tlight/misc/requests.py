import requests
from django.conf import settings
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

RequestException = requests.RequestException
Timeout = requests.Timeout


class GlobalTimeoutAdapter(HTTPAdapter):

    def send(self, request, **kwargs):
        timeout = kwargs.get('timeout')
        if not timeout:
            timeout = settings.REQUESTS_DEFAULT_TIMEOUT
        kwargs['timeout'] = timeout
        return super().send(request, **kwargs)


def get_http_client():
    http = requests.Session()
    http.mount('http://', GlobalTimeoutAdapter())
    http.mount('https://', GlobalTimeoutAdapter())
    return http


def get(url, **kwargs):
    http = get_http_client()
    return http.get(url, **kwargs)


def post(url, data=None, json=None, **kwargs):
    http = get_http_client()
    return http.post(url, data=data, json=json, **kwargs)


def put(url, data=None, json=None, **kwargs):
    http = get_http_client()
    return http.put(url, data=data, json=json, **kwargs)


def delete(url, data=None, json=None, **kwargs):
    http = get_http_client()
    return http.delete(url, data=data, json=json, **kwargs)
